<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackStatusesTable extends Migration
{
    public function up()
    {
        Schema::create('track_statuses', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('order')->nullable();
            $table->string('description')->nullable();
            $table->string('color')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('track_statuses');
    }
}
