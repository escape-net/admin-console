<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionsTable extends Migration
{

    public function up()
    {
        Schema::create('promotions', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('code')->nullable();
            $table->text('users')->nullable();
            $table->enum('type', ['flat', 'percentage'])->default('flat')->nullable();
            $table->float('amount')->nullable();
            $table->enum('validity', ['forever', 'date'])->default('forever')->nullable();
            $table->datetime('start_at')->nullable();
            $table->datetime('end_at')->nullable();
            $table->float('limit')->nullable();
            $table->text('description')->nullable();
            $table->enum('status', ['active', 'inactive'])->default('active')->nullable();
            $table->timestamps();

        });
    }

    public function down()
    {
        Schema::dropIfExists('promotions');
    }
}
