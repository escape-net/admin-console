<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            
            $table->increments('id');
            $table->integer('company_id')->nullable();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('phone');
            $table->integer('country_id')->nullable();
            $table->integer('zone_id')->nullable();
            $table->text('address')->nullable();
            $table->text('password')->nullable();
            $table->integer('status')->default(0);
            $table->enum('role', ['user', 'driver', 'partner', 'admin'])->default('user');
            $table->text('api_token')->nullable();
            $table->text('device_token')->nullable();
            $table->text('photo')->nullable();
            $table->text('device')->nullable();
            $table->float('balance')->default(0);
            $table->datetime('last_login')->nullable();
            $table->string('partner_vat_number')->nullable();
            $table->string('driver_license')->nullable();
            $table->enum('driver_status', ['online', 'assigned', 'on_trip', 'offline'])->default('offline')->nullable();
            $table->enum('driver_category', ['driving_only', 'vehicle_owner_and_driving', 'vehicle_owner_not_driving'])->default('driving_only')->nullable();
            $table->integer('driver_vehicle_type_id')->nullable();
            $table->integer('bank_id')->nullable();
            $table->string('bank_account_number')->nullable();
            $table->string('bank_account_name')->nullable();
            $table->integer('access_level')->nullable()->default(0);
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->text('remember_token')->nullable();
            $table->integer('otp')->nullable();
            $table->dateTime('expires_at')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }


    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('riders');
        Schema::dropIfExists('admins');
        Schema::dropIfExists('service_partners');
    }
}
