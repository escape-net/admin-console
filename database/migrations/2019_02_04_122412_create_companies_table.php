<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->integer('country_id')->nullable();
            $table->integer('zone_id')->nullable();
            $table->text('address')->nullable();
            $table->enum('status', ['active', 'inactive'])->default('inactive');
            $table->string('logo')->nullable();
            $table->timestamps();
            $table->softDeletes();
            
        });
    }


    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
