<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveriesTable extends Migration
{

    public function up()
    {
        Schema::create('deliveries', function (Blueprint $table) {
            
            $table->increments('id');
            $table->integer('user_id');
            $table->string('reference')->nullable();
            $table->string('tracking_code')->nullable();
            $table->string('item')->nullable();
            $table->integer('vehicle_type_id')->default(0);
            $table->integer('pickup_place_id')->nullable();
            $table->integer('delivery_place_id')->nullable();
            $table->integer('driver_id')->nullable();
            $table->integer('transaction_id')->nullable();
            $table->integer('promotion_id')->nullable();
            $table->string('recipient_name')->nullabe();
            $table->string('recipient_phone')->nullabe();
            $table->text('instructions')->nullable();
            $table->decimal('base_fare')->default(0)->nullabe();
            $table->decimal('per_minute')->default(0)->nullabe();
            $table->decimal('per_km')->default(0)->nullabe();
            $table->decimal('approx_km')->default(0)->nullabe();
            $table->decimal('surcharge')->default(0)->nullabe();
            $table->decimal('discount')->default(0)->nullabe();
            $table->decimal('size_fare')->default(0)->nullabe();
            $table->decimal('cancellation_fee')->default(0)->nullabe();
            $table->decimal('minimum')->default(0);
            $table->decimal('maximum')->default(0)->nullabe();
            $table->datetime('start_at')->nullable();
            $table->datetime('end_at')->nullable();
            $table->decimal('total_minutes')->default(0)->nullabe();
            $table->decimal('total_km')->default(0)->nullabe();
            $table->decimal('subtotal')->default(0)->nullabe();
            $table->decimal('total')->default(0)->nullabe();
            $table->decimal('commission')->default(0)->nullabe();
            $table->enum('status', ['pending', 'driver_assigned', 'scheduled', 'started', 'completed', 'active'])->default('pending');
            $table->datetime('scheduled_at')->nullable();
            $table->string('ip_address')->nullable();
            $table->string('device')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::dropIfExists('fares');
    }
}
