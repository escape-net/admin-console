<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{

    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('company_id');
            $table->integer('vehicle_type_id')->nullable();
            $table->string('make')->nullable();
            $table->string('model')->nullable();
            $table->string('year')->nullable();
            $table->string('color')->nullable();
            $table->string('plate_number')->nullable();
            $table->string('registration')->nullable();
            $table->string('insurance')->nullable();
            $table->enum('insurance_status', ['pending', 'in_progress', 'approved'])->nullable()->default('pending');
            $table->string('certificate')->nullable();
            $table->enum('certificate_status', ['pending', 'in_progress', 'approved'])->nullable()->default('pending');
            $table->string('image')->nullable();
            $table->enum('status', ['active', 'inactive'])->nullable()->default('inactive');
            $table->timestamps();
            $table->softDeletes();
            
        });
    }

    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
