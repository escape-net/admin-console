<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtpsTable extends Migration
{

    public function up()
    {
        Schema::create('otps', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('phone')->nullable();
            $table->string('ip_address')->nullable();
            $table->string('otp')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();

        });
    }

    public function down()
    {
        Schema::dropIfExists('otps');
    }
}
