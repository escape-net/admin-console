<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackHistoriesTable extends Migration
{
    public function up()
    {
        Schema::create('track_histories', function (Blueprint $table) {
            
            $table->increments('id');
            $table->integer('delivery_id')->nullable();
            $table->integer('track_status_id')->nullable();
            $table->text('status')->nullable();
            $table->text('comment')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('track_histories');
    }
}
