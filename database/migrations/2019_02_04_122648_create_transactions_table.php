<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{

    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('delivery_id')->nullable();
            $table->string('source')->nullable();
            $table->float('amount')->nullable();
            $table->string('status')->nullable();
            $table->string('transaction_reference')->nullable();
            $table->datetime('transaction_date')->nullable();
            $table->string('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
