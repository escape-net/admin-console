<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleTypeTable extends Migration
{

    public function up()
    {
        Schema::create('vehicle_types', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('name')->nullable();
            $table->decimal('base_fare')->nullable()->default(1);
            $table->decimal('per_minute')->nullable()->default(1);
            $table->decimal('per_km')->default(1);
            $table->decimal('commission_percentage')->default(0);
            $table->decimal('cancellation_fee')->default(0);
            $table->boolean('night')->default(false);
            $table->time('night_start_time')->nullable();
            $table->time('night_end_time')->nullable();
            $table->decimal('night_surcharge')->nullable()->default(0);
            $table->boolean('mobile_order')->default(0)->nullable();
            $table->string('mobile_icon')->default('motorcycle')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }


    public function down()
    {
        Schema::dropIfExists('vehicle_types');
    }
}
