<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryHistoriesTable extends Migration
{
    public function up()
    {
        Schema::create('delivery_histories', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('delivery_id');
            $table->integer('user_id');
            $table->text('history');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('delivery_histories');
    }
}
