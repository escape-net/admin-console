<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParcelSizesTable extends Migration
{

    public function up()
    {
        Schema::create('parcel_sizes', function (Blueprint $table) {
            
            $table->increments('id');
            $table->integer('vehicle_type_id')->default(0);
            $table->text('name')->nullable();
            $table->decimal('price')->default(0);
            $table->timestamps();

        });
    }

    public function down()
    {
        Schema::dropIfExists('parcel_sizes');
    }
}
