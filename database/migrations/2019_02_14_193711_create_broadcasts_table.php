<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBroadcastsTable extends Migration
{
    public function up()
    {
        Schema::create('broadcasts', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->enum('type', ['sms', 'email', 'notification'])->nullable();
            $table->text('body')->nullable();
            $table->text('subject')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->datetime('processed_at')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('broadcasts');
    }
}
