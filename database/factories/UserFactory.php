<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {

    return [

        'company_id' 		=> 1,
        'first_name' 		=> $faker->name,
        'last_name' 		=> $faker->name,
        'email' 			=> $faker->unique()->safeEmail,
        'phone' 			=> $faker->phoneNumber,
        'role'				=> 'user',
        'api_token'			=> str_random(16),
        'remember_token' 	=> str_random(10)
    ];
});
