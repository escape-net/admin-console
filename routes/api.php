<?php

use Illuminate\Http\Request;

Route::group(['namespace' => 'Api'], function(){

	Route::post('register', 'AuthController@register');
	Route::post('login', 'AuthController@login');
	Route::get('vehicle-types', 'UtilitiesController@vehicleTypes');
	Route::get('banks', 'UtilitiesController@banks');

});

Route::group(['namespace' => 'Api', 'middleware' => 'auth:api'], function(){

	Route::get('logout', 'AuthController@logout');
	Route::get('notifications', 'NotificationsController@index');
	Route::post('update-device-token', 'AuthController@updateDeviceToken');
	Route::get('update-location/{latitude?}/{longitude?}', 'UtilitiesController@updateLocation');
	Route::get('delivery/{id?}', 'Driver\DeliveriesController@delivery');

});

Route::group(['prefix' => 'driver', 'namespace' => 'Api\Driver', 'middleware' => 'auth:api'], function(){

	Route::get('deliveries', 'DeliveriesController@index');
	Route::get('delivery/{id?}', 'DeliveriesController@delivery');
	Route::get('delivery-start/{id?}', 'DeliveriesController@startDelivery');
	Route::get('delivery-complete/{id?}', 'DeliveriesController@completeDelivery');
	Route::get('decline-delivery/{id?}', 'DeliveriesController@decline');
	Route::post('bank-details', 'AccountController@bankDetails');
	Route::get('status/{status?}', 'AccountController@status');
	Route::post('upload/{field?}', 'AccountController@upload');
	Route::post('upload-vehicle/{field?}', 'AccountController@uploadVehicle');
	Route::post('edit-profile', 'AccountController@editProfile');
	Route::post('edit-vehicle', 'AccountController@editVehicle');	

});

Route::group(['prefix' => 'user', 'namespace' => 'Api\User', 'middleware' => 'auth:api'], function(){

	Route::get('deliveries', 'DeliveriesController@index');
	Route::get('vehicle-types', 'DeliveriesController@vehicleTypes');
	Route::post('delivery/estimate', 'DeliveriesController@estimate');
	Route::post('delivery/new', 'DeliveriesController@newDelivery');
	Route::post('assign-driver', 'DeliveriesController@assignDriver');
	Route::get('places', 'DeliveriesController@places');

});
