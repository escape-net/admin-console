<?php

Route::any('/', 'AuthController@login');
Route::any('login', 'AuthController@login')->name('login');
Route::any('logout', 'AuthController@logout');
Route::any('forgot-password', 'AuthController@forgotPassword');
Route::get('reset-password/{token?}', 'AuthController@resetPassword');
Route::post('change-password', 'AuthController@changePassword');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth', 'admin']], function(){

	Route::get('/', 'HomeController@index');
	Route::get('api-logs', 'HomeController@apiLogs');
	Route::get('otps', 'HomeController@otps');

	Route::group(['prefix' => 'administrators'], function(){

		Route::get('/', 'AdminsController@index');
		Route::get('/form/{id?}', 'AdminsController@form');
		Route::post('/save', 'AdminsController@save');

	});

	Route::group(['prefix' => 'users'], function(){

		Route::get('/', 'UsersController@index');
		Route::get('/form/{id?}', 'UsersController@form');
		Route::post('/save', 'UsersController@save');

	});

	Route::group(['prefix' => 'companies'], function(){

		Route::get('/', 'CompaniesController@index');
		Route::get('/form/{id?}', 'CompaniesController@form');
		Route::post('/save', 'CompaniesController@save');

	});

	Route::group(['prefix' => 'drivers'], function(){

		Route::get('/', 'DriversController@index');
		Route::get('/form/{id?}', 'DriversController@form');
		Route::post('/save', 'DriversController@save');
		Route::get('/driver/{id?}', 'DriversController@driver');

	});

	Route::group(['prefix' => 'vehicles'], function(){

		Route::get('/', 'VehiclesController@index');
		Route::get('/form/{id?}', 'VehiclesController@form');
		Route::post('/save', 'VehiclesController@save');
		Route::get('/vehicle/{id?}', 'VehiclesController@vehicle');
		Route::post('/document-status', 'VehiclesController@documentStatus');

	});

	Route::group(['prefix' => 'vehicle-types'], function(){

		Route::get('/', 'VehicleTypesController@index');
		Route::get('/form/{id?}', 'VehicleTypesController@form');
		Route::post('/save', 'VehicleTypesController@save');

	});

	Route::group(['prefix' => 'transactions'], function(){

		Route::get('/', 'TransactionsController@index');

	});

	Route::group(['prefix' => 'deliveries'], function(){

		Route::get('/', 'DeliveriesController@index');
		Route::get('/form/{id?}', 'DeliveriesController@form');
		Route::post('/save', 'DeliveriesController@save');
		Route::get('/delivery/{id?}', 'DeliveriesController@index');
		Route::get('/invoice/{id?}', 'DeliveriesController@invoice');
		Route::post('/search', 'DeliveriesController@search');

		Route::group(['prefix' => 'api'], function(){

			Route::get('/', 'DeliveriesController@deliveries');
			Route::get('/delivery/{id?}', 'DeliveriesController@delivery');
			Route::get('/search-drivers/{vehicle_type_id?}', 'DeliveriesController@searchDrivers');
			Route::post('/assign-driver/{driver_id?}', 'DeliveriesController@assignDriver');
			Route::post('/auto-assign-driver', 'DeliveriesController@autoAssignDriver');
			Route::post('/cash-payment', 'DeliveriesController@cashPayment');
			Route::get('/add-promotion-code/{delivery_id?}/{promotion_code?}', 'DeliveriesController@addPromotionCode');
			Route::post('/calculate-delivery', 'DeliveriesController@calculateDelivery');
			Route::post('/complete-delivery', 'DeliveriesController@completeDelivery');
		
		});

	});

	Route::group(['prefix' => 'promotions'], function(){

		Route::get('/', 'PromotionsController@index');
		Route::get('/form/{id?}', 'PromotionsController@form');
		Route::post('/save', 'PromotionsController@save');
	});

	Route::group(['prefix' => 'godsview'], function(){

		Route::get('/', 'GodsViewController@index');
	});

	Route::group(['prefix' => 'track-delivery'], function(){

		Route::post('save-history', 'TrackDeliveryController@saveHistory');
		Route::any('/{id?}', 'TrackDeliveryController@index');
	});

	Route::group(['prefix' => 'track-statuses'], function(){

		Route::get('/', 'TrackStatusesController@index');
		Route::get('/form/{id?}', 'TrackStatusesController@form');
		Route::post('/save', 'TrackStatusesController@save');
	});
	
});

Route::group(['prefix' => 'places'], function(){

    Route::get('search/{address?}', 'PlacesController@search');
    Route::get('all', 'PlacesController@places');
    Route::any('locations', 'PlacesController@locations');
});
