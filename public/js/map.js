function initMap() {

  let directionsService = new google.maps.DirectionsService();
  let directionsDisplay = new google.maps.DirectionsRenderer();

  let options = { center: new google.maps.LatLng(6.6038838, 3.3415067), zoom: 15, mapTypeId: google.maps.MapTypeId.ROADMAP };

  let map = new google.maps.Map(document.getElementById('map'), options);

  directionsDisplay.setMap(map);

  let origin = 'Gbagada, Lagos, Nigeria';
  let destination = 'Somolu, Lagos, Nigeria';

  let request = { origin: origin, destination: destination, travelMode: 'DRIVING' };

  directionsService.route(request, function(response, status) {

    if(status == 'OK') 
      directionsDisplay.setDirections(response);

  });

}