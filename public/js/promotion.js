var app = new Vue({

	el: '#promotion-app',

	data: {

		validity : validity,
		promocode : promocode
	},
	methods : {

		generate_code : function(){

			this.promocode = `V-P-${Math.floor(Date.now() / 1000 * Math.random()).toString().substring(0, 8)}`
		}
	},
	created : function(){

		setTimeout(() => {

			$(this.$refs.user).tooltip({template: '<div class="tooltip tooltip-primary" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'});
			$(this.$refs.type_ref).tooltip({template: '<div class="tooltip tooltip-primary" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'});
			$(this.$refs.limit).tooltip({template: '<div class="tooltip tooltip-primary" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'});
			$(this.$refs.users).select2({ placeholder: 'Select Users(s)' });
		})
	}

});