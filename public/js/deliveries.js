var app = new Vue({

  el: '#delivery-app',

  data: {

    base_url : `${base_url}/admin/deliveries/api`,
    all_deliveries: [],
    deliveries : [],
    pending : '',
    active : '',
    completed : '',
    id : 0,
    delivery : {},
    error : '',
    assign_driver : false,
    driver_id : 0,
    searching : false,
    driver_param : '',
    drivers : [],
    show_promo : false,
    promo_param : '',
    complete : {
      delivery_id : 0,
      show : false,
      total : 0,
      total_km : 0,
      total_minutes : 0,
      discount : 0,
      discount_text : '',
      add_payment : true
    }
  },
  methods : {

    init : function(){

      $.get(`${this.base_url}`, response => {

        let data = response.data;
        this.all_deliveries = this.deliveries = data.deliveries;
        this.pending = data.pending;
        this.active = data.active;
        this.completed = data.completed;

      });

      if(id > 0){

        this.get_delivery(id);
        this.id = id;
      }

      this.search_drivers();
    },

    filter_deliveries : function(status = 'pending'){

      this.deliveries = this.all_deliveries.filter(row => row.status.toLowerCase() === status.toLowerCase());
    },

    get_delivery : function(id){

      this.id = id;
      this.assign_driver = this.show_promo = this.complete.show = false;
      this.delivery = {};
      this.complete.show = false;
      this.complete.total = this.complete.total_km = this.complete.total_minutes = 0;
      this.complete.discount = '';

      $.get(`${this.base_url}/delivery/${this.id}`, response => {

        response.status == false ? response.data.toString() : this.delivery = response.data;

        setTimeout(() => {

          this.map();
          new PerfectScrollbar('.az-contact-info-body', { suppressScrollX: true });

        });


      }).catch(error => {

        window.location = `${base_url}/admin/deliveries`;

      });
    },

    show_assign_driver : function(){

      this.assign_driver = true
    },

    search_drivers : function(){

      this.searching = true;

      let vehicle_type_id = this.delivery.vehicle_type_id == undefined ? 0 : this.delivery.vehicle_type_id;

      $.get(`${this.base_url}/search-drivers/${vehicle_type_id}?param=${this.driver_param}`, response => {

        this.drivers = response.data;
        this.searching = false;

      });
    },

    assign : function(driver_id = 0){

      swal({

        title       : `You are about to assign a Driver to this Delivery.`,
        text        : `Once Assigned, you will not be able to assign any other driver and this Driver would be notified of this delivery immediately.`,
        icon        : 'warning',
        buttons     : true,
        dangerMode: true

      }).then(response => {

        if (response) {

          $.post(`${this.base_url}/assign-driver/${driver_id}`, {id : this.delivery.id, driver_id : driver_id}, response => {

            swal(response.data, { icon: response.status == true ? 'success' : 'warning', });

            this.init();
            this.get_delivery(this.delivery.id);

          })
        }

      });
    },

    auto_assign_driver : function(){

      swal({

        title       : `You are about to Auto Assign a Driver to this Delivery.`,
        text        : `Once Completed, you will not be able to assign any other driver manually and the selected Driver would be notified of this delivery immediately.`,
        icon        : 'warning',
        buttons     : true,
        dangerMode: true

      }).then(response => {

        if (response) {

          $.post(`${this.base_url}/auto-assign-driver`, {id : this.delivery.id}, response => {

            swal(response.data, { icon: response.status == true ? 'success' : 'error', });

            this.init();
            this.get_delivery(this.delivery.id);

          })
        }

      });

    },

    add_payment : function(){

      swal({

        title       : `You are about to Mark this Delivery as paid`,
        text        : `This delivery would be marked as cash and a Unique reference number would be automatically generated.`,
        icon        : 'info',
        buttons     : true,
        dangerMode: true

      }).then(response => {

        if (response) {

          $.post(`${this.base_url}/cash-payment`, {id : this.delivery.id}, response => {

            swal(response.data, { icon: response.status == true ? 'success' : 'error', });

            this.init();
            this.get_delivery(this.delivery.id);

          })
        }

      });

    },

    open_invoice : function(){

      window.open(`${base_url}/admin/deliveries/invoice/${this.delivery.id}`, this.delivery.item, 'resizable,scrollbars');
    },

    map : function() {

      if(!this.$refs.map)
        return;

      if(this.delivery.pickup_place == undefined || this.delivery.delivery_place == undefined)
        return;

      let pickup = this.delivery.pickup_place;
      let delivery = this.delivery.delivery_place;

      let directionsService = new google.maps.DirectionsService();
      let directionsDisplay = new google.maps.DirectionsRenderer();

      let options = { center: new google.maps.LatLng(pickup.latitude, pickup.longitude), zoom: 15, mapTypeId: google.maps.MapTypeId.ROADMAP };

      let map = new google.maps.Map(this.$refs.map, options);

      directionsDisplay.setMap(map);

      let request = { origin: pickup.address, destination: delivery.address, travelMode: 'DRIVING' };

      directionsService.route(request, function(response, status) {

        if(status == 'OK') 
          directionsDisplay.setDirections(response);

      });

    },

    show_promo_code : function(){

      this.show_promo = true;
    },

    add_promo_code : function(){

      $.get(`${this.base_url}/add-promotion-code/${this.delivery.id}/${this.promo_param}`, response => {

        swal(response.data, { icon: response.status == true ? 'success' : 'error' });
        this.get_delivery(this.delivery.id);
        this.promo_param = '';

      });
    },

    show_complete : function(){

      this.complete.show = true;
      this.complete.total_km = this.delivery.approx_km;
      this.complete.delivery_id = this.delivery.id;
    },

    calculate_total : function(){

      if(this.complete.total_km < 1 || this.complete.total_minutes < 1){

        this.complete.total = this.compelete.discount = 0;
        this.complete.discount_text = '';
        return;
      }

      let payload = {id : this.delivery.id, total_km : this.complete.total_km, total_minutes : this.complete.total_minutes};

      $.post(`${this.base_url}/calculate-delivery`, payload, response => {

        if(response.status == true)
          this.complete.total = response.data.total;

        if(response.data.discount > 0){

          this.complete.total = response.data.total - response.data.discount;
          this.complete.discount = response.data.discount;
          this.complete.discount_text = `A discount of ${response.data.discount_currency} has been applied due to Promotion Code on this Delivery`;
        }

      })
    },

    complete_delivery : function(){

      swal({

        title       : `You are about to Complete this Delivery with a Total of ${this.complete.total}`,
        text        : `This operation cannot be Undone.`,
        icon        : 'info',
        buttons     : true,
        dangerMode: true

      }).then(response => {

        if (response) {

          $.post(`${this.base_url}/complete-delivery`, this.complete, response => {

            swal(response.data, { icon: response.status == true ? 'success' : 'error', });

            this.init();
            this.get_delivery(this.delivery.id);

          })
        }

      });

    }

  },
  
  created : function(){

    this.init();
    
  },

});