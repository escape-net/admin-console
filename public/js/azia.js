$(function(){
  'use strict'

  // This template is mobile first so active menu in navbar
  // has submenu displayed by default but not in desktop
  // so the code below will hide the active menu if it's in desktop
  if(window.matchMedia('(min-width: 992px)').matches) {
    $('.az-navbar .active').removeClass('show');
  }

  // Shows header dropdown while hiding others
  $('.az-header .dropdown > a').on('click', function(e) {
    e.preventDefault();
    $(this).parent().toggleClass('show');
    $(this).parent().siblings().removeClass('show');
  });

  // Showing submenu in navbar while hiding previous open submenu
  $('.az-navbar .with-sub').on('click', function(e) {
    e.preventDefault();
    $(this).parent().toggleClass('show');
    $(this).parent().siblings().removeClass('show');
  });

  // this will hide dropdown menu from open in mobile
  $('.dropdown-menu .az-header-arrow').on('click', function(e){
    e.preventDefault();
    $(this).closest('.dropdown').removeClass('show');
  });

  // this will show navbar in left for mobile only
  $('#azNavShow, #azNavbarShow').on('click', function(e){
    e.preventDefault();
    $('body').addClass('az-navbar-show');
  });

  // this will hide currently open content of page
  // only works for mobile
  $('#azContentLeftShow').on('click touch', function(e){
    e.preventDefault();
    $('body').addClass('az-content-left-show');
  });

  // This will hide left content from showing up in mobile only
  $('#azContentLeftHide').on('click touch', function(e){
    e.preventDefault();
    $('body').removeClass('az-content-left-show');
  });

  // this will hide content body from showing up in mobile only
  $('#azContentBodyHide').on('click touch', function(e){
    e.preventDefault();
    $('body').removeClass('az-content-body-show');
  })

  // navbar backdrop for mobile only
  $('body').append('<div class="az-navbar-backdrop"></div>');
  $('.az-navbar-backdrop').on('click touchstart', function(){
    $('body').removeClass('az-navbar-show');
  });

  // Close dropdown menu of header menu
  $(document).on('click touchstart', function(e){
    e.stopPropagation();

    // closing of dropdown menu in header when clicking outside of it
    var dropTarg = $(e.target).closest('.az-header .dropdown').length;
    if(!dropTarg) {
      $('.az-header .dropdown').removeClass('show');
    }

    // closing nav sub menu of header when clicking outside of it
    if(window.matchMedia('(min-width: 992px)').matches) {
      var navTarg = $(e.target).closest('.az-navbar .nav-item').length;
      if(!navTarg) {
        $('.az-navbar .nav-item').removeClass('show');
      }
    }
  });


});


$('#datatable1').DataTable({

  responsive: true,
  language: { searchPlaceholder: 'Search...', sSearch: '', lengthMenu: '_MENU_ items/page', }

});

$('.dropify').dropify();
$('.select2').select2({ placeholder: 'Select Option(s)' });

$('.az-sidebar .with-sub').on('click', function(e){
  e.preventDefault();
  $(this).parent().toggleClass('show');
  $(this).parent().siblings().removeClass('show');
})

$(document).on('click touchstart', function(e){

  e.stopPropagation();

  if(!$(e.target).closest('.az-header-menu-icon').length) {
    var sidebarTarg = $(e.target).closest('.az-sidebar').length;
    if(!sidebarTarg) {
      $('body').removeClass('az-sidebar-show');
    }
  }
});




$('#azSidebarToggle').on('click', function(e){
  e.preventDefault();

  if(window.matchMedia('(min-width: 992px)').matches) {
    $('body').toggleClass('az-sidebar-hide');
  } else {
    $('body').toggleClass('az-sidebar-show');
  }
})

$('[data-toggle="tooltip"]').tooltip();

$('[data-toggle="tooltip-primary"]').tooltip({
  template: '<div class="tooltip tooltip-primary" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
});

$('[data-toggle="tooltip-secondary"]').tooltip({
  template: '<div class="tooltip tooltip-secondary" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
});
