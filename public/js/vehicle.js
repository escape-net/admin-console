var app = new Vue({

  el: '#vehicle-app',

  data: {

    tab : 'summary'
  },
  methods : {

    change_tab : function(tab){

      this.tab = tab;

    }
  }
  
});