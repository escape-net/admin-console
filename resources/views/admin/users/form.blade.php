@extends('admin.app')

@section('body')

<div class="az-content az-content-body-dashboard-three">
  <div class="container">
    <div class="az-content-body">
      <div class="az-content-breadcrumb">
        <span>Velocity Admin</span>
        <span>User</span>
      </div>

      <div class="row">
        <div class="col-md-8">
          <h2 class="az-content-title">User Form</h2>
        </div>
      </div>
      

      <form action="{{url('admin/users/save')}}" method="POST" role="form">

        {{csrf_field()}}

        <div class="row">

          <div class="form-group col-6">
            <label>First Name</label>
            <input type="text" name="first_name" class="form-control" placeholder="First Name" value="{{old('first_name', $user->first_name)}}" required>
            @if($errors->has('first_name'))
            <span class="form-error">{{$errors->first('first_name')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label>Last Name</label>
            <input type="text" name="last_name" class="form-control" placeholder="Last Name" value="{{old('last_name', $user->last_name)}}" required>
            @if($errors->has('last_name'))
            <span class="form-error">{{$errors->first('last_name')}}</span>
            @endif
          </div>

        </div>

        <div class="row">

          <div class="form-group col-6">
            <label>Email</label>
            <input type="text" name="email" class="form-control" placeholder="Email Address" value="{{old('email', $user->email)}}" required>
            @if($errors->has('email'))
            <span class="form-error">{{$errors->first('email')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label>Phone</label>
            <input type="text" name="phone" class="form-control" placeholder="Phone Number" value="{{old('phone', $user->phone)}}" required>
            @if($errors->has('phone'))
            <span class="form-error">{{$errors->first('phone')}}</span>
            @endif
          </div>

           <div class="form-group col-12">
            <label>User Status</label>
            <select name="status" class="form-control" required>

              @if($user->status == 'active')
              <option value="1" selected>Active</option>
              <option value="0">Inactive</option>
              @else
              <option value="1">Active</option>
              <option value="0" selected>Inactive</option>
              @endif
              
            </select>
            @if($errors->has('status'))
            <span class="form-error">{{$errors->first('status')}}</span>
            @endif
          </div>

        </div>

       
        <input type="hidden" name="id" value="{{$user->id}}">
        <button type="submit" class="btn btn-indigo btn-rounded"><i class="fa fa-save"></i> Submit</button>

      </form>

    </div>
  </div>
</div>

@endsection

