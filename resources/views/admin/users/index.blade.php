@extends('admin.app')

@section('body')

<div class="az-content">
  <div class="container">
    <div class="az-content-body">
      <div class="az-content-breadcrumb">
        <span>Velocity Admin</span>
        <span>Users</span>
      </div>

      <div class="row">
        <div class="col-md-8">
          <h2 class="az-content-title">Users</h2>

        </div>
        <div class="col-md-4">
          <a class="btn btn-indigo btn-rounded" style="float:right" href="{{url('admin/users/form')}}"><i class="fa fa-plus"></i> Add new</a>
        </div>
      </div>

      <table id="datatable1" class="display responsive nowrap">
        <thead>
          <tr>
            <th class="wd-15p">Name</th>
            <th class="wd-15p">Email</th>
            <th class="wd-20p">Phone</th>
            <th class="wd-20p">Deliveries</th>
            <th class="wd-25p">Action</th>
          </tr>
        </thead>
        <tbody>

          @foreach($users as $row)
          <tr>
            <td>{{$row->name}}</td>
            <td>{{$row->email}}</td>
            <td>{{$row->phone}}</td>
            <td>{{$row->user_deliveries->count()}}</td>
            <td>
              
              <div class="btn-group btn-sm" role="group" aria-label="Actions">
                <a href="{{url('admin/users/form/'.$row->id)}}" class="btn btn-indigo btn-icon"><i class="typcn typcn-pencil"></i></a>
               {{--  <a href="#" class="btn btn-indigo btn-icon"><i class="typcn typcn-eye-outline"></i></a> --}}
              </div>
              
            </td>
          </tr>
          @endforeach

        </tbody>
      </table>

    </div>
  </div>
</div>

@endsection

