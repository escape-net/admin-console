<div class="az-header">
  <div class="container-fluid">
    <div class="az-header-left">
      <a href="#" id="azSidebarToggle" class="az-header-menu-icon"><span></span></a>
    </div>

    <div class="az-header-center">

      <form method="post" action="{{url('admin/deliveries/search')}}">
        {{csrf_field()}}
        <input type="text" class="form-control" placeholder="Search Deliveries" name="query" required>
        <button class="btn" type="submit"><i class="fas fa-search"></i></button>
      </form>

    </div>

    <div class="az-header-right">

      {{-- <div class="dropdown az-header-notification">
        <a href="#" class="new"><i class="typcn typcn-bell"></i></a>
        <div class="dropdown-menu">
          <div class="az-dropdown-header mg-b-20 d-sm-none">
            <a href="#" class="az-header-arrow"><i class="icon ion-md-arrow-back"></i></a>
          </div>
          <h6 class="az-notification-title">Notifications</h6>
          <p class="az-notification-text">You have 0 unread notification</p>
          <div class="az-notification-list">
            <div class="media new">
              <div class="az-img-user online"><img src="{{asset('img/driver.png')}}" alt=""></div>
              <div class="media-body">
                <p><strong>Joyce Chua</strong> just created a new blog post</p>
                <span>Mar 13 04:16am</span>
              </div>
            </div>
          </div>
          <div class="dropdown-footer"><a href="#">View All Notifications</a></div>
        </div>
      </div> --}}
      <div class="dropdown az-profile-menu">
        <a href="#" class="az-img-user"><img src="{{asset('img/driver.png')}}" alt=""></a>
        <div class="dropdown-menu">
          <div class="az-dropdown-header d-sm-none">
            <a href="#" class="az-header-arrow"><i class="icon ion-md-arrow-back"></i></a>
          </div>
          <div class="az-header-profile">
            <div class="az-img-user">
              <img src="{{asset('img/driver.png')}}" alt="">
            </div>
            <h6>{{Auth::user()->name}}</h6>
            <span>Administrator</span>
          </div>

          <a href="{{url('admin/administrators/form/'.Auth::user()->id)}}" class="dropdown-item"><i class="typcn typcn-user-outline"></i> My Profile</a>
          <a href="{{url('logout')}}" class="dropdown-item"><i class="typcn typcn-power-outline"></i> Sign Out</a>
        </div>
      </div>
    </div>
  </div>
</div>