@if(session('message'))

<div class="alert alert-solid-success" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	{{session('message')}}
</div>

@endif

@if(session('error'))

<div class="alert alert-solid-danger" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	{{session('error')}}
</div>

@endif