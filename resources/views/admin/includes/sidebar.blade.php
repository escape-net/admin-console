<div class="az-sidebar">
  <div class="az-sidebar-header">
    <a href="{{url('admin')}}" class="az-logo"><img src="{{asset('img/logo-white.png')}}" /> VELOCITY</a>
  </div>
  <div class="az-sidebar-loggedin">
    <div class="az-img-user online"><img src="{{asset('img/pofile.png')}}" alt="{{Auth::user()->name}}"></div>
    <div class="media-body">
      <h6>{{Auth::user()->name}}</h6>
      <span>Administrator</span>
    </div>
  </div>
  <div class="az-sidebar-body">
    <ul class="nav">
      <li class="nav-label">Main Menu</li>
      
      <li class="nav-item active show">
        <a href="{{url('admin')}}" class="nav-link">
          <i class="typcn typcn-clipboard"></i>Dashboard
        </a>
      </li>
      
      
      <li class="nav-item">
        <a href="{{url('admin')}}" class="nav-link with-sub">
          <i class="typcn typcn-dropbox"></i> Deliveries
        </a>
        <nav class="nav-sub">
          <a href="{{url('admin/deliveries')}}" class="nav-sub-link">All Deliveries</a>
          @can('write', \App\Delivery::class)
          <a href="{{url('admin/deliveries/form')}}" class="nav-sub-link">Instant Delivery</a>
          <a href="{{url('admin/deliveries/form')}}" class="nav-sub-link">Scheduled Delivery</a>
          @endcan
          <a href="#" class="nav-sub-link">Recurring Deliveries</a>
          {{-- <a href="{{url('admin/track-delivery')}}" class="nav-sub-link">Track Delivery</a> --}}
          
        </nav>
      </li>
      
      @can('write', \App\Vehicle::class)
      <li class="nav-item">
        <a href="{{url('admin/vehicles')}}" class="nav-link">
          <i class="typcn typcn-directions"></i> Vehicles
        </a>
      </li>
      @endcan

      <li class="nav-item">
        <a href="{{url('admin/users')}}" class="nav-link">
          <i class="typcn typcn-group"></i> Users
        </a>
      </li>

      <li class="nav-item">
        <a href="{{url('admin/drivers')}}" class="nav-link">
          <i class="typcn typcn-user-add-outline"></i> Drivers
        </a>
      </li>

      <li class="nav-item">
        <a href="#" class="nav-link">
          <i class="typcn typcn-document-text"></i> Reviews
        </a>
      </li>

      @can('view-gods-eye', \App\Delivery::class)
      <li class="nav-item">
        <a href="{{url('admin/godsview')}}" class="nav-link">
          <i class="typcn typcn-world"></i> god's View
        </a>
      </li>
      @endcan
      
      <li class="nav-item">
        <a href="{{url('admin/transactions')}}" class="nav-link">
          <i class="typcn typcn-credit-card"></i> Transactions
        </a>
      </li>

      @can('view', \App\Promotion::class)
      <li class="nav-item">
        <a href="{{url('admin/promotions')}}" class="nav-link">
          <i class="typcn typcn-link-outline"></i> Promotions
        </a>
      </li>
      @endcan
      
      @can('view-admin-menu', \App\User::class)
      <li class="nav-item">
        <a href="{{url('admin')}}" class="nav-link with-sub">
          <i class="typcn typcn-folder-add"></i> Administration
        </a>
        <nav class="nav-sub">
          @can('write', \App\Company::class)
          <a href="{{url('admin/companies')}}" class="nav-sub-link">Companies</a>
          @endcan

          @can('view', \App\VehicleType::class)
          <a href="{{url('admin/vehicle-types')}}" class="nav-sub-link">Vehicle Types</a>
          @endcan

          @can('write', \App\Company::class)
          <a href="{{url('admin/track-statuses')}}" class="nav-sub-link">Track Delivery Statuses</a>
          {{-- <a href="#" class="nav-sub-link">Settings</a> --}}
          <a href="{{url('admin/administrators')}}" class="nav-sub-link">Administrators</a>
          <a href="{{url('admin/otps')}}" class="nav-sub-link">OTPs & Tokens</a>
          <a href="{{url('admin/api-logs')}}" class="nav-sub-link">Api Logs</a>
          @endcan
          
          <a href="#" class="nav-sub-link">Reports</a>
        </nav>
      </li>
      @endcan
      
    </ul>
  </div>
</div>