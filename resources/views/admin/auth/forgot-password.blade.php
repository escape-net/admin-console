@extends('admin.auth.app')

@section('body')

<div class="az-card-signin" style="background:#fff">
  <h1 class="az-logo"><span><img src="{{asset('img/logo.png')}}"/> VELOCITY ADMIN-CONSOLE<span></h1>
    <div class="az-signin-header">
      <h2>Forgot Password</h2>
      <h4>Don't worry, we all forget passwords all the time.</h4>

      <form action="{{url('forgot-password')}}" method="post">

        {{csrf_field()}}

        @include('admin.includes.alert')

        <div class="form-group">
          <label>Email</label>
          <input type="email" name="email" class="form-control" placeholder="Enter your email" required>
        </div>
        <div class="form-group">
          <label>Enter the Last Passwor you remember</label>
          <input type="password" name="password" class="form-control" placeholder="Last password you remember">
        </div>
        <button type="submit" class="btn btn-az-primary btn-block">Reset Password</button>
      </form>
    </div>
    <div class="az-signin-footer">
      <p>Or <a href="{{url('login')}}">Sign In</a></p>
    </div>
  </div>


  @endsection