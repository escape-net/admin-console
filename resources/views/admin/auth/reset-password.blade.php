@extends('admin.auth.app')

@section('body')

<div class="az-card-signin" style="background:#fff">
  <h1 class="az-logo"><span><img src="{{asset('img/logo.png')}}"/> VELOCITY ADMIN-CONSOLE<span></h1>
    <div class="az-signin-header">
      <h2>Reset Password</h2>
      <h4>Remember to set a Strong Password.</h4>

      <form action="{{url('change-password')}}" method="post">

        {{csrf_field()}}

        @include('admin.includes.alert')

        <div class="form-group">
          <label>Email</label>
          <input type="email" class="form-control" value="{{$user->email}}" disabled>
        </div>

        <div class="form-group">
          <label>New Password</label>
          <input type="password" name="password" class="form-control" placeholder="New Password" required>
          @if($errors->has('password'))
          <span class="form-error">{{$errors->first('password')}}</span>
          @endif
        </div>

        <div class="form-group">
          <label>Confirm New Password</label>
          <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm New Password" required>
        </div>

        <input type="hidden" name="token" value="{{$user->remember_token}}">
        <input type="hidden" name="id" value="{{$user->id}}">

        <button type="submit" class="btn btn-az-primary btn-block">Continue</button>
      </form>
    </div>
    <div class="az-signin-footer">
      <p>Or <a href="{{url('login')}}">Sign In</a></p>
    </div>
  </div>


  @endsection