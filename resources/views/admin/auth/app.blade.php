<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">    
  <meta name="description" content="Velocity Logistics">
  <meta name="author" content="Olaiya Segun <vadeshayo@gmail.com>">
  <title>velocity.ng - Authentication</title>
  <link href="{{asset('/')}}lib/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="{{asset('/')}}lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="{{asset('/')}}lib/typicons.font/typicons.css" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('/')}}css/azia.css">

</head>
<body class="az-body">

  <div class="az-signin-wrapper" style="background:linear-gradient(88deg, #52009f, #570a9ee8)">

    @yield('body')

  </div>

  <script src="{{asset('/')}}lib/jquery/jquery.min.js"></script>
  <script src="{{asset('/')}}lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="{{asset('/')}}lib/ionicons/ionicons.js"></script>

  <script src="{{asset('/')}}js/azia.js"></script>
</body>
</html>
