@extends('admin.auth.app')

@section('body')

<div class="az-card-signin" style="background:#fff">
  <h1 class="az-logo"><span><img src="{{asset('img/logo.png')}}"/> VELOCITY ADMIN-CONSOLE<span></h1>
    <div class="az-signin-header">
      <h2>Welcome back!</h2>
      <h4>Please Sign In to continue</h4>

      <form action="{{url('login')}}" method="post">

        {{csrf_field()}}

        @include('admin.includes.alert')

        <div class="form-group">
          <label>Email</label>
          <input type="email" name="email" class="form-control" placeholder="Enter your email" required>
        </div>
        <div class="form-group">
          <label>Password</label>
          <input type="password" name="password" class="form-control" placeholder="Enter your password">
        </div>
        <button type="submit" class="btn btn-az-primary btn-block">Sign In</button>
      </form>
    </div>
    <div class="az-signin-footer">
      <p><a href="{{url('forgot-password')}}">Forgot password?</a></p>
    </div>
  </div>


  @endsection