@extends('admin.app')

@section('body')

<div class="az-content az-content-body-dashboard-three">
  <div class="container">
    <div class="az-content-body">
      <div class="az-content-breadcrumb">
        <span>Velocity Admin</span>
        <span>Administration</span>
        <span>Vehicle Type</span>
      </div>

      <div class="row">
        <div class="col-md-8">
          <h2 class="az-content-title">Vehicle Type Form</h2>
        </div>
      </div>
      

      <form action="{{url('admin/vehicle-types/save')}}" method="POST" role="form">

        {{csrf_field()}}

        <div class="row">

         
          <div class="form-group col-6">
            <label>Vehicle Type Name</label>
            <input type="text" name="name" class="form-control" placeholder="Vehicle Type Name" value="{{old('name', $vehicle_type->name)}}" required>
            @if($errors->has('name'))
            <span class="form-error">{{$errors->first('name')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label>Base Fare</label>
            <input type="number" name="base_fare" class="form-control" placeholder="Base Fare" value="{{old('base_fare', $vehicle_type->base_fare)}}" step="0.1" required>
            @if($errors->has('base_fare'))
            <span class="form-error">{{$errors->first('base_fare')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label>Cost Per Minute</label>
            <input type="number" name="per_minute" class="form-control" placeholder="Cost Per Minute" value="{{old('per_minute', $vehicle_type->per_minute)}}" step="0.1" required>
            @if($errors->has('per_minute'))
            <span class="form-error">{{$errors->first('per_minute')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label>Cost Per KM</label>
            <input type="text" name="per_km" class="form-control" placeholder="Vehicle Per KM" value="{{old('per_km', $vehicle_type->per_km)}}" step="0.1" required>
            @if($errors->has('per_km'))
            <span class="form-error">{{$errors->first('per_km')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label>Commmission Percentage</label>
            <input type="text" name="commission_percentage" class="form-control" placeholder="Commission Percentage" value="{{old('commission_percentage', $vehicle_type->commission_percentage)}}" step="0.1" required>
            @if($errors->has('commission_percentage'))
            <span class="form-error">{{$errors->first('commission_percentage')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label>Cancellation Fee</label>
            <input type="text" name="cancellation_fee" class="form-control" placeholder="Commission Percentage" value="{{old('cancellation_fee', $vehicle_type->cancellation_fee)}}" step="0.1" required>
            @if($errors->has('cancellation_fee'))
            <span class="form-error">{{$errors->first('cancellation_fee')}}</span>
            @endif
          </div>

        </div>

        <input type="hidden" name="id" value="{{$vehicle_type->id}}">
        <button type="submit" class="btn btn-indigo btn-rounded"><i class="fa fa-save"></i> Submit</button>

      </form>

    </div>
  </div>
</div>

@endsection

