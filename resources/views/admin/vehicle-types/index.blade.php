@extends('admin.app')

@section('body')

<div class="az-content">
  <div class="container">
    <div class="az-content-body">
      <div class="az-content-breadcrumb">
        <span>Velocity Admin</span>
        <span>Administration</span>
        <span>Vehicle Types</span>
      </div>

      <div class="row">
        <div class="col-md-8">
          <h2 class="az-content-title">Vehicle Types</h2>

        </div>
        @can('write', \App\VehicleType::class)
        <div class="col-md-4">
          <a class="btn btn-indigo btn-rounded" style="float:right" href="{{url('admin/vehicle-types/form')}}"><i class="fa fa-plus"></i> Add new</a>
        </div>
        @endcan
      </div>


      <table id="datatable1" class="display responsive nowrap">
        <thead>
          <tr>
            <th class="wd-15p">Name</th>
            <th>Vehicles</th>
            <th>Base Fare</th>
            <th>Per Minute</th>
            <th>Per KM</th>
            <th>Commission</th>
            @can('write', \App\VehicleType::class)
            <th>Action</th>
            @endcan
          </tr>
        </thead>
        <tbody>

          @foreach($vehicle_types as $row)
          <tr>
            <td>{{$row->name}}</td>
            <td>{{$row->vehicles->count()}}</td>
            <td>{{$row->base_fare}}</td>
            <td>{{$row->per_minute}}</td>
            <td>{{$row->per_km}}</td>
            <td>{{$row->commission_percentage}}%</td>
            @can('write', \App\VehicleType::class)
            <td>
              <a href="{{url('admin/vehicle-types/form/'.$row->id)}}" class="btn btn-indigo btn-icon btn-sm">
                <i class="typcn typcn-pencil"></i>
              </a>
            </td>
            @endcan
          </tr>
          @endforeach

        </tbody>
      </table>

    </div>
  </div>
</div>

@endsection

