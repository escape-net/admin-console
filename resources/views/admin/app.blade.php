<!DOCTYPE html>
<html lang="en">
<head>    
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="velocity.ng">
  <meta name="author" content="Escape Technologies">
  <title>velocity.ng</title>

  <link href="{{asset('lib')}}/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="{{asset('lib')}}/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="{{asset('lib')}}/typicons.font/typicons.css" rel="stylesheet">
  <link href="{{asset('lib')}}/morris.js/morris.css" rel="stylesheet">
  <link href="{{asset('lib')}}/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
  <link href="{{asset('lib')}}/jqvmap/jqvmap.min.css" rel="stylesheet">
  <link href="{{asset('/')}}lib/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">
  <link href="{{asset('/')}}lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css" rel="stylesheet">
  <link href="{{asset('/')}}css/animate.css" rel="stylesheet">
  <link href="{{asset('/')}}lib/dropify/css/dropify.css" rel="stylesheet">
  <link href="{{asset('/')}}lib/select2/css/select2.min.css" rel="stylesheet">
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script type="text/javascript">
    let base_url = '{{url('/')}}';
  </script>
  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  <link rel="stylesheet" href="{{asset('css')}}/azia.css?v=1.1">

</head>
<body class="az-body az-body-sidebar">

  @include('admin.includes.sidebar')

  <div class="az-content">

    @include('admin.includes.header')

    @include('admin.includes.alert')

    @yield('body')

    <div class="az-footer">
      <div class="container-fluid">
        <span>&copy; {{date('Y')}} velocity.ng</span>
        <span>Developed by: <a href="https://twitter.com/massivebrains00" target="_blank">Escape Technologies</a></span>
      </div>
    </div>
  </div>

  <script src="{{asset('lib')}}/jquery/jquery.min.js"></script>
  <script src="{{asset('lib')}}/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="{{asset('lib')}}/ionicons/ionicons.js"></script>
  <script src="{{asset('/')}}lib/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="{{asset('/')}}lib/datatables.net-dt/js/dataTables.dataTables.min.js"></script>
  <script src="{{asset('/')}}lib/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
  <script src="{{asset('/')}}lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js"></script>
  <script src="{{asset('/')}}lib/dropify/js/dropify.min.js"></script>
  <script src="{{asset('/')}}lib/select2/js/select2.min.js"></script>
  <script src="{{asset('lib/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
  <script src="{{asset('lib/jquery.maskedinput/jquery.maskedinput.js')}}"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.core.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_API_KEY')}}&libraries=places,drawing,directions"></script>
  <script src="{{asset('js')}}/azia.js?v=1.3"></script>
  <script>

    $(function(){

      'use strict'

      
      $('.date').mask('99-99-9999');

      $('.place').select2({

        ajax:{

          url : '{{url('places/search')}}',
          dataType: 'json',
          delay : 250,
          cache : true,
          placeholder : 'Search for Places',
          minimumInputLength : 5
        }

      })
      
    });

    function openPopup(url = '', title = '') {

      window.open(url, title, 'resizable,scrollbars,status');

    }

  </script>

  @yield('script')
</body>
</html>