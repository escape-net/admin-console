@extends('admin.app')

@section('body')

<div class="az-content">
  <div class="container">
    <div class="az-content-body">
      <div class="az-content-breadcrumb">
        <span>Velocity Admin</span>
        <span>One Time Pins</span>
      </div>

      <div class="row">
        <div class="col-md-12">
          <h2 class="az-content-title">Recently Updated One Time Pins</h2>
        </div>
      </div>
      

      <table id="datatable1" class="display responsive nowrap">
        <thead>
          <tr>
            <th class="wd-20p">&nbsp;</th>
            <th class="wd-20p">Last Updated</th>
            <th class="wd-15p">User</th>
            <th class="wd-15p">Phone</th>
            <th class="wd-15p">OTP</th>
            <th class="wd-15p">Api Token</th>
            <th class="wd-15p">Device Token</th>
          </tr>
        </thead>
        <tbody>

          @php $i = 1; @endphp

          @foreach($users as $row)
          <tr>
            <td>{{$i++}}</td>
            <td>{{_d($row->updated_at, true)}}</td>
            <td>{{$row->name}}</a></td>
            <td>{{$row->phone}}</a></td>
            <td>{{$row->otp}}</td>
            <td>{{$row->api_token}}</td>
            <td>{{str_limit($row->device_token, 10, '...')}}</td>
          </tr>
          @endforeach

        </tbody>
      </table>

    </div>
  </div>
</div>

@endsection