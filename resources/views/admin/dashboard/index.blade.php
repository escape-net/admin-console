@extends('admin.app')

@section('body')


<div class="az-content-header d-block d-md-flex">
  <div>
    <h2 class="az-content-title mg-b-5 mg-b-lg-8">Hi {{Auth::user()->first_name}}, welcome back!</h2>
    <p class="mg-b-0">There are currently {{$online}} {{str_plural('Driver', $online)}} Online.</p>
  </div>
  <div class="az-dashboard-header-right">
    <div>
      <label class="tx-13">Drivers Ratings</label>
      <div class="az-star">
        <i class="typcn typcn-star active"></i>
        <i class="typcn typcn-star active"></i>
        <i class="typcn typcn-star active"></i>
        <i class="typcn typcn-star active"></i>
        <i class="typcn typcn-star"></i>
        <span>({{number_format($total_ratings)}})</span>
      </div>
    </div>
    <div>
      <label class="tx-13">Completed Deliveries</label>
      <h5>{{number_format($completed_deliveries)}}</h5>
    </div>
    <div>
      <label class="tx-13">Active Deliveries</label>
      <h5>{{number_format($active_deliveries)}}</h5>
    </div>
  </div>
</div>

<div class="az-content-body">
  <div class="card card-dashboard-seven">
    <div class="card-header">
      <div class="row row-sm">
        <div class="col-6 col-md-4 col-xl">
          <div class="media">
            <div><i class="icon ion-ios-people"></i></div>
            <div class="media-body">
              <label>Top Driver</label>
              <div class="date">
                <span>{{$top_driver->name}}</span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-6 col-md-4 col-xl">
          <div class="media">
            <div><i class="icon ion-ios-person"></i></div>
            <div class="media-body">
              <label>Top User</label>
              <div class="date">
                <span>{{$top_user->name}}</span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-6 col-md-4 col-xl mg-t-15 mg-md-t-0">
          <div class="media">
            <div><i class="icon ion-md-stats"></i></div>
            <div class="media-body">
              <label>Completed Deliveries this Week</label>
              <div class="date">
                <span>{{number_format($completed_deliveries_this_week)}}</span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-6 col-md-4 col-xl mg-t-15 mg-xl-t-0">
          <div class="media">
            <div><i class="icon ion-md-stats"></i></div>
            <div class="media-body">
              <label>Completed Deliveries this Month</label>
              <div class="date">
                <span>{{number_format($completed_deliveries_this_month)}}</span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-xl mg-t-15 mg-xl-t-0">
          <div class="media">
            <div><i class="icon ion-md-stats"></i></div>
            <div class="media-body">
              <label>Scheduled Deliveries</label>
              <div class="date">
                <span>{{number_format($scheduled_deliveries)}}</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    @can('view-revenue', \App\Company::class)
    <div class="card-body">
      <div class="row row-sm">
        <div class="col-6 col-lg-3">
          <label class="az-content-label">Revenue This Month</label>
          <h2>{{_c($revenue_this_month)}}</h2>
          <div class="desc up">
            <i class="icon ion-md-stats"></i>
            <span>(Calendar Month)</span>
          </div>
          <span id="compositeline2">5,9,5,6,4,12,18,14,10,15,12,5,8,5,12,5,12,10,16,12</span>
        </div>
        <div class="col-6 col-lg-3">
          <label class="az-content-label">Commission This Month</label>
          <h2>{{_c($comission_this_month)}}</h2>
          <div class="desc up">
            <i class="icon ion-md-stats"></i>
            <span>(Calendar Month)</span>
          </div>
          <span id="compositeline">3,2,4,6,12,14,8,7,14,16,12,7,8,4,3,2,2,5,6,7</span>
        </div>
        <div class="col-6 col-lg-3 mg-t-20 mg-lg-t-0">
          <label class="az-content-label">Pending Payment</label>
          <h2>{{_c($pending_payment)}}</h2>
          <div class="desc down">
            <i class="icon ion-md-stats"></i>
            <span>(All Time)</span>
          </div>
          <span id="compositeline4">5,9,5,6,4,12,18,14,10,15,12,5,8,5,12,5,12,10,16,12</span>
        </div>
        <div class="col-6 col-lg-3 mg-t-20 mg-lg-t-0">
          <label class="az-content-label">Revenue All Time</label>
          <h2>{{_c($revenue_all_time)}}</h2>
          <div class="desc up">
            <i class="icon ion-md-stats"></i>
            <span>(All Time)</span>
          </div>
          <span id="compositeline3">5,10,5,20,22,12,15,18,20,15,8,12,22,5,10,12,22,15,16,10</span>
        </div>
      </div>
    </div>
    @endcan

  </div>

  <div class="row row-sm mg-b-15 mg-sm-b-20">
    
    <div class="col-lg-{{Auth::user()->can('view-revenue', \App\Company::class) ? '6' : '12'}} col-xl-{{Auth::user()->can('view-revenue', \App\Company::class) ? '6' : '12'}} mg-t-20 mg-lg-t-0">
      <div class="card card-table-two" style="max-height:400px; overflow:scroll">
        <div class="az-content-label tx-13 mg-b-25">Activity Logs</div>
        <div class="az-profile-contact-list">

          @foreach($activities as $row)
          <div class="media">
            <div class="media-icon"><i class="icon ion-md-clock"></i></div>
            <div class="media-body">
              <span>{{_d($row->created_at, true)}}</span>
              <div>{{$row->description}}</div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>

    <div class="col-md-6 col-xl-6">
      <div class="card card-table-two">
        <h6 class="card-title">Your Most Recent Deliveries</h6>
        <span class="d-block mg-b-20">Most recent completed deliveries</span>
        <div class="table-responsive">
          <table class="table table-striped table-dashboard-two">
            <thead>
              <tr>
                <th class="wd-lg-25p">Reference</th>
                <th class="wd-lg-25p">Date</th>
                <th class="wd-lg-25p">User</th>
                <th class="wd-lg-25p">Item</th>
                <th class="wd-lg-25p">Driver</th>
                <th class="wd-lg-25p">Time</th>
                <th class="wd-lg-25p">Distance</th>
                <th class="wd-lg-25p">Amount</th>
              </tr>
            </thead>
            <tbody>
              @foreach($recent_deliveries as $row)
              <tr>
                <td><a href="{{url('admin/deliveries/?id='.$row->id)}}">#{{$row->reference}}</a></td>
                <td>{{_d($row->updated_at, true)}}</td>
                <td class="tx-medium tx-inverse">{{$row->user->name}}</td>
                <td class="tx-medium tx-inverse">{{$row->item}}</td>
                <td class="tx-medium tx-inverse"><a href="{{url('admin/drivers/driver/'.$row->driver_id)}}">{{$row->driver->name}}</a></td>
                <td class="tx-medium tx-inverse">{{$row->total_minutes}} Mins</td>
                <td class="tx-medium tx-inverse">{{$row->total_km}} KM</td>
                <td class="tx-medium tx-inverse">{{_c($row->total)}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

</div>

@endsection

@section('script')
<script src="{{asset('lib')}}/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="{{asset('lib')}}/raphael/raphael.min.js"></script>
<script src="{{asset('lib')}}/morris.js/morris.min.js"></script>
<script src="{{asset('lib')}}/jqvmap/jquery.vmap.min.js"></script>
<script src="{{asset('lib')}}/jqvmap/maps/jquery.vmap.usa.js"></script>
<script type="text/javascript">

  $('#compositeline').sparkline('html', {
    lineColor: '#cecece',
    lineWidth: 2,
    spotColor: false,
    minSpotColor: false,
    maxSpotColor: false,
    highlightSpotColor: null,
    highlightLineColor: null,
    fillColor: '#f9f9f9',
    chartRangeMin: 0,
    chartRangeMax: 10,
    width: '100%',
    height: 20,
    disableTooltips: true
  });

  $('#compositeline2').sparkline('html', {
    lineColor: '#cecece',
    lineWidth: 2,
    spotColor: false,
    minSpotColor: false,
    maxSpotColor: false,
    highlightSpotColor: null,
    highlightLineColor: null,
    fillColor: '#f9f9f9',
    chartRangeMin: 0,
    chartRangeMax: 10,
    width: '100%',
    height: 20,
    disableTooltips: true
  });

  $('#compositeline3').sparkline('html', {
    lineColor: '#cecece',
    lineWidth: 2,
    spotColor: false,
    minSpotColor: false,
    maxSpotColor: false,
    highlightSpotColor: null,
    highlightLineColor: null,
    fillColor: '#f9f9f9',
    chartRangeMin: 0,
    chartRangeMax: 10,
    width: '100%',
    height: 20,
    disableTooltips: true
  });

  $('#compositeline4').sparkline('html', {
    lineColor: '#cecece',
    lineWidth: 2,
    spotColor: false,
    minSpotColor: false,
    maxSpotColor: false,
    highlightSpotColor: null,
    highlightLineColor: null,
    fillColor: '#f9f9f9',
    chartRangeMin: 0,
    chartRangeMax: 10,
    width: '100%',
    height: 20,
    disableTooltips: true
  });


  var morrisData = [
  { y: 'Oct 01', a: 95000, b: 70000 },
  { y: 'Oct 05', a: 75000,  b: 55000 },
  { y: 'Oct 10', a: 50000,  b: 40000 },
  { y: 'Oct 15', a: 75000,  b: 65000 },
  { y: 'Oct 20', a: 50000,  b: 40000 },
  { y: 'Oct 25', a: 80000, b: 90000 },
  { y: 'Oct 30', a: 75000,  b: 65000 }
  ];

  new Morris.Bar({
    element: 'morrisBar1',
    data: morrisData,
    xkey: 'y',
    ykeys: ['a', 'b'],
    labels: ['Online', 'Offline'],
    barColors: ['#560bd0', '#00cccc'],
    preUnits: '$',
    barSizeRatio: 0.55,
    gridTextSize: 11,
    gridTextColor: '#494c57',
    gridTextWeight: 'bold',
    gridLineColor: '#999',
    gridStrokeWidth: 0.25,
    hideHover: 'auto',
    resize: true,
    padding: 5
  });

  $('#vmap2').vectorMap({
    map: 'usa_en',
    showTooltip: true,
    backgroundColor: '#fff',
    color: '#60adff',
    colors: {
      mo: '#9fceff',
      fl: '#60adff',
      or: '#409cff',
      ca: '#005cbf',
      tx: '#005cbf',
      wy: '#005cbf',
      ny: '#007bff'
    },
    hoverColor: '#222',
    enableZoom: false,
    borderWidth: 1,
    borderColor: '#fff',
    hoverOpacity: .85
  });
</script>

@endsection