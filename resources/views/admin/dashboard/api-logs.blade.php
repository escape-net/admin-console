@extends('admin.app')

@section('body')

<div class="az-content">
  <div class="container">
    <div class="az-content-body">
      <div class="az-content-breadcrumb">
        <span>Velocity Admin</span>
        <span>Api Logs</span>
      </div>

      <div class="row">
        <div class="col-md-8">
          <h2 class="az-content-title">Api Logs</h2>
        </div>
      </div>
      
      <div class="az-media-list-activity mg-b-20">

        <div class="row">
          @foreach($api_logs as $row)
          <div class="col-md-12 mb-2">
            <div class="media" style="border: 1px solid #eee; border-radius: 5px; padding: 5px; background-color:rgba(0, 0, 0, 0.01)">
              <div class="media-icon bg-purple">
                <i class="typcn typcn-tick-outline"></i>
              </div>
              <div class="media-body">
                <h6>[{{$row->id}}] {{$row->user->name}} | {{$row->endpoint}}</h6>
                <span>{{$row->comment}}</span>
              </div>
              <div class="media-right">{{_d($row->created_at, true)}}</div>
            </div>
          </div>
          @endforeach
        </div>

      </div>

    </div>
  </div>
</div>

@endsection