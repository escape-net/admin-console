@extends('admin.app')

@section('body')

<div class="az-content">
  <div class="container">
    <div class="az-content-body">
      <div class="az-content-breadcrumb">
        <span>Velocity Admin</span>
        <span>Companies</span>
      </div>

      <div class="row">
        <div class="col-md-8">
          <h2 class="az-content-title">Companies</h2>

        </div>
        <div class="col-md-4">
          <a class="btn btn-indigo btn-rounded" style="float:right" href="{{url('admin/companies/form')}}"><i class="fa fa-plus"></i> Add new</a>
        </div>
      </div>
      

      <table id="datatable1" class="display responsive nowrap">
        <thead>
          <tr>
            <th class="wd-15p">Name</th>
            <th class="wd-15p">Email</th>
            <th class="wd-20p">Phone</th>
            <th class="wd-20p">Drivers</th>
            <th class="wd-25p">Action</th>
          </tr>
        </thead>
        <tbody>

          @foreach($companies as $row)
          <tr>
            <td>{{$row->name}}</td>
            <td>{{$row->email}}</td>
            <td>{{$row->phone}}</td>
            <td>{{$row->users->count()}}</td>
            <td>
              <a href="{{url('admin/companies/form/'.$row->id)}}" class="btn btn-indigo btn-icon btn-sm">
                <i class="typcn typcn-pencil"></i>
              </a>
            </td>
          </tr>
          @endforeach

        </tbody>
      </table>



    </div>
  </div>
</div>

@endsection

