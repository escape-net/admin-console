@extends('admin.app')

@section('body')

<div class="az-content az-content-body-dashboard-three">
  <div class="container">
    <div class="az-content-body">
      <div class="az-content-breadcrumb">
        <span>Velocity Admin</span>
        <span>Company</span>
      </div>

      <div class="row">
        <div class="col-md-8">
          <h2 class="az-content-title">Administrator Form</h2>
        </div>
      </div>
      

      <form action="{{url('admin/companies/save')}}" method="POST" role="form">

        {{csrf_field()}}

        <div class="row">

          <div class="form-group col-12">
            <label>Company Name</label>
            <input type="text" name="name" class="form-control" placeholder="Company Name" value="{{old('name', $company->name)}}" required>
            @if($errors->has('name'))
            <span class="form-error">{{$errors->first('name')}}</span>
            @endif
          </div>

        </div>

        <div class="row">

          <div class="form-group col-6">
            <label>Contact Email</label>
            <input type="text" name="email" class="form-control" placeholder="Email Address" value="{{old('email', $company->email)}}" required>
            @if($errors->has('email'))
            <span class="form-error">{{$errors->first('email')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label>Contact Phone</label>
            <input type="text" name="phone" class="form-control" placeholder="Phone Number" value="{{old('phone', $company->phone)}}" required>
            @if($errors->has('phone'))
            <span class="form-error">{{$errors->first('phone')}}</span>
            @endif
          </div>

          <div class="form-group col-12">
            <label>Contact Address</label>
            <input type="text" name="address" class="form-control" placeholder="Company Address" value="{{old('address', $company->address)}}" required>
            @if($errors->has('address'))
            <span class="form-error">{{$errors->first('address')}}</span>
            @endif
          </div>

          <div class="form-group col-12">
            <label>Status</label>
            <select name="status" class="form-control" required>

              @if($company->status == 'active')
                <option value="active" selected>Active</option>
                <option value="inactive">Inactive</option>
              @else
                <option value="active">Active</option>
                <option value="inactive" selected>Inactive</option>
              @endif
              
            </select>
            @if($errors->has('status'))
            <span class="form-error">{{$errors->first('status')}}</span>
            @endif
          </div>

        </div>

        <input type="hidden" name="id" value="{{$company->id}}">
        <button type="submit" class="btn btn-indigo btn-rounded"><i class="fa fa-save"></i> Submit</button>

      </form>

    </div>
  </div>
</div>

@endsection

