@extends('admin.app')

@section('body')

<div class="az-content az-content-body-dashboard-three">
  <div class="container">
    <div class="az-content-body">
      <div class="az-content-breadcrumb">
        <span>Velocity Admin</span>
        <span>Administrators</span>
      </div>

      <div class="row">
        <div class="col-md-8">
          <h2 class="az-content-title">Administrator Form</h2>
        </div>
      </div>
      

      <form action="{{url('admin/administrators/save')}}" method="POST" role="form">

        {{csrf_field()}}

        <div class="row">

          <div class="form-group col-6">
            <label>First Name</label>
            <input type="text" name="first_name" class="form-control" placeholder="First Name" value="{{old('first_name', $user->first_name)}}" required>
            @if($errors->has('first_name'))
            <span class="form-error">{{$errors->first('first_name')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label>Last Name</label>
            <input type="text" name="last_name" class="form-control" placeholder="Last Name" value="{{old('last_name', $user->last_name)}}" required>
            @if($errors->has('last_name'))
            <span class="form-error">{{$errors->first('last_name')}}</span>
            @endif
          </div>

        </div>

        <div class="row">

          <div class="form-group col-6">
            <label>Email</label>
            <input type="text" name="email" class="form-control" placeholder="Email Address" value="{{old('email', $user->email)}}" required>
            @if($errors->has('email'))
            <span class="form-error">{{$errors->first('email')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label>Phone</label>
            <input type="text" name="phone" class="form-control" placeholder="Phone Number" value="{{old('phone', $user->phone)}}" required>
            @if($errors->has('phone'))
            <span class="form-error">{{$errors->first('phone')}}</span>
            @endif
          </div>

        </div>

        @if($user->password == '')

        <div class="row">

          <div class="form-group col-6">
            <label>Password</label>
            <input type="password" name="password" class="form-control" placeholder="Password" required>
            @if($errors->has('password'))
            <span class="form-error">{{$errors->first('password')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label>Retype Password</label>
            <input type="password" name="password_confirmation" class="form-control" placeholder="Retype Password" required>
          </div>

        </div>

        @endif

        <div class="row">
          
           <div class="form-group col-6">
            <label>Admin Status</label>
            <select name="status" class="form-control" required>

              @if($user->status == 1)
              <option value="1" selected>Active</option>
              <option value="0">Inactive</option>
              @else
              <option value="1">Active</option>
              <option value="0" selected>Inactive</option>
              @endif
              
            </select>
            @if($errors->has('status'))
            <span class="form-error">{{$errors->first('status')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label>Access Level</label>
            <select name="access_level" class="form-control select2" required>

              @foreach(_admin_types() as $row)
                @if($user->access_level == $row['id'])
                <option value="{{$row['id']}}" selected>{{$row['name']}}</option>
               @else
                <option value="{{$row['id']}}">{{$row['name']}}</option>
               @endif
              @endforeach
              
            </select>
            @if($errors->has('access_level'))
            <span class="form-error">{{$errors->first('access_level')}}</span>
            @endif
          </div>

        </div>

        <input type="hidden" name="id" value="{{$user->id}}">
        <button type="submit" class="btn btn-indigo btn-rounded"><i class="fa fa-save"></i> Submit</button>

      </form>

    </div>
  </div>
</div>

@endsection

