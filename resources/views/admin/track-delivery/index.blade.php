@extends('admin.app')

@section('body')

<div class="az-content" style="background:#F8F9FA">
    <div class="container">
        <div class="az-content-body">

            <div class="row">

                <div class="col-4 mt-5">

                    @if(isset($delivery))
                    <div class="az-content-left az-content-left-profile">

                        <div class="az-profile-overview">
                            <div class="az-img-user">
                                <img src="{{asset('img/deliveries.png')}}">
                            </div>
                            <div class="d-flex justify-content-between mg-b-20">
                                <div>
                                    <h5 class="az-profile-name">{{$delivery->user->name}}</h5>
                                    <p class="az-profile-name-text">Delivery Date: {{_d($delivery->created_at)}}</p>
                                </div>
                                <div class="btn-icon-list">
                                    <a href="{{url('admin/deliveries/?id='.$delivery->id)}}" class="btn btn-purple btn-icon"><i class="typcn typcn-eye"></i></a>
                                </div>
                            </div>

                            <hr class="mg-y-30">

                            <label class="az-content-label tx-13 mg-b-20">Other Info</label>
                            <div class="az-profile-social-list">
                                <div class="media">
                                    <div class="media-icon"><i class="fas fa-info-circle"></i></div>
                                    <div class="media-body">
                                        <span>Item</span>
                                        <span><strong><h5>{{$delivery->item}}</h5></strong></span>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-icon"><i class="fas fa-info-circle"></i></div>
                                    <div class="media-body">
                                        <span>Recipient Name</span>
                                        <span><strong><h5>{{$delivery->recipient_name}}</h5></strong></span>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-icon"><i class="fas fa-info-circle"></i></div>
                                    <div class="media-body">
                                        <span>Recipient Phone</span>
                                        <span><strong><h5>{{$delivery->recipient_phone}}</h5></strong></span>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-icon"><i class="fas fa-info-circle"></i></div>
                                    <div class="media-body">
                                        <span>Instructions</span>
                                        <span><strong><h5>{{$delivery->instructions}}</h5></strong></span>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    @endif
                </div>

                <div class="col-md-4 col-lg-4 col-xl-4 mg-t-20 mg-md-t-0 mg-xl-t-20">

                    <div class="az-header-center">

                        <form method="post" action="{{url('admin/track-delivery')}}">
                            {{csrf_field()}}
                            <input type="text" class="form-control" placeholder="Delivery Reference" name="reference" required>
                            <button class="btn" type="submit"><i class="fas fa-search"></i></button>
                        </form>

                    </div>

                    <hr>

                    @if(isset($delivery))
                    <div class="card card-dashboard-events">
                        <div class="card-header">
                            <h6 class="card-title">{{_d($delivery->created_at)}}</h6>
                            <h5 class="card-subtitle">Track History</h5>
                        </div>
                        <div class="card-body">
                            <div class="list-group">

                                @foreach($history as $row)
                                <div class="list-group-item">
                                    <div class="event-indicator bg-{{$row->track_status->color}}"></div>
                                    <label>{{_d($row->created_at, true)}}</label>
                                    <h6><strong>{{$row->status}}</strong>&nbsp;{{$row->comment}}</h6>
                                </div>
                                @endforeach
                                
                            </div>
                        </div>
                    </div>
                    @endif
                </div>

                <div class="col-md-4 mt-5">

                    @if(isset($delivery))
                    <form action="{{url('admin/track-delivery/save-history')}}" method="POST" role="form">

                        {{csrf_field()}}

                        <legend>Update Status</legend>

                        <div class="form-group">
                            <label>Track Status</label>
                            <select name="track_status_id" class="form-control select2" required>

                                @foreach($statuses as $row)
                                <option value="{{$row->id}}">{{$row->description}}</option>
                                @endforeach

                            </select>
                        </div>

                        <div class="form-group">
                            <label>Comment</label>
                            <textarea name="comment" class="form-control" rows="3" required></textarea>
                        </div>

                        <input type="hidden" name="id" value="{{$delivery->id}}">
                        <button type="submit" class="btn btn-indigo btn-rounded"><i class="fa fa-save"></i> Submit</button>
                    </form>

                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

