@extends('admin.app')

@section('body')

<div class="az-content az-content-body-dashboard-three">
  <div class="container">
    <div class="az-content-body">
      <div class="az-content-breadcrumb">
        <span>Velocity Admin</span>
        <span>Vehicle</span>
      </div>

      <div class="row">
        <div class="col-md-8">
          <h2 class="az-content-title">Vehicle Form</h2>
        </div>
      </div>
      

      <form action="{{url('admin/vehicles/save')}}" enctype="multipart/form-data" method="POST" role="form">

        {{csrf_field()}}

        <div class="row">

          <div class="form-group col-12">
            <label>Company</label>
            <select name="company_id" class="form-control select2" required>

              @foreach($companies as $row)

              @if(old('company_id', $vehicle->company_id) == $row->id)
              <option value="{{$row->id}}" selected>{{$row->name}}</option>
              @else
              <option value="{{$row->id}}">{{$row->name}}</option>
              @endif

              @endforeach
              
            </select>
            @if($errors->has('company_id'))
            <span class="form-error">{{$errors->first('company_id')}}</span>
            @endif
          </div>

        </div>

        <div class="row">

          <div class="form-group col-6">
            <label>Type</label>
            <select name="vehicle_type_id" class="form-control select2">

              @foreach($vehicle_types as $row)

              @if(old('mode_id', $vehicle->vehicle_type_id) == $row->id)
              <option value="{{$row->id}}" selected>{{$row->name}}</option>
              @else
              <option value="{{$row->id}}">{{$row->name}}</option>
              @endif

              @endforeach
              
            </select>
            @if($errors->has('mode_id'))
            <span class="form-error">{{$errors->first('mode_id')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label>Vehicle Make</label>
            <input type="text" name="make" class="form-control" placeholder="Vehicle Make" value="{{old('make', $vehicle->make)}}" required>
            @if($errors->has('make'))
            <span class="form-error">{{$errors->first('make')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label>Vehicle Model</label>
            <input type="text" name="model" class="form-control" placeholder="Vehicle Model" value="{{old('model', $vehicle->model)}}" required>
            @if($errors->has('model'))
            <span class="form-error">{{$errors->first('model')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label>Vehicle Year</label>
            <input type="text" name="year" class="form-control" placeholder="Vehicle Year" value="{{old('year', $vehicle->year)}}" required>
            @if($errors->has('year'))
            <span class="form-error">{{$errors->first('year')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label>Vehicle Color</label>
            <input type="text" name="color" class="form-control" placeholder="Vehicle Color" value="{{old('color', $vehicle->color)}}" required>
            @if($errors->has('color'))
            <span class="form-error">{{$errors->first('color')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label>Vehicle Plate Number</label>
            <input type="text" name="plate_number" class="form-control" placeholder="Vehicle Year" value="{{old('plate_number', $vehicle->plate_number)}}" required>
            @if($errors->has('plate_number'))
            <span class="form-error">{{$errors->first('plate_number')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label>Driver</label>
            <select name="user_id" class="form-control">

              @foreach($drivers as $row)
                @if($row->id == old('user_id', $vehicle->user_id))
                  <option value="{{$row->id}}" selected>{{$row->name}}</option>
                @else
                  <option value="{{$row->id}}" selected>{{$row->name}}</option>
                @endif
              @endforeach
              
            </select>
            @if($errors->has('user_id'))
            <span class="form-error">{{$errors->first('user_id')}}</span>
            @endif
          </div>

          <div class="form-group col-6" id="documents">
            <label>Vehicle Status</label>
            <select name="status" class="form-control" required>

              @if($vehicle->status == 'active')
              <option value="active" selected>Active</option>
              <option value="inactive">Inactive</option>
              @else
              <option value="active">Active</option>
              <option value="inactive" selected>Inactive</option>
              @endif
              
            </select>
            @if($errors->has('status'))
            <span class="form-error">{{$errors->first('status')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label>Vehicle Registration Document</label>
            <input type="file" name="registration" class="dropify" data-max-file-size="1M" data-default-file="{{$vehicle->registration}}" data-allowed-file-extensions="png jpg jpeg pdf docx">
          </div>

          <div class="form-group col-6">
            <label>Vehicle Insurance Document</label>
            <input type="file" name="insurance" class="dropify" data-max-file-size="1M" data-default-file="{{$vehicle->insurance}}" data-allowed-file-extensions="png jpg jpeg pdf docx">
          </div>

          <div class="form-group col-6">
            <label>Vehicle Certificate</label>
            <input type="file" name="certificate" class="dropify" data-max-file-size="1M" data-default-file="{{$vehicle->certificate}}" data-allowed-file-extensions="png jpg jpeg pdf docx">
          </div>

          <div class="form-group col-6">
            <label>Vehicle Image</label>
            <input type="file" name="image" class="dropify" data-max-file-size="1M" data-default-file="{{$vehicle->image}}" data-allowed-file-extensions="png jpg jpeg pdf docx">
          </div>

        </div>

        <input type="hidden" name="id" value="{{$vehicle->id}}">
        <button type="submit" class="btn btn-indigo btn-rounded"><i class="fa fa-save"></i> Submit</button>

      </form>

    </div>
  </div>
</div>

@endsection

