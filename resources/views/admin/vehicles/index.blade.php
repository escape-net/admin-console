@extends('admin.app')

@section('body')

<div class="az-content">
  <div class="container">
    <div class="az-content-body">
      <div class="az-content-breadcrumb">
        <span>Velocity Admin</span>
        <span>Vehicles</span>
      </div>

      <div class="row">
        <div class="col-md-8">
          <h2 class="az-content-title">Vehicles</h2>

        </div>
        <div class="col-md-4">
          <a class="btn btn-indigo btn-rounded" style="float:right" href="{{url('admin/vehicles/form')}}"><i class="fa fa-plus"></i> Add new</a>
        </div>
      </div>


      <table id="datatable1" class="display responsive nowrap">
        <thead>
          <tr>
            <th class="wd-15p">Company</th>
            <th class="wd-15p">Driver</th>
            <th class="wd-15p">Type</th>
            <th class="wd-20p">Description</th>
            <th class="wd-20p">Plate Number</th>
            <th class="wd-20p">Status</th>
            <th class="wd-25p">Action</th>
          </tr>
        </thead>
        <tbody>

          @foreach($vehicles as $row)
          <tr>
            <td>{{$row->company->name}}</td>
            <td>{{$row->driver->name}}</td>
            <td>{{$row->vehicle_type->name}}</td>
            <td>{{$row->make}} | {{$row->model}}</td>
            <td>{{$row->plate_number}}</td>
            <td>{{_badge($row->status)}}</td>
            <td>
              
              <div class="btn-group btn-sm" role="group" aria-label="Actions">
                <a href="{{url('admin/vehicles/form/'.$row->id)}}" class="btn btn-indigo btn-icon"><i class="typcn typcn-pencil"></i></a>
                <a href="{{url('admin/vehicles/vehicle/'.$row->id)}}" class="btn btn-indigo btn-icon"><i class="typcn typcn-eye-outline"></i></a>
              </div>

            </td>
          </tr>
          @endforeach

        </tbody>
      </table>

    </div>
  </div>
</div>

@endsection

