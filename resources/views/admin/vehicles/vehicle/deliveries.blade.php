<div class="az-profile-body animated fadeIn" v-if="tab == 'deliveries' ">

	<div class="row mg-b-20">
		<div class="col-md-12 col-xl-12">

			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Reference</th>
						<th>Item</th>
						<th>User</th>
						<th>Status</th>
						<th>Date</th>
					</tr>
				</thead>
				<tbody>
					@foreach($deliveries as $row)
					<tr>
						<td><a href="{{url('admin/deliveries?id='.$row->id)}}">#{{$row->reference}}</a></td>
						<td>{{$row->item}}</td>
						<td>{{$row->user->name}}</td>
						<td>{{_badge($row->status)}}</td>
						<td>{{_d($row->created_at, true)}}</td>
					</tr>
					@endforeach

					@if($deliveries->count() < 1)
					<tr>
						<td colspan="5">
							<h3 class="text-center">No deliveries yet for this vehicle.</h3>
						</td>
					</tr>
					@endif
				</tbody>
			</table>
		</div>

	</div>

</div>