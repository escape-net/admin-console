<div class="az-profile-body animated fadeIn" v-if="tab == 'summary' ">

	<div class="row mg-b-20">
		<div class="col-md-7 col-xl-8">
			<div class="az-profile-view-chart">
				<canvas id="chartArea"></canvas>
				<div class="az-profile-view-info">
					<div class="d-flex align-items-baseline">
						<h6>508</h6>
					</div>
					<p>All Time Deliveries Count</p>
				</div>
			</div>
		</div>
		<div class="col-md-5 col-xl-4 mg-t-40 mg-md-t-0">
			<div class="az-content-label tx-13 mg-b-20">Deliveries Breakdown last 5 Months</div>
			<div class="az-traffic-detail-item">
				<div>
					<span>January 2019</span>
					<span>24</span>
				</div>
				<div class="progress">
					<div class="progress-bar wd-20p" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
				</div>
			</div>
			<div class="az-traffic-detail-item">
				<div>
					<span>December 2018</span>
					<span>16</span>
				</div>
				<div class="progress">
					<div class="progress-bar bg-success wd-15p" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
				</div>
			</div>
			<div class="az-traffic-detail-item">
				<div>
					<span>November 2018</span>
					<span>87</span>
				</div>
				<div class="progress">
					<div class="progress-bar bg-pink wd-45p" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
				</div>
			</div>
			<div class="az-traffic-detail-item">
				<div>
					<span>October 2018</span>
					<span>32</span>
				</div>
				<div class="progress">
					<div class="progress-bar bg-teal wd-25p" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
				</div>
			</div>
		</div>
	</div>

	<hr class="mg-y-40">

	<div class="row">
		<div class="col-md-7 col-xl-8">
			<div class="az-content-label tx-13 mg-b-25">Driver Information</div>
			<div class="az-profile-work-list">
				<div class="media">
					<div class="media-logo bg-success"><i class="fas fa-phone-volume"></i></div>
					<div class="media-body">
						<h6>Phone Number</h6>
						<span>{{$vehicle->driver->phone}}</span>
					</div>
				</div>
				<div class="media">
					<div class="media-logo bg-primary"><i class="fas fa-envelope-open"></i></div>
					<div class="media-body">
						<h6>Email</h6>
						<span>{{$vehicle->driver->email}}</span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-5 col-xl-4 mg-t-40 mg-md-t-0">
			<div class="az-content-label tx-13 mg-b-25">Deliveries Completed</div>
			<div class="az-profile-contact-list">
				<div class="media">
					<div class="media-icon"><i class="fas fa-sort-amount-up"></i></div>
					<div class="media-body">
						<span>Count</span>
						<div>{{$deliveries->count()}}</div>
					</div>
				</div>
				<div class="media">
					<div class="media-icon"><i class="fas fa-align-justify"></i></div>
					<div class="media-body">
						<span>Total</span>
						<div>{{_c($deliveries->sum('total'))}}</div>
					</div>
				</div>
				<div class="media">
					<div class="media-icon"><i class="fas fa-crosshairs"></i></div>
					<div class="media-body">
						<span>Total Distance</span>
						<div>{{$deliveries->sum('total_km')}} KM</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="mg-b-20"></div>

</div>