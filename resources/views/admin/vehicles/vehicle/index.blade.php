@extends('admin.app')

@section('body')

<div class="az-content az-content-profile" id="vehicle-app">
	<div class="container mn-ht-100p">
		<div class="az-content-left az-content-left-profile">

			<div class="az-profile-overview">
				<div class="az-img-user">
					<img src="{{$vehicle->image ?? asset('img/rider.png')}}" alt="{{$vehicle->name}}">
				</div>
				<div class="d-flex justify-content-between mg-b-20">
					<div>
						<h5 class="az-profile-name">Driver: {{$vehicle->driver->name}}</h5>
						<p class="az-profile-name-text">Completed {{($deliveries->count())}} {{str_plural('Delivery', $deliveries->count())}} so far.</p>
					</div>
				</div>

				<hr class="mg-y-30">

				<label class="az-content-label tx-13 mg-b-20">Vehicle Details</label>
				<div class="az-profile-social-list">

					<div class="media">
						<div class="media-icon"><i class="fas fa-atom"></i></div>
						<div class="media-body">
							<span>Type</span>
							<strong>{{$vehicle->vehicle_type->name}}</strong>
						</div>
					</div>

					<div class="media">
						<div class="media-icon"><i class="fas fa-home"></i></div>
						<div class="media-body">
							<span>Company</span>
							<strong>{{$vehicle->company->name}}</strong>
						</div>
					</div>

					<div class="media">
						<div class="media-icon"><i class="fas fa-taxi"></i></div>
						<div class="media-body">
							<span>Plate Number</span>
							<strong>{{$vehicle->plate_number}}</strong>
						</div>
					</div>

					<div class="media">
						<div class="media-icon"><i class="fas fa-car"></i></div>
						<div class="media-body">
							<span>Make / Year / Color</span>
							<strong>{{$vehicle->make}} / {{$vehicle->year}} / {{ucwords($vehicle->color)}}</strong>
						</div>
					</div>

					<div class="media">
						<div class="media-icon"><i class="fas fa-cog"></i></div>
						<div class="media-body">
							<span>Status</span>
							<strong>{{_badge(strtoupper($vehicle->status))}}</strong>
						</div>
					</div>

					<div class="media">
						<div class="media-icon"><i class="fas fa-user"></i></div>
						<div class="media-body">
							<span>Driver Status</span>
							<strong>{{_badge(strtoupper($vehicle->driver->driver_status))}}</strong>
						</div>
					</div>
					
				</div>

			</div>

		</div>

		<div class="az-content-body az-content-body-profile">
			
			<nav class="nav az-nav-line">
				<a href="#" class="nav-link active" data-toggle="tab" @click="change_tab('summary')">Summary</a>
				<a href="#" class="nav-link" data-toggle="tab" @click="change_tab('deliveries')">Deliveries</a>
				<a href="#" class="nav-link" data-toggle="tab" @click="change_tab('reviews')">Reviews</a>
				<a href="#" class="nav-link" data-toggle="tab" @click="change_tab('documents')">Documents</a>
				<a href="#" class="nav-link" data-toggle="tab" onclick="window.location = '{{url('admin/vehicles/form/'.$vehicle->id)}}'">Edit Vehicle Details</a>
			</nav>

			@include('admin.vehicles.vehicle.summary')
			@include('admin.vehicles.vehicle.deliveries')
			@include('admin.vehicles.vehicle.reviews')
			@include('admin.vehicles.vehicle.documents')

			
		</div>
	</div>
</div>

@endsection

@section('script')
<script src="{{asset('lib/chart.js/Chart.bundle.min.js')}}"></script>
<script>
	$(function(){
		'use strict'

		function chart(){

			var ctx = document.getElementById('chartArea').getContext('2d');

			var gradient = ctx.createLinearGradient(0, 240, 0, 0);
			gradient.addColorStop(0, 'rgba(0,123,255,0)');
			gradient.addColorStop(1, 'rgba(0,123,255,.3)');

			new Chart(ctx, {
				type: 'line',
				data: {
					labels: ['Oct 1', 'Oct 2', 'Oct 3', 'Oct 4', 'Oct 5', 'Oct 6', 'Oct 7', 'Oct 8', 'Oct 9', 'Oct 10'],
					datasets: [{
						data: [12, 15, 18, 40, 35, 38, 32, 20, 25],
						borderColor: '#007bff',
						borderWidth: 1,
						backgroundColor: gradient
					}]
				},
				options: {
					maintainAspectRatio: false,
					legend: {
						display: false,
						labels: {
							display: false
						}
					},
					scales: {
						yAxes: [{
							display: false,
							ticks: {
								beginAtZero:true,
								fontSize: 10,
								max: 80
							}
						}],
						xAxes: [{
							ticks: {
								beginAtZero:true,
								fontSize: 11,
								fontFamily: 'Arial'
							}
						}]
					}
				}
			});
		}

		chart()

	});
</script>
<script type="text/javascript" src="{{asset('js/vehicle.js')}}"></script>
@endsection