<div class="az-profile-body animated fadeIn" v-if="tab == 'documents' ">

	<div class="row mg-b-20">
		<div class="col-md-12 col-xl-12">
			
			<div class="row row-sm">

				<div class="col-md-6 col-lg-4">
					<div class="card" style="min-height:362px;">
						@if($vehicle->registration != '')
						<a href="javascript:;" onclick="openPopup('{{$vehicle->registration}}')">
							<img class="img-fluid" src="{{asset('img/file.png')}}" alt="{{$vehicle->name}}">
						</a>
						@else
						<img class="img-fluid" src="{{asset('img/file.png')}}" alt="{{$vehicle->name}}">
						@endif
						<div class="card-body">
							<h5 class="card-title tx-dark tx-medium mg-b-10">Vehicle Registration Document</h5>
							@if($vehicle->registration != '')
							{{_badge('uploaded')}}
							@else
							<span class="text-danger">Document awaiting Upload.</span>
							<a href="{{url('admin/vehicles/form/'.$vehicle->id)}}#documents">Click here</a> to upload now.
							@endif
						</div>
					</div>
				</div>

				<div class="col-md-6 col-lg-4">
					<div class="card" style="min-height:362px;">

						@if($vehicle->insurance != '')
						<a href="javascript:;" onclick="openPopup('{{$vehicle->insurance}}')">
							<img class="img-fluid" src="{{asset('img/file.png')}}" alt="{{$vehicle->name}}">
						</a>
						@else
						<img class="img-fluid" src="{{asset('img/file.png')}}" alt="{{$vehicle->name}}">
						@endif

						<div class="card-body">
							<h5 class="card-title tx-dark tx-medium mg-b-10">Vehicle Insurance Document</h5>
							@if($vehicle->insurance)

							<form class="form form-inline" action="{{url('admin/vehicles/document-status')}}" method="post">

								{{csrf_field()}}

								<select name="insurance_status" class="form-control mr-2" style="width:75%" required {{$vehicle->insurance_status == 'approved' ? 'disabled' : ''}}>

									@foreach(['pending', 'in_progress', 'approved'] as $row)
									<option value="{{$row}}" {{$vehicle->insurance_status == $row ? 'selected' : ''}}>
										{{ucwords(implode(' ', explode('_', $row)))}}
									</option>
									@endforeach
								</select>

								<input type="hidden" name="id" value="{{$vehicle->id}}">

								@if($vehicle->insurance_status != 'approved')
								<button type="submit" class="btn btn-primary btn-icon">
									<i class="fas fa-save"></i>
								</button>
								@endif
							</form>

							@else
							<span class="text-danger">Document awaiting Upload.</span>
							<a href="{{url('admin/vehicles/form/'.$vehicle->id)}}#documents">Click here</a> to upload now.
							@endif
						</div>
					</div>
				</div>

				<div class="col-md-6 col-lg-4">
					<div class="card" style="min-height:362px;">

						@if($vehicle->certificate != '')
						<a href="javascript:;" onclick="openPopup('{{$vehicle->certificate}}')">
							<img class="img-fluid" src="{{asset('img/file.png')}}" alt="{{$vehicle->name}}">
						</a>
						@else
						<img class="img-fluid" src="{{asset('img/file.png')}}" alt="{{$vehicle->name}}">
						@endif

						<div class="card-body">
							<h5 class="card-title tx-dark tx-medium mg-b-10">Vehicle Certificate Document</h5>
							@if($vehicle->certificate)

							<form class="form form-inline" action="{{url('admin/vehicles/document-status')}}" method="post">

								{{csrf_field()}}

								<select name="certificate_status" class="form-control mr-2" style="width:75%" required {{$vehicle->certificate_status == 'approved' ? 'disabled' : ''}}>

									@foreach(['pending', 'in_progress', 'approved'] as $row)
									<option value="{{$row}}" {{$vehicle->certificate_status == $row ? 'selected' : ''}}>
										{{ucwords(implode(' ', explode('_', $row)))}}
									</option>
									@endforeach
								</select>

								<input type="hidden" name="id" value="{{$vehicle->id}}">

								@if($vehicle->certificate_status != 'approved')
								<button type="submit" class="btn btn-primary btn-icon">
									<i class="fas fa-save"></i>
								</button>
								@endif
							</form>

							@else
							<span class="text-danger">Document awaiting Upload.</span>
							<a href="{{url('admin/vehicles/form/'.$vehicle->id)}}#documents">Click here</a> to upload now.
							@endif
						</div>
					</div>
				</div>

				@if($vehicle->image)
				<div class="col-md-12 mt-5">
					<h1>Vehicle Image</h1>
					<img src="{{$vehicle->image}}" class="img img-responsive img-thumbnail" style="max-width:500px;">
				</div>
				@endif
				
			</div>
		</div>
		
	</div>

</div>