@extends('admin.app')

@section('body')

<div class="az-content-body row">

  <div class="az-content-body-left col-8">
    <h2 class="az-content-title tx-24 mg-b-5">{{$driver->name}} - Driver</h2>
    <p class="mg-b-20">
      Completed {{$driver->deliveries->count()}} {{str_plural('Delivery', $driver->deliveries->count())}} so far. Drivers 

      @if($driver->vehicle->id > 0)
      <a href="{{url('admin/vehicles/vehicle/'.$driver->vehicle->id)}}">{{$driver->vehicle->name}}</a>
      @endif
    </p>

    <div class="row row-sm mg-b-20">

      <div class="col-sm-6 col-lg-6 mg-t-20 mg-sm-t-0">
        <div class="card card-dashboard-twentysix card-dark-one">
          <div class="card-header">
            <h6 class="card-title">Deliveries</h6>
          </div>
          <div class="card-body">
            <div class="pd-x-15">
              <h6>{{$driver->deliveries->count()}} <span><i class="icon ion-md-arrow-up"></i> 0.20%</span></h6>
              <label>All Deliveries Count</label>
            </div>
            <div class="chart-wrapper">
              <div id="flotChart8" class="flot-chart"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6 mg-t-20 mg-lg-t-0">
        <div class="card card-dashboard-twentysix card-dark-two">
          <div class="card-header">
            <h6 class="card-title">Reviews</h6>
          </div>
          <div class="card-body">
            <div class="pd-x-15">
              <h6>0 <span><i class="icon ion-md-arrow-up"></i> 0%</span></h6>
              <label>Total Reviews</label>
            </div>
            <div class="chart-wrapper">
              <div id="flotChart9" class="flot-chart"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="card card-dashboard-table-six">
      <h6 class="card-title">Recently Completed Deliveries</h6>
      <div class="table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>&nbsp;</th>
              <th>Item</th>
              <th>User</th>
              <th>Total</th>
              <th>Time</th>
              <th>Started At</th>
              <th>Ended At</th>
            </tr>
          </thead>
          <tbody>
            @foreach($recent_deliveries as $row)
            <tr>
              <td><a href="{{url('admin/deliveries?id='.$row->id)}}">#{{$row->reference}}</a></td>
              <td>{{$row->item}}</td>
              <td>{{$row->user->name}}</td>
              <td>{{_c($row->total)}}</td>
              <td>{{$row->total_minutes}} Minutes</td>
              <td>{{_d($row->start_at, true)}}</td>
              <td>{{_d($row->end_at, true)}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>

  </div>
  <div class="az-content-body-right col-4">
    <div class="row mg-b-20">
      <div class="col">
        <label class="az-rating-label">Score</label>
        <h6 class="az-rating-value">98%</h6>
      </div>
      <div class="col">
        <label class="az-rating-label">Rating Average</label>
        <h6 class="az-rating-value">4.5</h6>
      </div>
    </div>
    <hr class="mg-y-25">
    <label class="az-content-label tx-base mg-b-25">2 Recent Reviews</label>
    <div class="az-media-list-reviews">

      @for($i = 0; $i<=1; $i++)
      <div class="media">
        <div class="az-img-user"><img src="{{asset('img/driver.png')}}" alt=""></div>
        <div class="media-body">
          <div class="d-flex justify-content-between mg-b-10">
            <div>
              <h6 class="mg-b-0">Olaiya Segun</h6>
              <div class="az-star-group">
                <div class="az-star-item"><i class="icon ion-md-star"></i></div>
                <div class="az-star-item"><i class="icon ion-md-star"></i></div>
                <div class="az-star-item"><i class="icon ion-md-star"></i></div>
                <div class="az-star-item"><i class="icon ion-md-star"></i></div>
                <div class="az-star-item"><i class="icon ion-md-star"></i></div>
                <span>4.1</span>
              </div>
            </div>
            <small>1 hour ago</small>
          </div>
          <p class="mg-b-0">This driver is super amazing! He Drives with his eyes closed! So cool... <a href="#">Read more</a></p>
        </div>
      </div>
      @endfor

    </div>

  </div>

</div>

@endsection

@section('script')

<script src="{{asset('/')}}lib/jquery.flot/jquery.flot.js"></script>
<script src="{{asset('/')}}js/dashboard.sampledata.js"></script>
<script>
  $(function(){    

    $.plot('#flotChart8', [{ data: dashData4, color: '#3381d6' }], {

      series: { bars: { show: true, lineWidth: 0, fill: 1, barWidth: .5 } },
      grid: { borderWidth: 0, labelMargin: 0 },
      yaxis: { show: true, min: 0, max: 30, ticks: [[0,''],[10,'100'],[20,'200']], tickColor: 'rgba(255,255,255,0)' },
      xaxis: { show: true, max: 40, ticks: [[0,''],[15,'Feb 12'],[30,'Feb 12']], tickColor: 'rgba(255,255,255,0)' }

    });

    $.plot('#flotChart9', [{ data: dashData3, color: '#fff',  bars: { show: true, lineWidth: 0, barWidth: .5 } },{

      data: dashData4,
      color: '#fff',
      lines: { show: true, lineWidth: 2, fill: .16 }
    }], {
      series: {  shadowSize: 0 },
      grid: { borderWidth: 0, labelMargin: 0 },
      yaxis: { show: true, min: 0, max: 30, ticks: [[0,''],[10,'100'],[20,'200']], tickColor: 'rgba(255,255,255,0)' },
      xaxis: { show: true, max: 40, ticks: [[0,''],[15,'Feb 12'],[30,'Feb 12']], tickColor: 'rgba(255,255,255,0)' }
    });

  });
</script>

@endsection