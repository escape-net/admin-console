@extends('admin.app')

@section('body')

<div class="az-content az-content-body-dashboard-three">
  <div class="container">
    <div class="az-content-body">
      <div class="az-content-breadcrumb">
        <span>Velocity Admin</span>
        <span>Administrators</span>
      </div>

      <div class="row">
        <div class="col-md-8">
          <h2 class="az-content-title">Administrator Form</h2>
        </div>
      </div>
      

      <form action="{{url('admin/drivers/save')}}" method="POST" role="form">

        {{csrf_field()}}

        <div class="row">

          <div class="form-group col-12">
            <label>Company</label>
            <select name="company_id" class="form-control" required>

              @foreach($companies as $row)

              @if(old('company_id', $user->company_id) == $row->id)
              <option value="{{$row->id}}" selected>{{$row->name}}</option>
              @else
              <option value="{{$row->id}}">{{$row->name}}</option>
              @endif

              @endforeach
              
            </select>
            @if($errors->has('company_id'))
            <span class="form-error">{{$errors->first('company_id')}}</span>
            @endif
          </div>

        </div>

        <div class="row">

          <div class="form-group col-6">
            <label>First Name</label>
            <input type="text" name="first_name" class="form-control" placeholder="First Name" value="{{old('first_name', $user->first_name)}}" required>
            @if($errors->has('first_name'))
            <span class="form-error">{{$errors->first('first_name')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label>Last Name</label>
            <input type="text" name="last_name" class="form-control" placeholder="Last Name" value="{{old('last_name', $user->last_name)}}" required>
            @if($errors->has('last_name'))
            <span class="form-error">{{$errors->first('last_name')}}</span>
            @endif
          </div>

        </div>

        <div class="row">

          <div class="form-group col-6">
            <label>Email</label>
            <input type="text" name="email" class="form-control" placeholder="Email Address" value="{{old('email', $user->email)}}" required>
            @if($errors->has('email'))
            <span class="form-error">{{$errors->first('email')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label>Phone</label>
            <input type="text" name="phone" class="form-control" placeholder="Phone Number" value="{{old('phone', $user->phone)}}" required>
            @if($errors->has('phone'))
            <span class="form-error">{{$errors->first('phone')}}</span>
            @endif
          </div>

        </div>

        <div class="row">

          <div class="form-group col-12">
            <label>Address</label>
            <input type="text" name="address" class="form-control" placeholder="Address" value="{{old('address', $user->address)}}" required>
            @if($errors->has('address'))
            <span class="form-error">{{$errors->first('address')}}</span>
            @endif
          </div>
          
        </div>

        <div class="row">

          <div class="form-group col-4">
            <label>Vehicle</label>
            <select name="vehicle_id" class="form-control">

              @foreach($vehicles as $row)
                @if($row->id == old('vehicle_id', $user->vehicle->id))
                <option value="{{$row->id}}" selected>{{$row->name}}</option>
                @else
                <option value="{{$row->id}}" selected>{{$row->name}}</option>
                @endif
              @endforeach
              
            </select>
           
          </div>

          <div class="form-group col-4">
            <label>Driver Status</label>
            <select name="driver_status" class="form-control">

              @foreach(['offline', 'online', 'assigned', 'on_trip'] as $row)
              <option value="{{$row}}" {{$user->driver_status == $row ? 'selected' : ''}}>{{ucwords($row)}}</option>
              @endforeach

            </select>
          </div>

          <div class="form-group col-4">
            <label>Account Status</label>
            <select name="status" class="form-control" required>

              @if($user->status == 1)
              <option value="1" selected>Active</option>
              <option value="0">Inactive</option>
              @else
              <option value="1">Active</option>
              <option value="0" selected>Inactive</option>
              @endif

            </select>
            @if($errors->has('status'))
            <span class="form-error">{{$errors->first('status')}}</span>
            @endif
          </div>

        </div>

        <div class="row">

          <div class="form-group col-4">
            <label>Bank</label>
            <select name="bank_id" class="form-control" required>

              @foreach($banks as $row)

              @if(old('bank_id', $user->bank_id) == $row->id)
              <option value="{{$row->id}}" selected>{{$row->name}}</option>
              @else
              <option value="{{$row->id}}">{{$row->name}}</option>
              @endif

              @endforeach
              
            </select>
            @if($errors->has('bank_id'))
            <span class="form-error">{{$errors->first('bank_id')}}</span>
            @endif
          </div>

          <div class="form-group col-4">
            <label>Bank Account Number</label>
            <input type="text" name="bank_account_number" class="form-control" placeholder="Bank Account Number" value="{{old('bank_account_number', $user->bank_account_number)}}">
            @if($errors->has('bank_account_number'))
            <span class="form-error">{{$errors->first('bank_account_number')}}</span>
            @endif
          </div>

          <div class="form-group col-4">
            <label>Bank Account Name</label>
            <input type="text" name="bank_account_name" class="form-control" placeholder="Bank Account Name" value="{{old('bank_account_name', $user->bank_account_name)}}">
            @if($errors->has('bank_account_name'))
            <span class="form-error">{{$errors->first('bank_account_name')}}</span>
            @endif
          </div>

        </div>

        <input type="hidden" name="id" value="{{$user->id}}">
        <button type="submit" class="btn btn-indigo btn-rounded"><i class="fa fa-save"></i> Submit</button>

      </form>

    </div>
  </div>
</div>

@endsection

