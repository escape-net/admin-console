@extends('admin.app')

@section('body')

<div class="az-content">
  <div class="container">
    <div class="az-content-body">
      <div class="az-content-breadcrumb">
        <span>Velocity Admin</span>
        <span>Drivers</span>
      </div>

      <div class="row">
        <div class="col-md-8">
          <h2 class="az-content-title">Drivers</h2>
        </div>

        @can('write-driver', \App\User::class)
        <div class="col-md-4">
          <a class="btn btn-indigo btn-rounded" style="float:right" href="{{url('admin/drivers/form')}}"><i class="fa fa-plus"></i> Add new</a>
        </div>
        @endcan
      </div>
      

      <table id="datatable1" class="display responsive nowrap">
        <thead>
          <tr>
            <th class="wd-15p">Company</th>
            <th class="wd-15p">Name</th>
            <th class="wd-15p">Email</th>
            <th class="wd-20p">Phone</th>
            <th class="wd-20p">Status</th>
            <th class="wd-20p">Deliveries</th>
            <th class="wd-20p">LastLogin</th>
            <th class="wd-25p">Action</th>
          </tr>
        </thead>
        <tbody>

          @foreach($users as $row)
          <tr>
            <td>{{$row->company->name}}</td>
            <td>{{$row->name}}</td>
            <td>{{$row->email}}</td>
            <td>{{$row->phone}}</td>
            <td>{{_badge($row->driver_status)}}</td>
            <td>{{$row->deliveries->count()}}</td>
            <td>{{_d($row->last_login)}}</td>
            <td>

              <div class="btn-group" role="group" aria-label="Actions">
                @can('write-driver', \App\User::class)
                <a href="{{url('admin/drivers/form/'.$row->id)}}" class="btn btn-indigo btn-icon"><i class="typcn typcn-pencil"></i></a>
                @endcan
                <a href="{{url('admin/drivers/driver/'.$row->id)}}" class="btn btn-indigo btn-icon"><i class="typcn typcn-eye-outline"></i></a>
              </div>
            </td>
          </tr>
          @endforeach

        </tbody>
      </table>



    </div>
  </div>
</div>

@endsection

