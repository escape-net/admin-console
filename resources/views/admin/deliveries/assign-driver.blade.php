<div class="card mt-4 animated slideInRight">
  <div class="card-body">

   <div class="az-header-center" style="margin:0">
    <input type="search" v-model="driver_param" class="form-control" placeholder="Search avaliable drivers" v-bind:disabled="searching == true" v-on:keyup.enter="search_drivers">
    <button class="btn"><i class="fas fa-search"></i></button>
  </div>

  <div class="az-profile-work-list mt-4" v-for="row in drivers" v-bind:key="row.id" v-if="drivers.length > 0">
    <div class="media">
      <div class="media-logo">
        <img src="{{asset('img/driver.png')}}" style="width:50px" class="img img-rounded">
      </div>
      <div class="media-body">
        <h6>@{{row.name}} <code>[@{{row.vehicle_type}}]</code> <br><small class="text-primary">@{{row.delivery_comment}}</small></h6>
        <span>
          @{{row.date}} - present 
          <button class="btn btn-purple btn-rounded btn-xs" style="float:right" @click="assign(row.id)">Assign</button>
        </span>
        <p>@{{row.phone}}, @{{row.deliveries}}</p>
      </div>
    </div>
  </div>

</div>
<div class="card-footer bd-t">
 <i class="typcn typcn-time"></i> {{$drivers_status}}
</div>
</div>