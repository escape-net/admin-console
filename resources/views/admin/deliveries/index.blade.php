@extends('admin.app')

@section('body')

<script type="text/javascript">
  let id = {{request()->input('id', 0)}}
</script>

<div class="az-content az-content-app az-content-contacts pd-b-0 mt-2" id="delivery-app">
  <div class="container">

    @include('admin.deliveries.sidebar')

    @include('admin.deliveries.delivery')

  </div>
</div>

@endsection

@section('script')
<script>

  $(function(){

    'use strict'
    new PerfectScrollbar('#azContactList', { suppressScrollX: true });

  });
</script>
<script type="text/javascript" src="{{asset('js/deliveries.js')}}?v=1.3"></script>
@endsection