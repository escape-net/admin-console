<div class="card mt-4 animated slideInRight">
  <div class="card-body">

   <div class="az-header-center" style="margin:0">
    <input type="text" v-model="promo_param" class="form-control" placeholder="Enter Promo Code" v-bind:disabled="searching == true" v-on:keyup.enter="add_promo_code">
    <button class="btn" @click="add_promo_code()"><i class="fas fa-plus"></i></button>
  </div>

</div>
<div class="card-footer bd-t">
 <i class="typcn typcn-time"></i> Promo Codes can only be applied once per user.
</div>
</div>