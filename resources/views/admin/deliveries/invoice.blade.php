<!DOCTYPE html>
<html lang="en">
<head>    
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="velocity.ng">
	<meta name="author" content="Escape Technologies">
	<title>INVOICE - {{$delivery->reference}} - velocity.ng</title>

	<link rel="stylesheet" href="{{asset('css')}}/azia.css?v=1.1">

</head>
<body class="az-body az-content-body">

	<div class="card card-invoice">
		<div class="card-body">
			<div class="invoice-header">
				<h1 class="invoice-title">Invoice</h1>
				<div class="billed-from">
					<h6><img src="{{asset('img/logo.png')}}"> velocity.ng</h6>
					<p>
						No 17a, June 12 Boulevard Road Abraham Adesanya Estate<br>
						Lekki Ajah, Lagos NG<br>
						Tel No: +234 807 766 6455<br>
						Email: deliveries@velocity.ng
					</p>
				</div>
			</div>

			<div class="row mg-t-20">
				<div class="col-md">
					<label class="tx-gray-600">Billed To</label>
					<div class="billed-to">
						<h6>{{$delivery->user->name}}</h6>
						<p>{{$delivery->user->address}}<br>
							Tel No:{{$delivery->user->phone}}<br>
						Email: {{$delivery->user->email}}</p>
					</div>
				</div>
				<div class="col-md">
					<label class="tx-gray-600">Invoice Information</label>
					<p class="invoice-info-row">
						<span>Invoice No</span>
						<span>{{$delivery->reference}}</span>
					</p>
					<p class="invoice-info-row">
						<span>Delivery Date</span>
						<span>{{_d($delivery->created_at, true)}}</span>
					</p>
					<p class="invoice-info-row">
						<span>Issue Date:</span>
						<span>{{_d(date('Y-m-d H:i:s'), true)}}</span>
					</p>
					<p class="invoice-info-row">
						<span>Due Date:</span>
						<span>{{_d($delivery->end_at, true)}}</span>
					</p>
				</div>
			</div>

			<div class="table-responsive mg-t-40">
				<table class="table table-invoice">
					<thead>
						<tr>
							<th class="wd-20p">Item</th>
							<th class="wd-40p">Description</th>
							<th class="tx-center">Total Time</th>
							<th class="tx-right">Total Distance</th>
							<th class="tx-right">Amount</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>{{$delivery->item}}</td>
							<td class="tx-12">Delivered by {{$delivery->driver->name}} via {{$delivery->vehicle_type->name}} </td>
							<td class="tx-center">{{$delivery->total_minutes}} Minutes</td>
							<td class="tx-right">{{$delivery->total_km}} KM</td>
							<td class="tx-right">{{_c($delivery->total)}}</td>
						</tr>

						<tr>
							<td colspan="2" rowspan="4" class="valign-middle">
								<div class="invoice-notes">
									<label class="az-content-label tx-13">Notes</label>
									<p>{{$delivery->instructions}}</p>
								</div>
							</td>
							<td class="tx-right">Sub-Total</td>
							<td colspan="2" class="tx-right">{{_c($delivery->subtotal)}}</td>
						</tr>
						<tr>
							<td class="tx-right">Surcharge</td>
							<td colspan="2"  class="tx-right">{{_c($delivery->surcharge)}}</td>
						</tr>
						<tr>
							<td class="tx-right">Discount</td>
							<td colspan="2" class="tx-right">{{_c($delivery->discount)}}</td>
						</tr>
						<tr>
							<td class="tx-right tx-uppercase tx-bold tx-inverse">Total Due</td>
							<td colspan="2" class="tx-right"><h4 class="tx-bold" style="color:#52009f">{{_c($delivery->total)}}</h4></td>
						</tr>
					</tbody>
				</table>
			</div>

			<hr class="mg-b-40">
		</div>
	</div>
</body>
</html>