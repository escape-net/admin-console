@extends('admin.app')

@section('body')

<div class="az-content az-content-body-dashboard-three">
  <div class="container">
    <div class="az-content-body">
      <div class="az-content-breadcrumb">
        <span>Velocity Admin</span>
        <span>Deliveries</span>
        <span>New Delivery</span>
      </div>

      <div class="row">
        <div class="col-md-8">
          <h2 class="az-content-title">New Delivery Form</h2>
        </div>
      </div>
      

      <form action="{{url('admin/deliveries/save')}}" method="POST" role="form">

        {{csrf_field()}}

        <div class="row">

          <div class="form-group col-12">
            <label>User | <small class="pull-right">Not an Exisiting user? <a href="{{url('admin/users/form')}}">Click here</a> to create a new user.</small></label>
            <select name="user_id" class="form-control select2" required>

              @foreach($users as $row)

              @if(old('user_id', $delivery->user_id) == $row->id)
              <option value="{{$row->id}}" selected>{{$row->name}}</option>
              @else
              <option value="{{$row->id}}">{{$row->name}}</option>
              @endif

              @endforeach
              
            </select>
            @if($errors->has('user_id'))
            <span class="form-error">{{$errors->first('user_id')}}</span>
            @endif
          </div>

        </div>

        <div class="row">

          <div class="form-group col-12">
            <label>Item Description</label>
            <input type="text" name="item" class="form-control" placeholder="Item Description" value="{{old('item', $delivery->item)}}" required>
            @if($errors->has('item'))
            <span class="form-error">{{$errors->first('item')}}</span>
            @endif
          </div>
          
          <div class="form-group col-6">
            <label>Vehicle Type</label>
            <select name="vehicle_type_id" class="form-control select2" required>

              @foreach($vehicle_types as $row)

              @if(old('vehicle_type_id', $delivery->vehicle_type_id) == $row->id)
              <option value="{{$row->id}}" selected>{{$row->name}}</option>
              @else
              <option value="{{$row->id}}">{{$row->name}}</option>
              @endif

              @endforeach
              
            </select>
            @if($errors->has('vehicle_type_id'))
            <span class="form-error">{{$errors->first('vehicle_type_id')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label>Schedule At</label>
            <input type="text" name="schedule_at" class="form-control date" placeholder="DD/MM/YYYY" value="{{old('schedule_at', $delivery->schedule_at)}}">
            @if($errors->has('schedule_at'))
            <span class="form-error">{{$errors->first('schedule_at')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label>Reciepient Name</label>
            <input type="text" name="recipient_name" class="form-control" placeholder="Reciepient Name" value="{{old('recipient_name', $delivery->recipient_name)}}" required>
            @if($errors->has('recipient_name'))
            <span class="form-error">{{$errors->first('recipient_name')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label>Reciepient Phone</label>
            <input type="text" name="recipient_phone" class="form-control" placeholder="Reciepient Phone" value="{{old('recipient_phone', $delivery->recipient_phone)}}" required>
            @if($errors->has('recipient_name'))
            <span class="form-error">{{$errors->first('recipient_name')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label>Pickup Location</label>
            <select name="pickup_place_id" class="form-control place" required>
              @if($delivery->delivery_place_id != '')
              <option value="{{$delivery->delivery_place->id}}" selected>{{$delivery->delivery_place->address}}</option>
              @endif
            </select>
            @if($errors->has('pickup_place_id'))
            <span class="form-error">{{$errors->first('pickup_place_id')}}</span>
            @endif
          </div>


          <div class="form-group col-6">
            <label>Delivery Location</label>
            <select name="delivery_place_id" class="form-control place" required>
              @if($delivery->delivery_place_id != '')
              <option value="{{$delivery->delivery_place->id}}" selected>{{$delivery->delivery_place->address}}</option>
              @endif
            </select>
            @if($errors->has('delivery_place_id'))
            <span class="form-error">{{$errors->first('delivery_place_id')}}</span>
            @endif
          </div>

          <div class="form-group col-12">
            <label>Delivery Instructions</label>
            <textarea name="instructions" class="form-control" placeholder="Delivery Instructions" rows="3">{{old('instructions', $delivery->instructions)}}</textarea>
          </div>

        </div>

        <input type="hidden" name="id" value="{{$delivery->id}}">
        <button type="submit" class="btn btn-indigo btn-rounded"><i class="fa fa-save"></i> Submit</button>

      </form>

    </div>
  </div>
</div>

@endsection

