<div ref="delivery" class="az-content-body az-content-body-contacts">

  @include('admin.deliveries.header')
  
  <div class="az-contact-info-body animated fadeIn" v-if="delivery.id && delivery.id > 0 && assign_driver == false" style="height:500px">
    <div class="media-list">

      <div class="media">
        <div class="media-icon"><i class="fas fa-shopping-bag"></i></div>
        <div class="media-body">
          <div>
            <label>Item</label>
            <span class="tx-medium">@{{delivery.item}}</span>
          </div>
          <div v-if="delivery.driver_id > 0">
            <label>Driver</label>
            <span class="tx-medium">
              @{{delivery.driver.name}} (@{{delivery.vehicle_type.name}})
            </span>
          </div>
          <div v-if="delivery.driver_id < 1 && delivery.vehicle_type.name.length > 0">
            <label>Prefered Vehicle</label>
            <span class="tx-medium">
              @{{delivery.vehicle_type.name}}
            </span>
          </div>
        </div>
      </div>

      <div class="media">
        <div class="media-icon"><i class="far fa-user"></i></div>
        <div class="media-body">
          <div>
            <label>Recipient Name</label>
            <span class="tx-medium">@{{delivery.recipient_name}}</span>
          </div>
          <div>
            <label>Recipient Phone</label>
            <span class="tx-medium">@{{delivery.recipient_phone}}</span>
          </div>
        </div>
      </div>


      <div class="media">
        <div class="media-icon align-self-start"><i class="far fa-map"></i></div>
        <div class="media-body">
          <div>
            <label>Pickup</label>
            <span class="tx-medium">@{{delivery.pickup_place.address}}</span>
          </div>
          <div>
            <label>Delivery</label>
            <span class="tx-medium">@{{delivery.delivery_place.address}}</span>
          </div>
        </div>
      </div>

      <div class="media">
        <div class="media-icon align-self-start"><i class="fas fa-wallet"></i></div>
        <div class="media-body">
          <div>
            <label>Payment Status</label>
            <span class="tx-medium">@{{delivery.transaction.id && delivery.transaction.id > 0 ? 'PAID' : 'PENDING'}}</span>
          </div>
          <div>
            <label>Payment Source</label>
            <span class="tx-medium">@{{delivery.transaction.source}}</span>
          </div>
          <div>
            <label>Amount Paid</label>
            <span class="tx-medium">@{{delivery.transaction.amount}}</span>
          </div>
        </div>
      </div>

      <div class="media">
        <div class="alert" style="width:100%">
          <strong>INSTRUCTIONS:</strong><br>
          <code>@{{delivery.instructions}}</code>
        </div>
      </div>

      <div class="media" v-if="complete.show == false">

        <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>BASE FARE</th>
              <th>PER MINUTE</th>
              <th>PER KM</th>
              <th>APPROX KM</th>
              <th>MINIMUM</th>
              <th>MAXIMUM</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td v-html="delivery.base_fare"></td>
              <td v-html="delivery.per_minute"></td>
              <td v-html="delivery.per_km"></td>
              <td v-html="delivery.approx_km"></td>
              <td v-html="delivery.minimum"></td>
              <td v-html="delivery.maximum"></td>
            </tr>
            <tr v-if="delivery.status.toLowerCase() != 'completed'">
              <td colspan="6" class="text-center">
                <button class="btn btn-outline-indigo btn-rounded text-center" @click="show_complete()">Complete Delivery Manually</button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div v-if="complete.show == true" class="mb-2">
        @include('admin.deliveries.complete-manually')
      </div>

      <div class="media" v-if="delivery.scheduled">
        <div class="media-icon align-self-start"><i class="far fa-calendar"></i></div>
        <div class="media-body">
          <div>
            <label>Scheduled At</label>
            <span class="tx-medium">@{{delivery.date}}</span>
          </div>
          <div>
            <label>Scheduled For</label>
            <span class="tx-medium">@{{delivery.scheduled_at}}</span>
          </div>
          <div>
            <label>Scheduled Comments</label>
            <span class="tx-medium">@{{delivery.schedule_difference}} Days from Today</span>
          </div>
        </div>
      </div>

      <div class="media" v-if="delivery.status.toLowerCase() == 'completed' ">

        <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>DISCOUNT</th>
              <th>SUBTOTAL</th>
              <th>TOTAL</th>
              <th>COMMISSION</th>
              <th>TIME</th>
              <th>DISTANCE</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td v-html="delivery.discount"></td>
              <td v-html="delivery.subtotal"></td>
              <td v-html="delivery.total"></td>
              <td v-html="delivery.commission"></td>
              <td v-html="delivery.total_minutes"></td>
              <td v-html="delivery.total_km"></td>
            </tr>
            <tr>
              <td colspan="6" class="text-center">Started at @{{delivery.start_at}} Ended At @{{delivery.end_at}}</td>
            </tr>
          </tbody>
        </table>
      </div>

      <div class="media" v-if="delivery.promotion_id > 0">

        <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>PROMOTION</th>
              <th>TYPE</th>
              <th>AMOUNT</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td v-html="delivery.promotion.code"></td>
              <td v-html="delivery.promotion.type.toUpperCase()"></td>
              <td v-html="delivery.promotion.amount"></td>
            </tr>
          </tbody>
        </table>
      </div>

      <div class="media" ref="map" style="height:400px;"></div>
      
      <div class="media" ref="history" id="history">
        <table class="table table-striped table-sm">          
          <tbody>
            <tr v-for="row in delivery.histories" v-bind:key="row.id">
              <td style="width:90%">
                <small><code>@{{row.created_at}}</code></small><br>
                @{{row.history}}
              </td>
            </tr>
          </tbody>
        </table>
      </div>

    </div>

  </div>

</div>