<div class="az-content-left az-content-left-contacts">

  <div class="az-content-breadcrumb lh-1 mg-b-2">
    <span>Velocity Admin</span>
    <span>Deliveries | <a href="javascript:;" style="margin-left: 10px;"><i class="fa fa-search"></i> Advanced Search</a></span>
  </div>
  <h2 class="az-content-title tx-24 mg-b-30">Deliveries</h2>

  <nav class="nav az-nav-line az-nav-line-chat">
    <a href="javascript:;" data-toggle="tab" class="nav-link active" @click="init()">All</a>
    <a href="javascript:;" data-toggle="tab" class="nav-link" @click="filter_deliveries('pending')">Pending <label class="badge badge-danger">@{{pending}}</label></a>
    <a href="javascript:;" data-toggle="tab" class="nav-link" @click="filter_deliveries('active')">Active <label class="badge badge-primary">@{{active}}</label></a>
    <a href="javascript:;" data-toggle="tab" class="nav-link" @click="filter_deliveries('completed')">Completed <label class="badge badge-success">@{{completed}}</label></a>
  </nav>


  <div id="azContactList" class="az-contacts-list">    
    <div v-for="row in deliveries" v-bind:key="row.id" @click="get_delivery(row.id)" v-bind:class="['az-contact-item', 'animated', 'tada', id === row.id ? 'selected' : '']">
      
      <div v-bind:class="['az-img-user', row.status]"><img v-bind:src="row.image" style="width:50px"></div>
      <div class="az-contact-body">
        <h6>@{{row.user}}</h6>
        <span class="phone" style="font-weight: bold">@{{row.item}} [@{{row.date}}]<br></span>
        <span class="phone">Pickup at @{{row.pickup}}</span>
      </div>
      <a href="#" v-bind:class="['az-contact-star', row.star]"><i class="typcn typcn-star"></i></a>

    </div>

    
  </div>

</div>