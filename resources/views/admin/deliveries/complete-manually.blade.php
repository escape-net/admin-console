<div class="card mt-4 animated bounceIn">
  <div class="card-header tx-medium bd-0 tx-white bg-indigo">
    Complete Delivery #@{{delivery.reference}} Manually<br>
    <small><i class="typcn typcn-time"></i> By completing this delivery manually, You can override the Standard Price calculation by changing the Delivery Total.</small>
  </div>

  <div class="card-body">

    <div class="az-header-center" style="margin:0">

      <div class="row">
        <div class="form-group col-6">
          <label>Total Distance (KM)</label>
          <input type="number" v-model="complete.total_km" class="form-control" placeholder="Enter Total Delivery Distance in KM" v-on:keyup="calculate_total()" step="0.1">
        </div>

        <div class="form-group col-6">
          <label>Total Time Taken (Minutes)</label>
          <input type="number" v-model="complete.total_minutes" class="form-control" placeholder="Enter Total Delivery Distance in KM" v-on:keyup="calculate_total()" step="0.1">
        </div>
      </div>

      <div class="row">
        <div class="form-group col-12">
          <label>Delivery Total</label>
          <input type="number" v-model="complete.total" class="form-control" placeholder="Enter Total Delivery Amount" v-bind:disabled="complete.submitting == true">
          <code v-show="complete.discount_text.length > 0">@{{complete.discount_text}}</code>
        </div>
      </div>

      <div class="row">
        <div class="form-group col-12">
          <label class="ckbox">
            <input type="checkbox" v-model="complete.add_payment"><span>Also add Payment to this Delivery</span>
          </label>
        </div>
      </div>

    </div>

  </div>
  <div class="card-footer bd-t">
    <button type="submit" class="btn btn-indigo btn-block btn-rounded text-center" @click="complete_delivery()">Save Delivery</button>
  </div>
</div>