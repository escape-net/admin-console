@extends('admin.app')

@section('body')

<div class="az-content az-content-mail">
  <div class="container">

    <div class="az-content-body az-content-body-mail">
      <div class="az-mail-header">
        <div>
          <h4 class="az-content-title mg-b-5">Search Results</h4>
        </div>
        <p style="font-size:18px;"><code>Found {{$deliveries->count()}} {{str_plural('Result', $deliveries->count())}} for <strong>*{{strtoupper($query)}}*</strong></code></p>

      </div>

      <div class="az-mail-list">

        @foreach($deliveries as $row)
        <div class="az-mail-item unread" onclick="window.location = '{{url('admin/deliveries/?id='.$row->id)}}'">
          <div class="az-mail-star">{{_badge($row->status)}}</div>
          <div class="az-img-user"><img src="../img/img16.jpg" alt=""></div>
          <div class="az-mail-body">
            <div class="az-mail-from">#{{$row->reference}} By {{$row->user->name}}</div>
            <div class="az-mail-subject">
              <strong>Item : {{$row->item}}</strong> &nbsp;
              <span>{{$row->instructions}}</span>
            </div>
          </div>
          <div class="az-mail-date">{{_d($row->created_at, true)}}</div>
        </div>
        @endforeach

        <div class="mg-lg-b-30"></div>

      </div>
    </div>
  </div>

  @endsection

