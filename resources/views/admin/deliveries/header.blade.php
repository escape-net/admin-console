<div class="az-contact-info-header animated fadeIn" v-if="delivery.id && delivery.id > 0">
  <div class="media">
    <div class="az-img-user" v-if="assign_driver == false">
      <img v-bind:src="delivery.user.photo">
      <a href="#"><i class="typcn typcn-camera-outline"></i></a>
    </div>
    <div class="media-body">
      <h4>@{{delivery.user.name}}</h4>
      <p>
        <small>
          <strong style="color:#52009f">#@{{delivery.reference}} </strong> - 
          TC: <strong><code> @{{delivery.tracking_code}}</code></strong> 
          <span style="float:right" class="text-info">Created: <strong>@{{delivery.date}}</strong></span>
        </small>
      </p>
      <nav class="nav" v-if="assign_driver == false && show_promo == false" style="font-size:10px;">

        @can('write', \App\Delivery::class)
        <a href="javasacript:;" class="nav-link" v-if="delivery.driver_id < 1 && delivery.status.toLowerCase() == 'pending'" @click="show_assign_driver()">
          <i class="fas fa-motorcycle"></i> Assign Driver
        </a>

        <a href="javasacript:;" class="nav-link" v-if="delivery.driver_id < 1 && delivery.status.toLowerCase() == 'pending'" @click="auto_assign_driver()">
          <i class="fas fa-user-cog"></i> Auto Assign Driver 
        </a>

        <a href="javasacript:;" class="nav-link" v-if="delivery.driver_id > 0 && delivery.transaction_id < 1 && delivery.status.toLowerCase() != 'pending'" @click="add_payment()">
          <i class="fas fa-wallet"></i> Add Payment
        </a>

        <a href="javasacript:;" class="nav-link" v-if="delivery.status.toLowerCase() != 'completed' && delivery.promotion_id < 1" @click="show_promo_code()">
          <i class="typcn typcn-link-outline"></i> Add a PromoCode
        </a>
        @endcan

        <a href="javasacript:;" class="nav-link" v-if="delivery.status.toLowerCase() == 'completed'" @click="open_invoice()">
          <i class="fas fa-file-invoice"></i> View Invoice
        </a>


      </nav>

      <div v-if="assign_driver == true">
        @include('admin.deliveries.assign-driver')
      </div>

      <div v-if="show_promo == true">
        @include('admin.deliveries.promo')
      </div>

    </div>
  </div>

  <div class="az-contact-action">
    <a href="javascript:;" v-bind:class="['nav-link', delivery.badge]">
      <i class="fas fa-info-circle"></i> @{{delivery.status.toUpperCase()}}
    </a>
    <a href="#history" class="nav-link">
      <i class="fas fa-history"></i> Track Delivery
    </a>
  </div>

</div>