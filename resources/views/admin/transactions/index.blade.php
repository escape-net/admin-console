@extends('admin.app')

@section('body')

<div class="az-content">
  <div class="container">
    <div class="az-content-body">
      <div class="az-content-breadcrumb">
        <span>Velocity Admin</span>
        <span>Transactions</span>
      </div>

      <div class="row">
        <div class="col-md-12">
          <h2 class="az-content-title">Transactions</h2>
        </div>
      </div>
      

      <table id="datatable1" class="display responsive nowrap">
        <thead>
          <tr>
            <th class="wd-15p">Delivery</th>
            <th class="wd-15p">Source</th>
            <th class="wd-20p">Amount</th>
            <th class="wd-20p">Status</th>
            <th class="wd-20p">Reference</th>
            <th class="wd-20p">Transaction Date</th>
          </tr>
        </thead>
        <tbody>

          @foreach($transactions as $row)
          <tr>
            <td><a href="{{url('admin/deliveries?id='.$row->delivery->id)}}">#{{$row->delivery->reference}}</a></td>
            <td>{{$row->source}}</td>
            <td>{{_c($row->amount)}}</td>
            <td>{{_badge($row->status)}}</td>
            <td>{{$row->transaction_reference}}</td>
            <td>{{_d($row->transaction_date, true)}}</td>
          </tr>
          @endforeach

        </tbody>
      </table>



    </div>
  </div>
</div>

@endsection

