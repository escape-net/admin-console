@extends('admin.app')

@section('body')

<div class="az-content az-content-body-dashboard-three">
  <div class="container">
    <div class="az-content-body">
      <div class="az-content-breadcrumb">
        <span>Velocity Admin</span>
        <span>Track Delivery Status</span>
      </div>

      <div class="row">
        <div class="col-md-8">
          <h2 class="az-content-title">Track Delivery Status Form</h2>
        </div>
      </div>
      

      <form action="{{url('admin/track-statuses/save')}}" method="POST" role="form">

        {{csrf_field()}}

        <div class="row">

          <div class="form-group col-6">
            <label>Order</label>
            <input type="number" name="order" class="form-control" placeholder="Order" value="{{old('order', $status->order)}}" required>
            @if($errors->has('order'))
            <span class="form-error">{{$errors->first('order')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label>Color Type</label>

            <select name="color" class="form-control" required>

              @foreach(['success', 'primary', 'warning', 'danger', 'purple', 'pink'] as $row)

              @if(old('color', $status->color) == $row)
              <option value="{{$row}}" selected>{{strtoupper($row)}}</option>
              @else
              <option value="{{$row}}">{{strtoupper($row)}}</option>
              @endif            

              @endforeach

            </select>

            @if($errors->has('color'))
            <span class="form-error">{{$errors->first('color')}}</span>
            @endif

          </div>

          <div class="form-group col-12">
            <label>Snippet</label>
            <input type="text" name="description" class="form-control" placeholder="Snippet" value="{{old('description', $status->description)}}" required>
            @if($errors->has('description'))
            <span class="form-error">{{$errors->first('description')}}</span>
            @endif
          </div>

        </div>

        <input type="hidden" name="id" value="{{$status->id}}">
        <button type="submit" class="btn btn-indigo btn-rounded"><i class="fa fa-save"></i> Submit</button>

      </form>

    </div>
  </div>
</div>

@endsection

