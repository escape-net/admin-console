@extends('admin.app')

@section('body')

<div class="az-content">
  <div class="container">
    <div class="az-content-body">
      <div class="az-content-breadcrumb">
        <span>Velocity Admin</span>
        <span>Track Delivery Statuses</span>
      </div>

      <div class="row">
        <div class="col-md-8">
          <h2 class="az-content-title">Track Delivery Statuses</h2>

        </div>
        <div class="col-md-4">
          <a class="btn btn-indigo btn-rounded" style="float:right" href="{{url('admin/track-statuses/form')}}"><i class="fa fa-plus"></i> Add new</a>
        </div>
      </div>
      

      <table id="datatable1" class="display responsive nowrap">
        <thead>
          <tr>
            <th class="wd-15p">Order</th>
            <th class="wd-15p">Snippet</th>
            <th class="wd-15p">Color</th>
            <th class="wd-25p">Action</th>
          </tr>
        </thead>
        <tbody>

          @foreach($statuses as $row)
          <tr>
            <td>#{{$row->order}}</td>
            <td>{{$row->description}}</td>
            <td>{{strtoupper($row->color)}}</td>
            <td>
              <a href="{{url('admin/track-statuses/form/'.$row->id)}}" class="btn btn-indigo btn-icon btn-sm">
                <i class="typcn typcn-pencil"></i>
              </a>
            </td>
          </tr>
          @endforeach

        </tbody>
      </table>
    </div>
  </div>
</div>

@endsection

