@extends('admin.app')

@section('body')

<div class="az-content">
  <div class="container">
    <div class="az-content-body">
      <div class="az-content-breadcrumb">
        <span>Velocity Admin</span>
        <span>Promotions</span>
      </div>

      <div class="row">
        <div class="col-md-8">
          <h2 class="az-content-title">Promotions</h2>

        </div>

        @can('write', \App\Promotion::class)
        <div class="col-md-4">
          <a class="btn btn-indigo btn-rounded" style="float:right" href="{{url('admin/promotions/form')}}"><i class="fa fa-plus"></i> Add new</a>
        </div>
        @endcan
        
      </div>
      

      <table id="datatable1" class="display responsive nowrap">
        <thead>
          <tr>
            <th class="wd-15p">Code</th>
            <th>Type</th>
            <th>Amount</th>
            <th>Validity</th>
            <th>Limit</th>
            <th>Used</th>
            <th>Status</th>
            @can('write', \App\Promotion::class)
            <th>Action</th>
            @endcan
          </tr>
        </thead>
        <tbody>

          @foreach($promotions as $row)
          <tr>
            <td>{{$row->code}}</td>
            <td>{{_badge($row->type)}}</td>
            <td>{{number_format($row->amount, 2)}}</td>
            <td>{{strtoupper($row->validity)}}</td>
            <td>{{$row->limit}}</td>
            <td>0</td>
            <td>{{_badge($row->status)}}</td>
            @can('write', \App\Promotion::class)
            <td>
              <a href="{{url('admin/promotions/form/'.$row->id)}}" class="btn btn-indigo btn-icon btn-sm">
                <i class="typcn typcn-pencil"></i>
              </a>
            </td>
            @endcan
          </tr>
          @endforeach

        </tbody>
      </table>

    </div>
  </div>
</div>

@endsection

