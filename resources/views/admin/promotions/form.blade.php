@extends('admin.app')

@section('body')

<div class="az-content az-content-body-dashboard-three" id="promotion-app">
  <div class="container">
    <div class="az-content-body">
      <div class="az-content-breadcrumb">
        <span>Velocity Admin</span>
        <span>Promotions</span>
      </div>

      <div class="row">
        <div class="col-md-8">
          <h2 class="az-content-title">Promotion Form</h2>
        </div>
      </div>


      <form action="{{url('admin/promotions/save')}}" method="POST" role="form">

        {{csrf_field()}}

        <div class="row">

          <div class="form-group col-6">
            <label>Promotion Code</label>  
            <div class="input-group">
              <input type="text" name="code" v-model="promocode" class="form-control" placeholder="Promotion Code" value="{{old('code', $promotion->code)}}" required>
              <div class="input-group-append" style="cursor:pointer">
                <span class="input-group-text" @click="generate_code()">Generate Code</span>
              </div>
            </div>          

            @if($errors->has('code'))
            <span class="form-error">{{$errors->first('code')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label  {!! _tooltip('velocity.tooltips.promotion.user', true) !!} ref="user"><i class="fa fa-info-circle"></i> Users</label>
            <select name="users[]" class="form-control" ref="users" required multiple>
              <option value="0" {{in_array(0, $selected) ? 'selected' : ''}}>AA - All Users</option>
              @foreach($users as $row)
              @if(in_array($row->id, $selected))
                <option value="{{$row->id}}" selected>{{$row->name}}</option>
              @else
                <option value="{{$row->id}}">{{$row->name}}</option>
              @endif
              @endforeach
            </select>
            @if($errors->has('last_name'))
            <span class="form-error">{{$errors->first('last_name')}}</span>
            @endif
          </div>

        </div>

        <div class="row">

          <div class="form-group col-6">

            <label {!! _tooltip('velocity.tooltips.promotion.type', true) !!} ref="type_ref"><i class="fa fa-info-circle"></i> Type</label>

            <select name="type" class="form-control" required>

              @if($promotion->type == 'flat')
              <option value="flat" selected>Flat</option>
              <option value="percentage">Percentage</option>
              @else
              <option value="flat">Flat</option>
              <option value="percentage" selected>Percentage</option>
              @endif

            </select>

          </div>

          <div class="form-group col-6">
            <label>Amount</label>
            <input type="text" name="amount" class="form-control" placeholder="Amount" value="{{old('amount', $promotion->amount)}}" required>
            @if($errors->has('amount'))
            <span class="form-error">{{$errors->first('amount')}}</span>
            @endif
          </div>

        </div>

        <div class="row">

          <div class="form-group col-12">
            <label>Validity</label>
            <select name="validity" v-model="validity" class="form-control" required>

              @if($promotion->validity == 'forever')
              <option value="forever" selected>Until Disabled</option>
              <option value="date">For a Period</option>
              @else
              <option value="forever">Until Disabled</option>
              <option value="date" selected>For a Period</option>
              @endif

            </select>
          </div>

          <div class="form-group col-6 animated fadeIn" v-show="validity == 'date' ">
            <label>Start Date</label>
            <div class="input-group">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <i class="typcn typcn-calendar-outline tx-24 lh--9 op-6"></i>
                </div>
              </div>
              <input type="text" name="start_at" ref="start_at" class="form-control date" placeholder="DD-MM-YYYY" value="{{old('start_at', $promotion->start_at)}}" >
            </div>
            @if($errors->has('start_at'))
            <span class="form-error">{{$errors->first('start_at')}}</span>
            @endif
          </div>

          <div class="form-group col-6 animated fadeIn" v-show="validity == 'date' ">
            <label>End Date</label>
            <div class="input-group">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <i class="typcn typcn-calendar-outline tx-24 lh--9 op-6"></i>
                </div>
              </div>
              <input type="text" name="end_at" class="form-control date" placeholder="DD-MM-YYYY" value="{{old('end_at', $promotion->end_at)}}" >
            </div>
            @if($errors->has('end_at'))
            <span class="form-error">{{$errors->first('end_at')}}</span>
            @endif

          </div>


        </div>

        <div class="row">

          <div class="form-group col-12">
            <label>Description</label>
            <textarea name="description" class="form-control" rows="5" required>{{old('description', $promotion->description)}}</textarea>
            @if($errors->has('description'))
            <span class="form-error">{{$errors->first('description')}}</span>
            @endif
          </div>

        </div>

        <div class="row">

          <div class="form-group col-6">
            <label {!! _tooltip('velocity.tooltips.promotion.limit', true) !!} ref="limit"><i class="fa fa-info-circle"></i> Usage Limit</label>
            <input type="text" name="limit" class="form-control" placeholder="Usage Limit" value="{{old('limit', $promotion->limit)}}" required>
            @if($errors->has('limit'))
            <span class="form-error">{{$errors->first('limit')}}</span>
            @endif
          </div>

          <div class="form-group col-6">
            <label>Status</label>
            <select name="status" class="form-control" required>

              @if($promotion->status == 'active')
              <option value="active" selected>Active</option>
              <option value="inactive">Inactive</option>
              @else
              <option value="active">Active</option>
              <option value="inactive" selected>Inactive</option>
              @endif

            </select>
            @if($errors->has('status'))
            <span class="form-error">{{$errors->first('status')}}</span>
            @endif
          </div>

        </div>

        <input type="hidden" name="id" value="{{$promotion->id}}">
        <button type="submit" class="btn btn-indigo btn-rounded"><i class="fa fa-save"></i> Submit</button>

      </form>

    </div>
  </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
  let promocode = '{{$promotion->code}}'
  let validity = '{{$promotion->validity}}'
</script>
<script type="text/javascript" src={{asset('js/promotion.js')}}></script>
@endsection
