@extends('admin.email')

@section('body')

<p><strong>Hi {{$delivery->user->name}},</strong></p>
<p>
	<strong>{{$delivery->driver->name}}</strong> with Plate Number <strong>{{$delivery->driver->vehicle->plate_number}}</strong> Has been assigned to your Delivery <strong>#{{$delivery->reference}}</strong>.

	<br><br>

	We would update you on any activity on your Delivery.
</p>

@endsection