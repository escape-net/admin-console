@extends('admin.email')

@section('body')

<p><strong>Hi {{$delivery->user->name}},</strong></p>
<p>
	Thank you for your Delivery!<br><br>

	Your Delivery <strong>#{{$delivery->reference}}</strong> has been compeleted successfully.
</p>

@endsection