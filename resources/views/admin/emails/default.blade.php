@extends('admin.email')

@section('body')

<p>Hi {{$user->name}},</p>
<p>I noticed you were interested in learning more about growing --Company=your business--. I would like to <a href="http://www.autopilothq.com/">invite you</a> to our free 30 day business growth course. Or, you can <a href="http://www.autopilothq.com/">signup</a> for a free complimentary 45 minute session for being such a valuable contributor to our Facebook community.
	<br><br>
Just to confirm, is this your first time starting a business, or have you done it before? </p>

@endsection