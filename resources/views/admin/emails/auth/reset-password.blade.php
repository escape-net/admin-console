@extends('admin.email')

@section('body')

<p><strong>Hi {{$user->name}},</strong></p>
<p>
	You have requested to reset the password to your account on Velocity. Click the link below to get started.<br><br>

	<a href="{{url('reset-password/'.$token)}}">{{url('reset-password/'.md5($token))}}</a>
</p>

@endsection