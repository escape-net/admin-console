@extends('admin.app')

@section('body')

<div class="az-content">
  <div class="container">
    <div class="az-content-body" id="map" style="height:1000px">

    </div>
  </div>
</div>

@endsection

@section('script')
<script type="text/javascript">

  function initMap(){

    let options = { center: new google.maps.LatLng(6.6038838, 3.3415067), zoom: 15, mapTypeId: google.maps.MapTypeId.ROADMAP };
    let map = new google.maps.Map(document.getElementById('map'), options);
  }

  initMap();

</script>
@endsection

