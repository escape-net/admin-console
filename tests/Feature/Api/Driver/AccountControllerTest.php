<?php

namespace Tests\Feature\Api\Driver;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class AccountControllerTest extends TestCase
{
	/** @test */
	public function it_can_upload_driver_photo()
	{
		$driver = factory(User::class)->create(['role' => 'driver']);

		$payload = [

			'image'	=> base64_encode(file_get_contents('http://velocity.ng/admin/img/logo.png'))
		];

		$this->actingAs($driver, 'api')
		->json('POST', '/api/driver/upload/photo', [])
		->assertStatus(200)
		->assertJsonFragment([

			'status' => false,
		])
		->assertJsonStructure([

			'status',
			'data'
		]);

		$this->actingAs($driver, 'api')
		->json('POST', '/api/driver/upload/photo', $payload)
		->assertStatus(200)
		->assertJsonFragment([

			'status' => true
		])
		->assertJsonStructure([

			'status',
			'data'
		]);

	}

	/** @test */
	public function it_can_upload_driver_vehicle_image()
	{
		$driver = User::where(['role' => 'driver'])->first();

		$payload = [

			'image'	=> base64_encode(file_get_contents('http://velocity.ng/admin/img/logo.png'))
		];

		$this->actingAs($driver, 'api')
		->json('POST', '/api/driver/upload-vehicle/image', [])
		->assertStatus(200)
		->assertJsonFragment([

			'status' => false,
		])
		->assertJsonStructure([

			'status',
			'data'
		]);

		$response = $this->actingAs($driver, 'api')
		->json('POST', '/api/driver/upload-vehicle/image', $payload)
		->assertStatus(200)
		->assertJsonFragment([

			'status' => true
		])
		->assertJsonStructure([

			'status',
			'data'
		]);

	}

	/** @test */
    public function it_can_edit_driver_profile()
    {
        $driver = User::where(['role' => 'driver'])->first();
               
        $payload = [

            'first_name'    => 'Test',
            'last_name'     => 'Test'
        ];

        $response = $this->actingAs($driver, 'api')
        ->json('POST', '/api/driver/edit-profile', $payload)
        ->assertStatus(200)
        ->assertJsonFragment([

            'status' => true
        ])
        ->assertSee('Test')
        ->assertJsonStructure([

            'status',
            'data'
        ]);
    }

    /** @test */
    public function it_can_edit_vehicle_information()
    {
        $driver = User::where(['role' => 'driver'])->first();
               
        $payload = [

            'make'  => 'Test'
        ];

        $response = $this->actingAs($driver, 'api')
        ->json('POST', '/api/driver/edit-vehicle', $payload)
        ->assertStatus(200)
        ->assertJsonFragment([

            'status' => true
        ])
        ->assertSee('Test')
        ->assertJsonStructure([

            'status',
            'data'
        ]);
    }

    /** @test */
    public function it_can_edit_bank_information()
    {
        $driver = User::where(['role' => 'driver'])->first();
               
        $payload = [

            'bank_name'  => 'Test'
        ];

        $response = $this->actingAs($driver, 'api')
        ->json('POST', '/api/driver/bank-details', $payload)
        ->assertStatus(200)
        ->assertJsonFragment([

            'status' => true
        ])
        ->assertSee('Test')
        ->assertJsonStructure([

            'status',
            'data'
        ]);
    }
}
