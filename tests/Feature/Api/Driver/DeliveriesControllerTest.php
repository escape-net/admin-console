<?php

namespace Tests\Feature\Api\Driver;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\VehicleType;
use App\Delivery;

class DeliveriesControllerTest extends TestCase
{
    /** @test */
    public function it_can_get_all_deliveries()
    {
        $driver = factory(User::class)->create(['role' => 'driver']);

        $this->actingAs($driver, 'api')
        ->json('GET', '/api/driver/deliveries')
        ->assertStatus(200)
        ->assertJsonStructure([

            'status',

            'data' => [

                'assigned',
                'active',
                'completed'
            ]
        ]);
    }

    /** @test */
    public function it_can_get_single_deliveries()
    {
        $driver = factory(User::class)->create(['role' => 'driver']);
        $user   = factory(User::class)->create(['role' => 'user']);

        $vehicle_type = VehicleType::create([

            'name'          => 'Bicycle',
            'base_fare'     => 500,
            'per_minute'    => 100,
            'per_km'        => 15
        ]);

        $payload = [

            'delivery' => [

                'address'   => 'Alagbado, Lagos Nigeria',
                'latitude'  => 6.658440299999999,
                'longitude' => 3.2633918,
                'place_id'  => 'ChIJR2vnOVSXOxAR1NPJND9941Q'
            ],
            'pickup'    => [

                'address'   => '8 Odenike St, Abule ijesha 100001, Lagos, Nigeria',
                'latitude'  => 6.5246963,
                'longitude' => 3.3791974,
                'place_id'  => 'ChIJld0AqqqNOxARfk28KlgOfS4'
            ],
            'item'              => 'Bag',
            'payment'           => 'card',
            'recipient_name'    => 'Olson',
            'recipient_phone'   => '08175020329',
            'vehicle_type_id'   => $vehicle_type->id
        ];

        $response = $this->actingAs($user, 'api')
        ->json('POST', '/api/user/delivery/new', $payload)
        ->assertStatus(200)
        ->assertJsonFragment([

            'status' => true,
        ])
        ->assertJsonStructure([

            'status',
            'data'
        ]);

        $content = $response->decodeResponseJson();

        $delivery_id = $content['data']['id'];

        $this->actingAs($driver, 'api')
        ->json('GET', '/api/driver/delivery/'.$delivery_id)
        ->assertStatus(200)
        ->assertJsonFragment([

            'status' => true,
        ])
        ->assertJsonStructure([

            'status',
            'data'
        ]);
    }

    /** @test */
    public function it_can_start_deliveries()
    {
        //using the driver created at it_can_get_all_deliveries
        $driver    = User::where(['role' => 'driver'])->first();

        //The delviery created in the above test (it_can_get_single_delivery) can be used here.
        $delivery   = Delivery::first();

        $response = $this->actingAs($driver, 'api')
        ->json('GET', '/api/driver/delivery-start/0')
        ->assertStatus(200)
        ->assertJsonFragment([

            'status' => false,
        ])
        ->assertSee('Delivery Not Found.')
        ->assertJsonStructure([

            'status',
            'data'
        ]);

        $delivery->update(['driver_id' => $driver->id, 'status' => 'driver_assigned']);
        $driver->update(['driver_status' => 'assigned']);

        $response = $this->actingAs($driver, 'api')
        ->json('GET', '/api/driver/delivery-start/'.$delivery->id)
        ->assertStatus(200)
        ->assertJsonFragment([

            'status' => true,
        ])
        ->assertSee('Delivery has been Started!')
        ->assertJsonStructure([

            'status',
            'data'
        ]);
    }

    /** @test */
    public function it_can_complete_deliveries()
    {
        //using the driver created at it_can_get_all_deliveries
        $driver    = User::where(['role' => 'driver'])->first();

        //The delviery created in the above test (it_can_start_deliveries) can be used here.
        $delivery   = Delivery::first();

        $response = $this->actingAs($driver, 'api')
        ->json('GET', '/api/driver/delivery-complete/0')
        ->assertStatus(200)
        ->assertJsonFragment([

            'status' => false,
        ])
        ->assertSee('Delivery Not Found.')
        ->assertJsonStructure([

            'status',
            'data'
        ]);

        $response = $this->actingAs($driver, 'api')
        ->json('GET', '/api/driver/delivery-complete/'.$delivery->id)
        ->assertStatus(200)
        ->assertJsonFragment([

            'status' => true,
        ])
        ->assertSee('Delivery has been completed Succesfully')
        ->assertJsonStructure([

            'status',
            'data'
        ]);
    }

    /** @test */
    public function it_can_change_driver_status()
    {
        //using the driver created at it_can_get_all_deliveries
        $driver    = User::where(['role' => 'driver'])->first();

        $this->actingAs($driver, 'api')
        ->json('GET', '/api/driver/status/online')
        ->assertStatus(200)
        ->assertJsonStructure([

            'status',
            'data'
        ]);
    }

    /** @test */
    public function it_can_decline_delivery()
    {
        //using the driver created at it_can_get_all_deliveries
        $driver    = User::where(['role' => 'driver'])->first();

        //The delviery created in the above test (it_can_start_deliveries) can be used here.
        $delivery   = Delivery::first();

        $delivery->update(['driver_id' => $driver->id, 'status' => 'driver_assigned']);
        $driver->update(['driver_status' => 'assigned']);
        
        $response = $this->actingAs($driver, 'api')
        ->json('GET', '/api/driver/decline-delivery/0')
        ->assertStatus(200)
        ->assertJsonFragment([

            'status' => false,
        ])
        ->assertSee('Delivery Not found')
        ->assertJsonStructure([

            'status',
            'data'
        ]);

        $response = $this->actingAs($driver, 'api')
        ->json('GET', '/api/driver/decline-delivery/'.$delivery->id)
        ->assertStatus(200)
        ->assertJsonFragment([

            'status' => true,
        ])
        ->assertSee('Delivery has been declined.')
        ->assertJsonStructure([

            'status',
            'data'
        ]);
    }
}
