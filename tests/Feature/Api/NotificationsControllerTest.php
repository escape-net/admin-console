<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class NotificationsControllerTest extends TestCase
{
    /** @test */
    public function it_can_get_all_notifications()
    {               
        $user = factory(User::class)->create(['role' => 'user']);

        $response = $this->actingAs($user, 'api')
        ->json('GET', '/api/notifications', [])
        ->assertStatus(200)
        ->assertJsonFragment([

            'status' => true
        ])
        ->assertJsonStructure([

            'status',
            'data'
        ]);
    }
}
