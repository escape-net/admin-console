<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class AuthControllerTest extends TestCase
{
    
    /** @test */
    public function it_can_register_new_user()
    {               
        $payload = [

            'phone'			=> '08175020329',
            'email'			=> 'vadeshayo@gmail.com',
            'first_name'	=> 'Massive',
            'last_name'		=> 'Brains',
            'device'		=> 'android'
        ];

        $response = $this->json('POST', '/api/register', [])
        ->assertStatus(422)
        ->assertJsonFragment([

            'status' => false
        ])
        ->assertJsonStructure([

            'status',
            'data'
        ]);

        $response = $this->json('POST', '/api/register', $payload)
        ->assertStatus(200)
        ->assertJsonFragment([

            'status' => true
        ])
        ->assertJsonStructure([

            'status',
            'data'
        ]);

        $this->assertDatabaseHas('users',[

        	'email'	=> 'vadeshayo@gmail.com'
        ]);
    }

    /** @test */
    public function it_can_login_user()
    {               
        $payload = [

            'phone'	=> '08175020329'
        ];

        $response = $this->json('POST', '/api/login', [])
        ->assertStatus(422)
        ->assertJsonFragment([

            'status' => false
        ])
        ->assertJsonStructure([

            'status',
            'data'
        ]);

        $response = $this->json('POST', '/api/login', $payload)
        ->assertStatus(200)
        ->assertJsonFragment([

            'status' => true
        ])
        ->assertJsonStructure([

            'status',
            'data'
        ]);
    }

    /** @test */
    public function it_can_update_device_token()
    {               
        $payload = [

            'token'	=> str_random(16)
        ];

        $user = User::where(['role' => 'user'])->first();

        $response = $this->actingAs($user, 'api')
        ->json('POST', '/api/update-device-token', [])
        ->assertStatus(200)
        ->assertJsonFragment([

            'status' => false
        ])
        ->assertJsonStructure([

            'status',
            'data'
        ]);

        $response = $this->actingAs($user, 'api')
        ->json('POST', '/api/update-device-token', $payload)
        ->assertStatus(200)
        ->assertJsonFragment([

            'status' => true
        ])
        ->assertJsonStructure([

            'status',
            'data'
        ]);
    }

    /** @test */
    public function it_can_logout()
    {               
        $user = factory(User::class)->create(['role' => 'user']);

        $response = $this->actingAs($user, 'api')
        ->json('GET', '/api/logout', [])
        ->assertStatus(200)
        ->assertJsonFragment([

            'status' => true
        ])
        ->assertSee('Logout Successful')
        ->assertJsonStructure([

            'status',
            'data'
        ]);
    }
}
