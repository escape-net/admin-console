<?php

namespace Tests\Feature\Api\User;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\VehicleType;

class DeliveriesControllerTest extends TestCase
{
	/** @test */
	public function it_can_estimate_delivery()
	{
		$user = factory(User::class)->create(['role' => 'user']);

		$vehicle_type = VehicleType::create([

			'name'          => 'Bicycle',
			'base_fare'     => 500,
			'per_minute'    => 100,
			'per_km'        => 15
		]);

		$payload = [

			'vehicle_type_id' 		=> $vehicle_type->id,
			'pickup_latitude'		=> 6.5244,
			'pickup_longitude'		=> 3.3792,
			'delivery_latitude'		=> 6.4698419,
			'delivery_longitude'	=> 3.5851718000000004
		];


		$this->actingAs($user, 'api')
		->json('POST', '/api/user/delivery/estimate', [])
		->assertStatus(422)
		->assertJsonFragment([

			'status' => false,
		])
		->assertJsonStructure([

			'status',
			'data'
		]);

		$this->actingAs($user, 'api')
		->json('POST', '/api/user/delivery/estimate', $payload)
		->assertStatus(200)
		->assertJsonFragment([

			'status' => true,
		])
		->assertJsonStructure([

			'status',
			'data'
		]);
	}

	/** @test */
	public function it_can_assign_driver()
	{
		$driver = factory(User::class)->create(['role' => 'driver']);
		$user   = User::where(['role' => 'user'])->first();

		$vehicle_type = VehicleType::first();

		$payload = [

			'delivery' => [

				'address'   => 'Alagbado, Lagos Nigeria',
				'latitude'  => 6.658440299999999,
				'longitude' => 3.2633918,
				'place_id'  => 'ChIJR2vnOVSXOxAR1NPJND9941Q'
			],
			'pickup'    => [

				'address'   => '8 Odenike St, Abule ijesha 100001, Lagos, Nigeria',
				'latitude'  => 6.5246963,
				'longitude' => 3.3791974,
				'place_id'  => 'ChIJld0AqqqNOxARfk28KlgOfS4'
			],
			'item'              => 'Bag',
			'payment'           => 'card',
			'recipient_name'    => 'Olson',
			'recipient_phone'   => '08175020329',
			'vehicle_type_id'   => $vehicle_type->id
		];

		$response = $this->actingAs($user, 'api')
		->json('POST', '/api/user/delivery/new', $payload)
		->assertStatus(200)
		->assertJsonFragment([

			'status' => true,
		])
		->assertJsonStructure([

			'status',
			'data'
		]);

		$content = $response->decodeResponseJson();

		$delivery_id = $content['data']['id'];

		$payload = [

			'latitude'		=> 6.5246963,
			'longitude'		=> 3.3791974,
			'delivery_id'	=> $delivery_id
		];

		$this->actingAs($user, 'api')
		->json('POST', '/api/user/assign-driver', [])
		->assertStatus(422)
		->assertJsonFragment([

			'status' => false,
		])
		->assertJsonStructure([

			'status',
			'data'
		]);

		$response = $this->actingAs($user, 'api')
		->json('POST', '/api/user/assign-driver', $payload)
		->assertStatus(200)
		->assertJsonStructure([

			'status',
			'data'
		]);
	}

	/** @test */
    public function it_can_get_all_deliveries()
    {
        $user = User::where(['role' => 'user'])->first();

        $this->actingAs($user, 'api')
        ->json('GET', '/api/user/deliveries')
        ->assertStatus(200)
        ->assertJsonStructure([

            'status',

            'data' => [

                'active',
                'completed'
            ]
        ]);
    }

    /** @test */
    public function it_can_get_all_places()
    {
        $user = User::where(['role' => 'user'])->first();

        $this->actingAs($user, 'api')
        ->json('GET', '/api/user/places')
        ->assertStatus(200)
        ->assertJsonStructure([

            'status',
            'data'
        ]);
    }

    public function it_can_create_new_delivery()
    {
    	//This has been tested in it_can_assign_driver above.

    	//Me i'm too tired to test again. e dey work.
        //return true;
    }
}
