<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class UtilitiesControllerTest extends TestCase
{
    /** @test */
    public function it_can_update_location()
    {               
        $user = factory(User::class)->create(['role' => 'user']);

        $response = $this->actingAs($user, 'api')
        ->json('GET', '/api/update-location')
        ->assertStatus(200)
        ->assertJsonFragment([

            'status' => false
        ])
        ->assertSee('Latitude or Longitude is invalid')
        ->assertJsonStructure([

            'status',
            'data'
        ]);

        $response = $this->actingAs($user, 'api')
        ->json('GET', '/api/update-location/0.2/0.3')
        ->assertStatus(200)
        ->assertJsonFragment([

            'status' => true
        ])
        ->assertJsonStructure([

            'status',
            'data'
        ]);
    }

    /** @test */
    public function it_can_get_vehicle_types()
    {               
        $user = factory(User::class)->create(['role' => 'user']);

        $response = $this->actingAs($user, 'api')
        ->json('GET', '/api/vehicle-types')
        ->assertStatus(200)
        ->assertJsonFragment([

            'status' => true
        ])
        ->assertJsonStructure([

            'status',
            'data'
        ]);
    }

    /** @test */
    public function it_can_get_banks()
    {               
        $user = factory(User::class)->create(['role' => 'user']);

        $response = $this->actingAs($user, 'api')
        ->json('GET', '/api/banks')
        ->assertStatus(200)
        ->assertJsonFragment([

            'status' => true
        ])
        ->assertJsonStructure([

            'status',
            'data'
        ]);
    }
}
