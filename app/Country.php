<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $hidden 	= ['updated_at'];
	protected $guarded 	= ['updated_at'];
}
