<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Broadcast extends Model
{
    protected $hidden 	= ['updated_at'];
	protected $guarded 	= ['updated_at'];

	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
