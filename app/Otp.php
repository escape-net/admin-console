<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Otp extends Model
{
    protected $hidden 	= ['updated_at', 'deleted_at'];
	protected $guarded 	= ['updated_at'];
}
