<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackHistory extends Model
{
    protected $hidden = ['updated_at'];
	protected $guarded 	= ['updated_at'];

	public function delivery()
	{
		return $this->belongsTo('App\Delivery');
	}

	public function track_status()
	{
		return $this->belongsTo('App\TrackStatus');
	}
}
