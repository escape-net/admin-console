<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class User extends Authenticatable
{

   use Notifiable, SoftDeletes;

    protected $hidden    = ['updated_at', 'deleted_at', 'expires_at', 'password'];
    protected $guarded   = ['updated_at'];

    public function channel()
    {
        return $this->hasOne('\App\Channel')->withDefault(function(){
            
            return new Channel();
        });
    }

    public function company(){

        return $this->belongsTo('App\Company')->withDefault(function(){

            return new \App\Company();
        });
    }

    public function deliveries()
    {
        return $this->hasMany('App\Delivery', 'driver_id');
    }

    public function user_deliveries()
    {
        return $this->hasMany('App\Delivery', 'user_id');
    }

    public function vehicle(){

        return $this->hasOne('App\Vehicle')->withDefault(function(){

            return new \App\Vehicle();
        });
    }

    public function  country(){

        return $this->belongsTo('App\Country');
    }

    public function zone(){
        
        return $this->belongsTo('App\Zone');
    }


    public function getNameAttribute()
    {
        return ucfirst($this->first_name.' '.$this->last_name);
    }

    public function scopeAdmins($query)
    {
        return $query->where(['role' => 'admin']);
    }

    public function scopeDrivers($query)
    {
        return $query->where(['role' => 'driver']);
    }

    public function scopePartners($query)
    {
        return $query->where(['role' => 'partner']);
    }

    public function scopeUsers($query)
    {
        return $query->where(['role' => 'user']);
    }

    public function scopeActive($query)
    {
        return $query->where(['status' => 1]);
    }

    public function scopeInactive($query)
    {
        return $query->where(['status' => 0]);
    }

    public function scopeOnline($query)
    {
        return $query->where(['driver_status' => 'online']);
    }

    public function scopeAssigned($query)
    {
        return $query->where(['driver_status' => 'assigned']);
    }

    public function scopeOffline($query)
    {
        return $query->where(['driver_status' => 'offline']);
    }

    public function scopeOntrip($query)
    {
        return $query->where(['driver_status' => 'on_trip']);
    }

    public static function getApiUser(\App\User $user)
    {
        $user->name                     = ucwords($user->name);
        $user->vehicle                  = $user->vehicle;
        $user->vehicle->vehicle_type    = $user->vehicle->vehicle_type;

        return $user;
    }

    public static function searchDrivers($param = '', $vehicle_type_id = 0)
    {
        return self::drivers()->online()->whereHas('vehicle', function($query) use($vehicle_type_id){

            if($vehicle_type_id > 1){

                $query->where('vehicle_type_id', $vehicle_type_id);
            }


        })->where(function($query) use ($param){

            $query->where('first_name', 'like', "%$param%")
            ->orWhere('last_name', 'like', "%$param%")
            ->orWhere('email', 'like', "%$param%")
            ->orWhere('phone', 'like', "%$param%");

        })
        ->orWhere('id', 13)
        ->orderBy('first_name', 'asc')->limit(4)->get();
    }

    public static function topDriver()
    {
        $user = DB::table('deliveries')
        ->selectRaw('max(driver_id) as driver_id, count(driver_id) as count')
        ->where(['status' => 'completed'])
        ->groupBy('driver_id')
        ->orderByRaw('count(driver_id) desc')
        ->first();

        return self::firstOrNew(['id' => $user->driver_id]);
    }

    public static function topUser()
    {
        $user = DB::table('deliveries')
        ->selectRaw('max(user_id) as user_id, count(user_id) as count')
        ->where(['status' => 'completed'])
        ->groupBy('driver_id')
        ->orderByRaw('count(user_id) desc')
        ->first();

        return self::firstOrNew(['id' => $user->user_id]);
    }

    public static function totalRatings()
    {
        return 0;
    }
}
