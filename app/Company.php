<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $hidden 	= ['updated_at'];
	protected $guarded 	= ['updated_at'];
	
	public function country()
	{
		return $this->belongsTo('App\Country');
	}

	public function zone()
	{
		return $this->belongsTo('App\Zone');
    }
    
    public function users(){
    	
        return $this->hasMany('App\User');
    }

    public function scopeActive($query)
    {
        return $query->where(['status' => 'active']);
    }

    public function scopeInactive($query)
    {
        return $query->where(['status' => 'inactive']);
    }
}
