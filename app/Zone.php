<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    protected $table 	= 'country_zones';
	protected $hidden 	= ['updated_at'];

	public function country()
	{
		return $this->belongsTo('App\Country');
	}
}
