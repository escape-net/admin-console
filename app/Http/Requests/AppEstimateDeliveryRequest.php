<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AppEstimateDeliveryRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [

            'vehicle_type_id'       => 'required',
            'pickup_latitude'       => 'required',
            'pickup_longitude'      => 'required',
            'delivery_latitude'     => 'required',
            'delivery_longitude'    => 'required'

        ];
    }
}
