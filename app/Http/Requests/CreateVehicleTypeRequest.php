<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class CreateVehicleTypeRequest extends FormRequest
{

    public function authorize()
    {
        return Auth::check() && Auth::user()->role == 'admin';
    }

    public function rules()
    {
        return [
            
            'name'                      => 'required',
            'base_fare'                 => 'required|numeric',
            'per_minute'                => 'required|numeric',
            'per_km'                    => 'required|numeric',
            'commission_percentage'     => 'required|numeric',
            'cancellation_fee'          => 'required|numeric',
            // 'night_start_time'          => 'required|datetime',
            // 'night_end_time'            => 'required|datetime',
            // 'night_surcharge'           => 'required|numeric',
        ];
    }
}
