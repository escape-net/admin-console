<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateDeliveryRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

 
    public function rules()
    {
        return [
            
            'user_id'           => 'required',
            'item'              => 'required',
            'vehicle_type_id'   => 'required',
            'pickup_place_id'   => 'required',
            'delivery_place_id' => 'required',
            'recipient_name'    => 'required',
            'recipient_phone'   => 'required',
            'instructions'      => ''
        ];
    }
}
