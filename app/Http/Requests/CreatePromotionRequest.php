<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class CreatePromotionRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::check() && Auth::user()->role == 'admin';
    }


    public function rules()
    {
        return [
            
            'code'                  => 'required',
            'users'                 => 'required',
            'type'                  => 'required',
            'amount'                => 'required',
            'validity'              => 'required',
            'limit'                 => 'required',
            'description'           => 'required',
            'status'                => 'required'
        ];
    }
}
