<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AppRegisterRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            
            'phone'         => 'required|min:11',
            'first_name'    => 'required',
            'last_name'     => 'required',
            'email'         => 'required|email|unique:users,email,'.request('id'),
            'phone'         => 'required|min:11|unique:users,phone,'.request('id'),
        ];
    }
}
