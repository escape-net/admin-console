<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
class CreateCompanyRequest extends FormRequest
{
    
    public function authorize()
    {
        return Auth::check() && Auth::user()->role == 'admin';
    }


    public function rules()
    {
        return [
            
            'name'          => 'required',
            'email'         => 'required|email|unique:companies,email,'.request('id'),
            'phone'         => 'required|unique:companies,phone,'.request('id'),
            'address'       => 'required',
            'status'        => 'required'
        ];
    }
}
