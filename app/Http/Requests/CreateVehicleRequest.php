<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class CreateVehicleRequest extends FormRequest
{

    public function authorize()
    {
        return Auth::check() && Auth::user()->role == 'admin';
    }

  
    public function rules()
    {
        return [
            
            'company_id'            => 'required',
            'vehicle_type_id'       => 'required',
            'make'                  => 'required',
            'model'                 => 'required',
            'year'                  => 'required',
            'color'                 => 'required',
            'plate_number'          => 'required',
            'status'                => 'required'
        ];
    }
}
