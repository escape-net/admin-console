<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class CreateAdminRequest extends FormRequest
{

    public function authorize()
    {
        return Auth::check() && Auth::user()->role == 'admin';
    }

  
    public function rules()
    {
        return [
            
            'first_name'    => 'required',
            'last_name'     => 'required',
            'email'         => 'required|email|unique:users,email,'.request('id'),
            'phone'         => 'required|unique:users,phone,'.request('id'),
            'password'      => 'confirmed'
        ];
    }
}
