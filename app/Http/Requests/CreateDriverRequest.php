<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class CreateDriverRequest extends FormRequest
{

    public function authorize()
    {
        return Auth::check() && Auth::user()->role == 'admin';
    }


    public function rules()
    {
        return [
            
            'company_id'            => 'required',
            'first_name'            => 'required',
            'last_name'             => 'required',
            'email'                 => 'required|email|unique:users,email,'.request('id'),
            'phone'                 => 'required|unique:users,phone,'.request('id'),
            'address'               => 'string',
            'status'                => 'required',
            'bank_id'               => '',
            'bank_account_number'   => 'numeric',
            'bank_account_name'     => ''
        ];
    }
}
