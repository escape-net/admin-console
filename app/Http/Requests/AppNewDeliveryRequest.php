<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AppNewDeliveryRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [

            'delivery'              => 'required',
            'delivery.address'      => 'required',
            'delivery.latitude'     => 'required',
            'delivery.longitude'    => 'required',
            'delivery.place_id'     => 'required',
            'pickup'                => 'required',
            'pickup.address'        => 'required',
            'pickup.latitude'       => 'required',
            'pickup.longitude'      => 'required',
            'pickup.place_id'       => 'required',
            'item'                  => 'required',
            'payment'               => 'required',
            'recipient_name'        => 'required',
            'recipient_phone'       => 'required',
            'vehicle_type_id'       => 'required',

        ];
    }
}
