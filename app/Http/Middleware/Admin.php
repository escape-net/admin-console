<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Admin
{

    public function handle($request, Closure $next)
    {
        if(optional(Auth::user())->role != 'admin'){

            Auth::logout();
            return redirect('login'); 
        }

        return $next($request);
    }
}
