<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Delivery;
use App\VehicleType;
use App\Place;
use App\User;
use App\FirebaseMessaging;
use App\Http\Requests\AppEstimateDeliveryRequest;
use App\Http\Requests\AppNewDeliveryRequest;
use Auth;

class DeliveriesController extends Controller
{

    public function index()
    {
        $user   = Auth::user();

        $deliveries = Delivery::getApiDeliveries('user_id', $user->id);

        $completed = $active = [];

        foreach($deliveries as $row){

            $row['total']           = _c($row['total']);
            $row['commission']      = _c($row['commission']);

            if($row['status'] == 'completed')
                $completed[] = $row;

            if($row['status'] == 'active')
                $active[] = $row;

        }

        _api_log('Pulled user Deliveries');

        return response()->json([

            'status'  => true,
            'data'        => [ 'active'  => $active, 'completed'  => $completed]
        ]);
    }


    public function estimate(AppEstimateDeliveryRequest $request)
    {
        $delivery = new Delivery();

        $type = VehicleType::find(request('vehicle_type_id'));
        if(!$type) return response()->json(['status' => false, 'data' => 'Vehicle Type not found']);

        $pickup_place = new Place();
        $pickup_place->latitude = request('pickup_latitude');
        $pickup_place->latitude = request('pickup_longitude');

        $delivery_place = new Place();
        $delivery_place->latitude = request('delivery_latitude');
        $delivery_place->latitude = request('delivery_longitude');

        $distance       = Delivery::getDistanceDifferenceInKm($pickup_place, $delivery_place);
        $estimated_time = floor($distance / 15.5);

        $estimated_total = $type->base_fare + ($type->per_minute & $estimated_time) + ($type->per_km * $distance);

        _api_log('Calculated Estimate');

        return response(['status' => true, 'data' => _c($estimated_total)]);
    }

    public function newDelivery(AppNewDeliveryRequest $request)
    {
        $delivery_place = Place::updateOrCreate(['google_place_id' => request('delivery.place_id')], [

            'name'              => request('delivery.address'),
            'address'           => request('delivery.address'),
            'google_place_id'   => request('delivery.place_id'),
            'latitude'          => request('delivery.latitude'),
            'longitude'         => request('delivery.longitude')
        ]);

        $pickup_place = Place::updateOrCreate(['google_place_id' => request('pickup.place_id')], [

            'name'              => request('pickup.address'),
            'address'           => request('pickup.address'),
            'google_place_id'   => request('pickup.place_id'),
            'latitude'          => request('pickup.latitude'),
            'longitude'         => request('pickup.longitude')
        ]);

        $delivery = Delivery::create([

            'user_id'               => Auth::user()->id,
            'reference'             => _reference(),
            'item'                  => request('item'),
            'vehicle_type_id'       => request('vehicle_type_id'),
            'pickup_place_id'       => $pickup_place->id,
            'delivery_place_id'     => $delivery_place->id,
            'recipient_name'        => request('recipient_name'),
            'recipient_phone'       => request('recipient_phone'),
            'instructions'          => request('instructions'),
            'device'                => request('device'),
            'ip_address'            => request()->ip()
        ]);

        Delivery::saveEstimate($delivery);

        $tracking_code = _tracking_code($delivery);
        $delivery->update(['tracking_code' => $tracking_code]);

        _log('New Delivery '.$delivery->reference, null, $delivery->id);
        _api_log('New Delivery');
        _log('Set Delivery Tracking Code - '.$delivery->tracking_code, null, $delivery->id);

        return response()->json(['status' => true, 'data' => Delivery::getApiDelivery($delivery->id)]);
    }

    public function assignDriver(Request $request)
    {
        $this->validate($request, [

            'latitude'      => 'required',
            'longitude'     => 'required',
            'delivery_id'   => 'required'
        ]);

        $search = Place::closestUserToLocation(request('latitude'), request('longitude'), request('exclude_driver_id'));

        $delivery = Delivery::find(request('delivery_id'));

        if(!$delivery)
            return response()->json(['status' => false, 'data' => 'Invalid Delivery. Please try again.']);

        if(!$search){

            $drivers    = User::drivers()->online()->get();
            $driver     = null;

            foreach($drivers as $row){

                if($row->vehicle->id == null)
                    continue;

                //if($row->vehicle->vehicle_type_id == $delivery->vehicle_type_id){

                $driver = $row;
                break;
                //}

            }

            //$driver = User::find(13);

            if(!$driver)
                return response()->json(['status' => false, 'data' => 'No Nearby Drivers Avaliable']);

        }else{

            $driver = User::find($search->id);
        }


        $delivery->update(['driver_id' => $driver->id, 'status' => 'driver_assigned']);
        $driver->update(['driver_status' => 'assigned']);

        _log('Assigned Driver to Delivery - '.$delivery->reference, null, $delivery->id);
        _api_log('Assign Driver');

        FirebaseMessaging::notifyDriverOfDeliveryAssignment($driver, $delivery);
        
        return response()->json(['status' => true, 'data' => User::getApiUser($driver)]);   

    }

    public function places()
    {
        $user_id = Auth::user()->id;

        $deliveries = Delivery::where(['user_id' => $user_id])->completed()->orderBy('updated_at', 'desc')->take(10)->get();

        $data = [];

        foreach($deliveries as $row){

            $pickup_place   = $row->pickup_place;
            $delivery_place = $row->delivery_place;

            $data[] = [

                'id'                => $pickup_place->id, 
                'type'              => 'pickup',
                'place_id'          => $pickup_place->google_place_id,
                'description'       => $pickup_place->name, 
                'formatted_address' => $pickup_place->address,
                'geometry'          => [

                    'location' => ['lat' => $pickup_place->latitude, 'lng' => $pickup_place->longitude ]
                ]
            ];

            $data[] = [

                'id'                => $delivery_place->id, 
                'type'              => 'delivery',
                'place_id'          => $delivery_place->google_place_id,
                'description'       => $delivery_place->name, 
                'formatted_address' => $delivery_place->address,
                'geometry'      => [

                    'location' => ['lat' => $delivery_place->latitude, 'lng' => $delivery_place->longitude ]
                ]
            ];
        }

        $data = array_map('unserialize', array_unique(array_map('serialize', $data)));

        $response = [];
        foreach($data as $row)
            $response[] = $row;

        return response()->json(['status' => true, 'data' => $response]);
    }
}
