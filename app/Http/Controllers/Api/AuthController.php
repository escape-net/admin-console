<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AppRegisterRequest;
use App\User;
use Auth;
use App\Vehicle;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $this->validate($request, ['phone' => 'required']);

        $user = User::where(['phone' => request('phone')])->with(['vehicle'])->first();

        if(!$user) 
            return response()->json(['status' => false, 'data' => 'Your Phone Number is invalid.']);
        
        $user->otp = rand(1111, 9999);
        $user->api_token = str_random(16);
        $user->save();

        Auth::loginUsingId($user->id);
        _log('New Sign In', $user);

        _sms($user->phone, 'OTP: '.$user->otp);

        return response()->json(['status' => true, 'data' => User::getApiUser($user)]);
    }

    public function register(AppRegisterRequest $request)
    {
    	$user = User::create([

    		'company_id'	=> 1,
    		'first_name'	=> request('first_name'),
    		'last_name'		=> request('last_name'),
    		'email'			=> request('email'),
    		'phone'			=> request('phone'),
    		'role'			=> request('role', 'user'),
    		'api_token'		=> str_random(16),
    		'device'		=> request('device'),
    		'otp'			=> rand(1111, 9999)
    	]);

    	Auth::loginUsingId($user->id);
    	
    	_log('New '.ucwords($user->role).' Registration', $user);

        $category = null;

        if(request('category') && $user->role == 'driver'){

            switch(request('category')){

                case 1 : $category = 'driving_only'; break;
                case 2 : $category = 'vehicle_owner_and_driving'; break;
                case 3 : $category = 'vehicle_owner_not_driving'; break;
            }

            if($category != null){

                if($category == 'vehicle_owner_not_driving'){

                    $user->update(['role' => 'partner']);
                }

                $user->update(['driver_category' => $category]);
            }
        }

        if(request('vehicle_type_id')){

            Vehicle::create([

                'user_id'           => $user->id, 
                'company_id'        => $user->company_id,
                'vehicle_type_id'   => request('vehicle_type_id')
            ]);
        }

        _sms($user->phone, 'OTP: '.$user->otp);
    	return response()->json(['status' => true, 'data' => User::getApiUser($user)]);
    }

    public function logout()
    {
        _api_log('Device Token Updated after Logout');
        
        Auth::user()->update(['device_token' => null]);

        return response()->json(['status' => true, 'data' => 'Logout Successful']);
    }

    public function updateDeviceToken(Request $request)
    {
        if(!request('token')) 
            return response()->json(['status' => false, 'data' => 'No token sent.']);

        Auth::user()->update(['device_token' => request('token')]);

        _api_log('Device Token Updated');

        return response()->json(['status' => true, 'data' => User::getApiUser(Auth::user()) ]);
    }
}
