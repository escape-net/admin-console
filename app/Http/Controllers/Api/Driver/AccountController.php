<?php

namespace App\Http\Controllers\Api\Driver;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use App\Delivery;
use App\Bank;
use App\Vehicle;

class AccountController extends Controller
{
    public function bankDetails(Request $request)
    {
        $user = Auth::user();
        $bank = Bank::where(['name' => request('bank_name', null)])->first();

        if($bank) $user->update(['bank_id' => $bank->id]);
        if(request('bank_id')) $user->update(['bank_id' => request('bank_id')]);
        if(request('bank_account_number')) $user->update(['bank_account_number' => request('bank_account_number')]);
        if(request('bank_account_number')) $user->update(['bank_account_number' => request('bank_account_name')]);

        $user = User::getApiUser($user);

        _api_log('Updated Bank Details', $user);

        return response()->json(['status' => true, 'data' => $user]);
    }

    public function status(Request $request, $status = '')
    {
    	$driver = Auth::user();
    	$status = strtolower($status);

    	if(!in_array($status, ['offline', 'online']))
    		return response()->json(['status' => false, 'data' => 'Status could not be updated']);

    	if($driver->driver_status == 'assigned'){

            $delivery = Delivery::where(['driver_id' => $driver->id])->driverAssigned()->first();

            if($delivery){

                FirebaseMessaging::notifyDriverOfDeliveryAssignment($driver, $delivery);
            }

    		return response()->json(['status' => false, 'data' => 'You are currently assigned to a delivery. Check your Incoming Deliveries to cancel.']);
        }

    	$driver->driver_status = $status;
    	$driver->save();

    	_log('Status updated to '.$status, $driver);
        _api_log('Status updated', $status);

    	return response()->json(['status' => true, 'data' => 'You are now '.$status]);
    }

    public function upload(Request $request, $field = '')
    {
        if(!request('image')) return response()->json(['status' => false, 'data' => 'Upload Failed. No Image sent.']);

        $response = _cloudinary(request('image'), '', true);

        if($response->link == null){

            return response()->json(['status' => false, 'data' => $response->error]);

        }else{

            $user = Auth::user();
            $user->update([$field => $response->link]);

            return response()->json(['status' => true, 'data' => User::getApiUser($user)]);
        }
    }

    public function uploadVehicle(Request $request, $field = '')
    {
        if(!request('image')) return response()->json(['status' => false, 'data' => 'Upload Failed. No Image sent.']);

        $response = _cloudinary(request('image'), '', true);

        if($response->link == null){

            return response()->json(['status' => false, 'data' => $response->error]);

        }else{

            $user       = Auth::user();
            $vehicle    = Vehicle::where(['user_id' => $user->id])->first();

            if(!$vehicle)
                $vehicle = Vehicle::create(['user_id' => $user->id, 'company_id' => $user->company_id, 'vehicle_type_id' => 1]);

            $vehicle->update([$field => $response->link]);

            return response()->json(['status' => true, 'data' => User::getApiUser($user)]);
        }
    }

    public function editProfile(Request $request)
    {
        $user = Auth::user();

        $phone = request('phone');
        $email = request('email');

        if($phone && $user->phone != $phone){

            if(User::where(['phone' => $phone])->count() > 0)
                return response()->json(['status' => false, 'data' => 'This phone number is being used by another user.']);

            $user->update(['phone' => $phone]);
        }

        if($email && $user->email != $email){

            if(User::where(['email' => $email])->count() > 0)
                return response()->json(['status' => false, 'data' => 'This email is being used by another user.']);

            $user->update(['email' => $email]);
        }

        $user->update([

            'first_name'    => request('first_name') ?? $user->first_name, 
            'last_name'     => request('last_name') ?? $user->last_name
        ]);

        $user = User::getApiUser($user);

        _api_log('Edit Profile', $user);

        return response()->json(['status' => true, 'data' => $user]);
    }

    public function editVehicle(Request $request)
    {
        $user       = Auth::user();
        $vehicle    = Vehicle::where(['user_id' => $user->id])->first();

        if(!$vehicle)
            return response()->json(['status' => false, 'data' => 'An error occured. Pleas try again.']);

        $vehicle->update([

            'make'          => request('make', $vehicle->make),
            'year'          => request('year', $vehicle->year),
            'model'         => request('model', $vehicle->model),
            'color'         => request('color', $vehicle->color),
            'plate_number'  => request('plate_number', $vehicle->plate_number),
        ]);

        _sms($user->phone, 'OTP: '.$user->otp);

        $user = User::getApiUser($user);

        _api_log('Edit Vehichle', $user);

        return response()->json(['status' => true, 'data' => $user]);
    }

}
