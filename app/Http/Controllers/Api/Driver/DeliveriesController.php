<?php

namespace App\Http\Controllers\Api\Driver;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Delivery;
use Auth;
use App\FirebaseMessaging;
use \Carbon\Carbon;
use App\Promotion;
use App\User;
use App\Broadcast;

class DeliveriesController extends Controller
{
  public function index()
  {

    $user               = Auth::user();
    $assignedDelivery   = Delivery::by($user->id)->driverAssigned()->first();

    if($assignedDelivery){

      FirebaseMessaging::notifyDriverOfDeliveryAssignment($user, $assignedDelivery);
    }

    $deliveries = Delivery::getApiDeliveries('driver_id', $user->id);

    $completed = $assigned = $active = [];

    foreach($deliveries as $row){

      $row['total']           = _c($row['total']);
      $row['commission']      = _c($row['commission']);

      if($row['status'] == 'completed')
        $completed[] = $row;

      if($row['status'] == 'driver_assigned')
        $assigned[] = $row;

      if($row['status'] == 'active')
        $active[] = $row;

    }

    _api_log('Pulled User Deliveries');

    return response()->json([

      'status'  => true,
      'data'        => [ 'assigned' => $assigned, 'active' => $active, 'completed' => $completed]
      
    ]);
  }

  public function delivery($id = 0)
  {

    $delivery = Delivery::with(['user', 'pickup_place', 'delivery_place', 'driver'])->where('id', $id)->first();

    if(!$delivery) 
      return response()->json(['status' => false, 'data' => 'Delivery not found.']);

    $delivery->driver = User::getApiUser($delivery->driver);;

    _api_log('Get Single Delivery '.$id, $delivery);

    return response()->json(['status' => true, 'data' => $delivery]);
  }

  public function startDelivery($id = 0)
  {
    $delivery = Delivery::where(['id' => $id, 'driver_id' => Auth::user()->id, 'status' => 'driver_assigned'])->first();

    if(!$delivery) 
      return response()->json(['status' => false, 'data' => 'Delivery Not Found.']);

    $delivery->update(['status' => 'active', 'start_at' => date('Y-m-d H:i:s')]);

    $driver = Auth::user();
    _log('Started Delivery '.$delivery->reference, null, $delivery->id);

    FirebaseMessaging::notifyUserThatDeliveryHasStarted($delivery->user, $delivery);

    _api_log('Start Delivery '.$id);

    return response()->json(['status' => true, 'data' => 'Delivery has been Started!']);
  }

  public function completeDelivery($id = 0)
  {
    $delivery = Delivery::where(['id' => $id, 'driver_id' => Auth::user()->id, 'status' => 'active'])->first();

    if(!$delivery) 
      return response()->json(['status' => false, 'data' => 'Delivery Not Found.']);

    $end_at             = date('Y-m-d H:i:s');
    $total_km           = $delivery->approx_km;
    $total_minutes      = (Carbon::parse($delivery->start_at))->diffInMinutes(Carbon::parse($end_at));

    $discount           = 0;
    $total              = $delivery->base_fare + ($delivery->per_minute * $total_minutes) + ($delivery->per_km * $total_km);
    $commission         = $commission = ($delivery->vehicle_type->commission_percentage / 100) * $total;

    if($delivery->promotion_id > 0){

      $discount = Promotion::getPromotionTotal($delivery->promotion_id, $total);
    }


    $delivery->update([

      'end_at'            => $end_at,
      'total_minutes'     => $total_minutes,
      'total_km'          => $delivery->approx_km,
      'discount'          => $discount,
      'subtotal'          => $total,
      'total'             => $total - $discount,
      'commission'        => $commission,
      'status'            => 'completed'
    ]);

    _log('Completed Delivery '.$delivery->reference, null, $delivery->id);
    _api_log('Completed Delivery '.$id);

    FirebaseMessaging::notifyUserThatDeliveryIsComplete($delivery->user, $delivery);

    return response()->json(['status' => true, 'data' => 'Delivery has been completed Succesfully']);

  }

  public function decline($id = 0)
  {
    $delivery   = Delivery::find($id);
    $driver     = Auth::user();

    if(!$delivery)
      return response()->json(['status' => false, 'data' => 'Delivery Not found']);

    if($delivery->status != 'driver_assigned')
      return response()->json(['status' => false, 'data' => 'Delivery is not currently assigned']);

    if($delivery->driver_id != $driver->id)
      return response()->json(['status' => false, 'data' => 'This delivery is not assigned to you.']);

    $delivery->update(['driver_id' => null, 'status' => 'pending']);
    $driver->update(['driver_status' => 'online']);

    _log('Declined Delivery '.$delivery->reference, null, $delivery->id);
    _api_log('Declined delivery '.$id);

    Broadcast::create([

      'user_id'   => $delivery->user_id,
      'type'      => 'notification',
      'subject'     => 'Delivery Declined',
      'body'      => $driver->name.' has declined your delivery of '.$delivery->item

    ]);

    FirebaseMessaging::notifyUserThatDeliveryWasDeclined($delivery->user, $delivery);

    return response()->json(['status' => true, 'data' => 'Delivery has been declined.']);
  }
}
