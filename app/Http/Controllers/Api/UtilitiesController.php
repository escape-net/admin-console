<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\VehicleType;
use App\Bank;
use App\Place;
use Auth;

class UtilitiesController extends Controller
{
    public function vehicleTypes()
    {
        $types = VehicleType::orderBy('mobile_order')->get();

        $types = $types->map(function($row){

            return collect($row)->only(['id', 'name', 'mobile_icon'])->toArray();
        });

        _api_log('Pulled Vehicle Types');

        return response()->json(['status' => true, 'data' => $types]);
    }

    public function banks()
    {
        return response()->json(['status' => true, 'data' => Bank::get()]);
    }

    public function updateLocation($latitude = 0, $longitude = 0)
    {
        if($latitude == 0 || $longitude == 0)
            return response()->json(['status' => false, 'data' => 'Latitude or Longitude is invalid']);

        Auth::user()->update(['latitude' => $latitude, 'longitude' => $longitude]);

        _api_log('Updated Location Latitude '.$latitude.' Longitude '.$longitude);

        return response()->json(['status' => true, 'data' => 'Update successful.']);
    }
}
