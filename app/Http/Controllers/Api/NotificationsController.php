<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Broadcast;
use Auth;

class NotificationsController extends Controller
{
    public function index()
    {

    	$broadcasts = Broadcast::where(['user_id' => Auth::user()->id, 'type' => 'notification'])->orderBy('created_at', 'desc')->take(50)->get();

    	$notifications = $broadcasts->map(function($row){

    		return collect($row)->only(['id', 'body', 'subject', 'created_at'])->toArray();
    	});

    	_api_log('Pulled '.$notifications->count().' Notifications');
    	
    	return response()->json(['status' => true, 'data' => $notifications]);
    }
}
