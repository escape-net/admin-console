<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transaction;

class TransactionsController extends Controller
{
    public function index()
    {
    	$data['transactions']	= Transaction::orderBy('id', 'desc')->get();

    	return view('admin.transactions.index', $data);
    }
}
