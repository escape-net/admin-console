<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateDeliveryRequest;
use App\Delivery;
use App\User;
use App\VehicleType;
use App\Transaction;
use \Carbon\Carbon;
use App\Promotion;
use Auth;
use App\FirebaseMessaging;

class DeliveriesController extends Controller
{
	public function index($id = 0)
	{		
		$online 	= User::drivers()->online()->count();
		$ontrip		= User::drivers()->ontrip()->count();

		$data['drivers']			= User::drivers()->online()->get();
		$data['drivers_status']		= 'There are '.$online.' Drivers Currently Online and '.$ontrip.' currently on Trip.';
		$data['delivery_search']	= true;

		return view('admin.deliveries.index', $data);
	}

	public function form(Request $request, $id = 0)
	{
		$data['delivery'] 		= Delivery::firstOrNew(['id' => $id]);
		$data['users']	 		= User::users()->get();
		$data['vehicle_types']	= VehicleType::get();

		return view('admin.deliveries.form', $data); 
	}

	public function save(CreateDeliveryRequest $request)
	{
		$delivery = Delivery::updateOrCreate(['id' => request('id')],[

			'user_id'    			=> request('user_id'),
			'item'					=> request('item'),
			'vehicle_type_id'     	=> request('vehicle_type_id'),
			'pickup_place_id'    	=> request('pickup_place_id'),
			'delivery_place_id'     => request('delivery_place_id'),
			'recipient_name'        => request('recipient_name'),
			'recipient_phone'       => request('recipient_phone'),
			'instructions'        	=> request('instructions')
		]);

		_log('Updated Delivery Information - '.$delivery->item, null, $delivery->id);

		if(request('schedule_at')){

			$delivery->update(['schedule_at' => request('schedule_at')]);
		}
		
		if(!request('id')){

			$delivery->update(['reference' => _reference()]);
			_log('Set Delivery Reference - '.$delivery->reference, null, $delivery->id);

			$tracking_code = _tracking_code($delivery);
			$delivery->update(['tracking_code' => $tracking_code]);

			_log('Set Delivery Tracking Code - '.$delivery->tracking_code, null, $delivery->id);
		}

		Delivery::saveEstimate($delivery);

		return redirect('admin/deliveries?id='.$delivery->id)->with('message', 'Delivery Saved Succesfully. Continue to Assign a Driver.');
	}

	public function deliveries()
	{
		return response()->json([ 'status' => true, 'data' => Delivery::getAll() ]);
	}

	public function delivery($id = 0)
	{
		$delivery = Delivery::getSingle($id);

		if(!$delivery) 
			return response()->json(['status' => false, 'data' => 'Could not find the delivery specified.']);

		return response()->json(['status' => true, 'data' => $delivery ]);
	}

	public function searchDrivers($vehicle_type_id = 0)
	{
		$drivers = User::searchDrivers(request('param'), $vehicle_type_id);

		$data = [];

		foreach($drivers as $row){

			$lastDelivery 	= Delivery::completed()->by($row->id)->latest()->first();
			$deliveries 	= Delivery::completed()->by($row->id)->count();

			$data[]	= [

				'id'					=> $row->id,
				'name'					=> $row->name,
				'date'					=> _d($row->created_at),
				'phone'					=> $row->phone,
				'email'					=> $row->email,
				'vehicle_type'			=> ucwords($row->vehicle->vehicle_type->name),	
				'delivery_comment'		=> $lastDelivery ? 'Last Deliverered '.$lastDelivery->item.' for '.$lastDelivery->user->name.' on '._d($lastDelivery->end_at) : '',
				'deliveries'			=> 'Completed '.$deliveries.' '.str_plural('Delivery', $deliveries)
			];
		}

		return response()->json(['status' => true, 'data' => $data]);
	}

	public function assignDriver(Request $request, $driver_id = 0)
	{
		$delivery = Delivery::pending()->where(['id' => request('id')])->first();
		if(!$delivery) return response()->json(['status' => false, 'data' => 'Delivery Could not be found.']);

		if($driver_id == 0){

			$driver = User::drivers()->online()->whereHas('vehicle', function($query) use($delivery){

				$query->where('vehicle_type_id', $delivery->vehicle_type_id);

			})->find(request('driver_id'));

		}else{

			$driver = User::find($driver_id);
		}
		

		if(!$driver) return response()->json(['status' => false, 'data' => 'Driver not found. Please Choose another Driver.']);
		
		$delivery->update(['driver_id' => $driver->id, 'status' => 'driver_assigned']);
		$driver->update(['driver_status' => 'assigned']);

		FirebaseMessaging::notifyDriverOfDeliveryAssignment($driver, $delivery);

		_log('Assigned Driver to Delivery - '.$delivery->reference, null, $delivery->id);
		return response()->json(['status' => true, 'data' => $driver->name.' has been assigned to this Delivery.']);
	}

	public function autoAssignDriver(Request $request)
	{
		$delivery = Delivery::pending()->where(['id' => request('id')])->first();
		if(!$delivery) return response()->json(['status' => false, 'data' => 'Delivery Could not be found.']);

		$driver = User::drivers()->online()->orderBy('id', 'random')->first();

		$driver = User::drivers()->online()->whereHas('vehicle', function($query) use($delivery){

			$query->where('vehicle_type_id', $delivery->vehicle_type_id);

		})->orderBy('id', 'random')->first();

		if(!$driver) return response()->json(['status' => false, 'data' => 'Does not look like we can do the Job for you. Please select a driver yourself. :)']);
		
		$delivery->update(['driver_id' => $driver->id, 'status' => 'driver_assigned']);
		$driver->update(['driver_status' => 'assigned']);

		_log('Auto Assigned Driver to Delivery - '.$delivery->reference, null, $delivery->id);
		return response()->json(['status' => true, 'data' => $driver->name.' has been assigned to this Delivery.']);
	}

	public function cashPayment(Request $request)
	{
		$delivery = Delivery::where(['id' => request('id'), 'transaction_id' => null])->first();
		if(!$delivery) return response()->json(['status' => false, 'data' => 'Delivery already has payment.']);
		
		if($delivery->subtotal < 1)
			return response()->json(['status' => false, 'data' => 'Delivery Total is Zero. You cannot add Payment yet.']);

		$transaction = Transaction::addCashPayment($delivery);
		$delivery->update(['transaction_id' => $transaction->id]);

		_log('Added Cash Payment to Delivery - '.$delivery->reference, null, $delivery->id);
		return response()->json(['status' => true, 'data' => 'Transaction '.$transaction->transaction_reference.' has been added to this Delivery.']);
	}

	public function invoice($id = 0)
	{
		$delivery = Delivery::completed()->where(['id' => $id])->first();

		if(!$delivery)
			return redirect('admin/deliveries')->with('error', 'Delivery Not found');

		$data['delivery'] = $delivery;

		return view('admin.deliveries.invoice', $data);
	}

	public function addPromotionCode($delivery_id = 0, $promotion_code = '')
	{
		$delivery = Delivery::find($delivery_id);

		if(!$delivery)
			return response()->json(['status' => false, 'data' => 'Invalid Delivery.']);

		if($delivery->status == 'completed')
			return response()->json(['status' => false, 'data' => 'You cannot add a promo code to a completed Delivery.']);

		if($delivery->transaction_id > 0)
			return response()->json(['status' => false, 'data' => 'Payment has already being applied to this Delivery. You cannot add a Promotion Code.']);
		
		if($delivery->promotion_id > 0)
			return response()->json(['status' => false, 'data' => 'This Delivery already has a Promotion Code applied. You can only apply one Promotion code Per Delivery']);


		$promotion = Promotion::where(['code' => $promotion_code, 'status' => 'active'])->first();

		if(!$promotion)
			return response()->json(['status' => false, 'data' => 'Invalid Promotion Code']);

		$userDeliveryWithThisPromotionCode = Delivery::where(['user_id' => $delivery->user_id, 'promotion_id' => $promotion->id])->first();

		if($userDeliveryWithThisPromotionCode)
			return response()->json(['status' => false, 'data' => 'This User already applied this promotion code to delivery #'.$userDeliveryWithThisPromotionCode->reference]);

		if($promotion->validity == 'date'){

			$today = date('d-m-Y');

			if($today >= $promotion->start_at)
				return response()->json(['status' => false, 'data' => 'Promotion Period has not began yet.']);

			if($today <= $promotion->end_at)
				return response()->json(['status' => false, 'data' => 'Promotion Period has ended']);

			$users = json_decode($promotion->users);

			if(!in_array(0, $users)){

				if(!in_array($delivery->user_id, $users))
					return response()->json(['status' => false, 'data' => 'Promotion is not avaliable for '.$delivery->user->name]);
			}
		}

		$delivery->update(['promotion_id' => $promotion->id]);

		_log('Added Promo Code '.$promotion->code.' to Delivery - '.$delivery->reference, null, $delivery->id);
		return response()->json(['status' => true, 'data' => $promotion->code.' Has been added to this Delivery Succesfully']);
	}

	public function calculateDelivery(Request $request)
	{
		$delivery = Delivery::find(request('id'));

		if(!$delivery)
			return response()->json(['status' => false, 'data' => 'Delivery Not Found']);

		$totalAndDiscount = Delivery::getTotalAndDiscount($delivery->id, request('total_km'), request('total_minutes'));

		$totalAndDiscount['discount_currency'] = _c($totalAndDiscount['discount']);

		return response()->json(['status' => true, 'data' => $totalAndDiscount]);
	}

	public function completeDelivery(Request $request)
	{
		$delivery = Delivery::find(request('delivery_id'));

		if(!$delivery)
			return response()->json(['status' => false, 'data' => 'Invalid Delivery.']);

		if($delivery->status == 'completed')
			return response()->json(['status' => false, 'data' => 'This Delivery has already been completed.']);

		$subtotal = request('total');
		$discount = request('discount');

		if($subtotal < 1)
			return response()->json(['status' => false, 'data' => 'You cannot complete a delivery with Zero Total.']);
		
		$commission = ($delivery->vehicle_type->commission_percentage / 100) * $subtotal;

		$delivery->update([

			'start_at'			=> date('Y-m-d H:i:s'),
			'end_at'			=> date('Y-m-d H:i:s'),
			'total_minutes'		=> request('total_minutes'),
			'total_km'			=> request('total_km'),
			'discount'			=> $discount,
			'subtotal'			=> (float)$subtotal,
			'total'				=> (float)$subtotal + (float)$discount,
			'commission'		=> (float)$commission,
			'status'			=> 'completed'
		]);

		$delivery->refresh();

		if(request('add_payment') == true && $delivery->transaction_id < 1){

			$transaction = Transaction::addCashPayment($delivery);
			$delivery->update(['transaction_id' => $transaction->id]);
		}

		_log('Completed Delivery '.$delivery->reference.' Manually', null, $delivery->id);
		return response()->json(['status' => true, 'data' => 'Delivery has been completed Succesfully']);
	}

	public function search()
	{
		$query = request('query');

		if($query[0] == '#')
			$query = substr($query, 1);

		$data['deliveries'] = Delivery::search($query);
		$data['query']		= request('query');

		if(count($data['deliveries']) == 1){

			return redirect('admin/deliveries?id='.$data['deliveries'][0]->id);
		}

		return view('admin.deliveries.search', $data);
	}
}
