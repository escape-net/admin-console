<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateAdminRequest;
use App\User;

class AdminsController extends Controller
{
	public function index()
	{
		$data['users']	= User::admins()->get();

		return view('admin.administrators.index', $data);
	}

	public function form(Request $request, $id = 0)
	{
		$data['user'] = User::firstOrNew(['id' => $id]);

		return view('admin.administrators.form', $data); 
	}

	public function save(CreateAdminRequest $request)
	{
		$user = User::updateOrCreate(['id' => request('id')],[

			'first_name'    => request('first_name'),
			'last_name'     => request('last_name'),
			'email'         => request('email'),
			'phone'         => request('phone'),
			'role'          => 'admin',
			'status'        => 1,
			'access_level'	=> request('access_level'),
			'password'      => bcrypt(request('password'))
		]);

		 _log('Updated User Information', $user);

		return redirect('admin/administrators')->with('message', 'Administrator Saved Succesfully.');
	}
}
