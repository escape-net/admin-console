<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Vehicle;
use App\Company;
use App\VehicleType;
use App\User;
use App\Delivery;
use App\Http\Requests\CreateVehicleRequest;

class VehiclesController extends Controller
{
	public function index()
	{
		$data['vehicles']	= Vehicle::get();

		return view('admin.vehicles.index', $data);
	}

	public function form(Request $request, $id = 0)
	{
		$data['vehicle'] 		= Vehicle::firstOrNew(['id' => $id]);
		$data['companies']		= Company::active()->get();
		$data['vehicle_types']	= VehicleType::get();
		$data['drivers']		= User::drivers()->active()->get();
		
		return view('admin.vehicles.form', $data); 
	}

	public function save(CreateVehicleRequest $request)
	{		
		$vehicle = Vehicle::updateOrCreate(['id' => request('id')],[

			'user_id'			=> request('user_id'),
			'company_id'    	=> request('company_id'),
			'vehicle_type_id'	=> request('vehicle_type_id'),
			'make'     			=> request('make'),
			'model'    			=> request('model'),
			'year'    			=> request('year'),
			'color'    			=> request('color'),
			'plate_number'    	=> request('plate_number'),
			'status'    		=> request('status')
		]);

		_log('Updated Vehicle Information - '.$vehicle->plate_number);

		if(request('registration')){

			$response = _cloudinary($request->file('registration'));

			if($response->link != null){

				$vehicle->update(['registration' => $response->link]);
				_log('Uploaded Vehicle Registration Document - '.$vehicle->plate_number);
			}
		}

		if(request('insurance')){

			$response = _cloudinary($request->file('insurance'));

			if($response->link != null){

				$vehicle->update(['insurance' => $response->link]);
				_log('Uploaded Vehicle Insurance Document - '.$vehicle->plate_number);
			}
		}

		if(request('certificate')){

			$response = _cloudinary($request->file('certificate'));

			if($response->link != null){

				$vehicle->update(['certificate' => $response->link]);
				_log('Uploaded Vehicle Certificate Document - '.$vehicle->plate_number);
			}
		}

		if(request('image')){

			$response = _cloudinary($request->file('image'));

			if($response->link != null){

				$vehicle->update(['image' => $response->link]);
				_log('Uploaded Vehicle Image - '.$vehicle->plate_number);
			}
		}

		return redirect('admin/vehicles')->with('message', 'Vehicle Saved Succesfully.');
	}

	public function vehicle($id = 0)
	{
		$vehicle = Vehicle::find($id);

		if(!$vehicle)
			return redirect('admin/vehicles')->with('error', 'Vehicle not found.');

		$data['vehicle']		= $vehicle;
		$data['deliveries']		= Delivery::completed()->where(['driver_id' => $vehicle->driver->id])->get();

		return view('admin.vehicles.vehicle.index', $data);
	}

	public function documentStatus()
	{
		$vehicle = Vehicle::find(request('id'));

		if(!$vehicle)
			return redirect('admin/vehicles')->with('error', 'Vehicle not found.');

		if(request('insurance_status')){

			$vehicle->update(['insurance_status' => request('insurance_status')]);
			_log('Marked vehicle Insurance Document as '.request('insurance_status').' - '.$vehicle->plate_number);
		}

		if(request('certificate_status')){

			$vehicle->update(['certificate_status' => request('certificate_status')]);
			_log('Marked vehicle Certificate Document as '.request('certificate_status').' - '.$vehicle->plate_number);
		}

		return redirect('admin/vehicles/vehicle/'.$vehicle->id)->with('message', 'Vehicle updated Succesfully.');
	}
}
