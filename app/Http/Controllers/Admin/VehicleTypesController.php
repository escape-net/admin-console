<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\VehicleType;
use App\Http\Requests\CreateVehicleTypeRequest;


class VehicleTypesController extends Controller
{
    public function index()
	{
		$data['vehicle_types']	= VehicleType::get();

		return view('admin.vehicle-types.index', $data);
	}

	public function form(Request $request, $id = 0)
	{
		$data['vehicle_type'] 	= VehicleType::firstOrNew(['id' => $id]);
		
		return view('admin.vehicle-types.form', $data); 
	}

	public function save(CreateVehicleTypeRequest $request)
	{		
		$vehicle_type = VehicleType::updateOrCreate(['id' => request('id')],[

			'name'    					=> request('name'),
			'base_fare'					=> request('base_fare'),
			'per_minute'				=> request('per_minute'),
			'per_km'     				=> request('per_km'),
			'commission_percentage'     => request('commission_percentage'),
			'cancellation_fee'     		=> request('cancellation_fee'),
			'night_start_time'     		=> request('night_start_time'),
			'night_end_time'     		=> request('night_end_time'),
			'night_surcharge'     		=> request('night_surcharge'),
			'night_surcharge'     		=> request('night_surcharge')
		]);

		_log('Updated Vehicle Type Information - '.$vehicle_type->name);
		return redirect('admin/vehicle-types')->with('message', 'Vehicle Type Saved Succesfully.');
	}
}
