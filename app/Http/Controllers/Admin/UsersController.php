<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateAdminRequest;
use App\User;

class UsersController extends Controller
{
	public function index()
	{
		$data['users']	= User::users()->get();

		return view('admin.users.index', $data);
	}

	public function form(Request $request, $id = 0)
	{
		$data['user'] = User::firstOrNew(['id' => $id]);

		return view('admin.users.form', $data); 
	}

	public function save(CreateAdminRequest $request)
	{
		$user = User::updateOrCreate(['id' => request('id')],[

			'first_name'    => request('first_name'),
			'last_name'     => request('last_name'),
			'email'         => request('email'),
			'phone'         => request('phone'),
			'role'          => 'user',
			'status'        => request('status')
		]);

		_log('Updated User Information', $user);
		return redirect('admin/users')->with('message', 'User Saved Succesfully.');
	}
    
}
