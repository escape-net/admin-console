<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Delivery;
use App\TrackStatus;
use App\TrackHistory;

class TrackDeliveryController extends Controller
{
	public function index($reference = '')
	{

		$data = [];

		$reference = request('reference', $reference);

		if($reference){

			if($reference[0] == '#'){

				$reference = substr($reference, 1);
			}

			$delivery	= Delivery::where(['reference' => $reference])->first();

			if(!$delivery){

				return redirect('admin/track-delivery')->with('error', 'Delivery with Reference '.request('reference').' Not Found.');
			}

			$data['delivery'] 	= $delivery;
			$data['statuses']	= TrackStatus::orderBy('order', 'asc')->get();
			$data['history']	= TrackHistory::where(['delivery_id' => $delivery->id])->orderBy('created_at', 'desc')->get();
		}
	
		return view('admin.track-delivery.index', $data);
	}

	public function saveHistory(Request $request)
	{
		$delivery = Delivery::find(request('id'));

		if(!$delivery){

			return redirect('admin/track-delivery')->with('error', 'Delivery Not Found.');
		}

		$status = TrackStatus::firstOrNew(['id' => request('track_status_id')]);

		TrackHistory::create([

			'delivery_id'			=> $delivery->id,
			'track_status_id'		=> request('track_status_id'),
			'status'				=> $status->description,
			'comment'				=> request('comment')
		]);

		_log('Updated Dilivery #'.$delivery->reference.' Track History :: '.$status->description. '('.request('comment'));

		return redirect('admin/track-delivery/'.$delivery->reference)->with('message', 'Track History added successfully.');

	}
}
