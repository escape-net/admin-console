<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Promotion;
use App\User;
use App\Delivery;
use App\Http\Requests\CreatePromotionRequest;

class PromotionsController extends Controller
{
	public function index()
	{
		$data['promotions']	= Promotion::get();

		return view('admin.promotions.index', $data);
	}

	public function form(Request $request, $id = 0)
	{	
		$promotion 			= Promotion::firstOrNew(['id' => $id]);
		$data['promotion'] 	= $promotion;
		$data['users']		= User::users()->active()->orderBy('first_name')->get();
		$data['selected']	= $promotion->users == null ? [] : json_decode($promotion->users, true);
		
		return view('admin.promotions.form', $data); 
	}

	public function save(CreatePromotionRequest $request)
	{		
		$promotion = Promotion::updateOrCreate(['id' => request('id')],[

			'code'    		=> request('code'),
			'users'     	=> json_encode(request('users')),
			'type'         	=> request('type'),
			'amount'        => request('amount'),
			'validity'      => request('validity'),
			'start_at'      => request('start_at'),
			'end_at'        => request('end_at'),
			'limit'         => request('limit'),
			'description'   => request('description'),
			'status'        => request('status')
		]);

		_log('Updated Promotion Information. - '.$promotion->code);
		return redirect('admin/promotions')->with('message', 'Promotion Saved Succesfully.');
	}
}
