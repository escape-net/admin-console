<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateDriverRequest;
use App\User;
use App\Company;
use App\Bank;
use App\Vehicle;
use App\Delivery;

class DriversController extends Controller
{
    public function index()
	{
		$data['users']	= User::drivers()->get();

		return view('admin.drivers.index', $data);
	}

	public function form(Request $request, $id = 0)
	{
		$data['user'] 		= User::firstOrNew(['id' => $id]);
		$data['companies']	= Company::active()->get();
		$data['banks']		= Bank::get();
		$data['vehicles']	= Vehicle::where('user_id', null)->orWhere('user_id', $id)->get();

		return view('admin.drivers.form', $data); 
	}

	public function save(CreateDriverRequest $request)
	{
		$user = User::updateOrCreate(['id' => request('id')],[

			'company_id'			=> request('company_id'),
			'first_name'    		=> request('first_name'),
			'last_name'     		=> request('last_name'),
			'email'         		=> request('email'),
			'phone'         		=> request('phone'),
			'address'				=> request('address'),
			'role'          		=> 'driver',
			'driver_status'        	=> request('driver_status'),
			'status'        		=> request('status'),
			'bank_id'				=> request('bank_id'),
			'bank_account_number'	=> request('bank_account_number'),
			'bank_account_name'		=> request('bank_account_name')
		]);

		if(request('vehicle_id')){

			Vehicle::where(['id' => request('vehicle_id')])->update(['user_id' => $user->id]);
		}

		_log('Updated Driver Information', $user);
		return redirect('admin/drivers')->with('message', 'Driver Saved Succesfully.');
	}

	public function driver($id = 0)
	{
		$data['driver']				= User::firstOrNew(['id' => $id]);
		$data['recent_deliveries']	= Delivery::completed()->where(['driver_id' => $id])->orderBy('id', 'desc')->take(10)->get();
		
		return view('admin.drivers.driver', $data);
	}
}
