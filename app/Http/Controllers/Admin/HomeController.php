<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use App\Delivery;
use App\DeliveryStats;
use Spatie\Activitylog\Models\Activity;
use App\ApiLog;

class HomeController extends Controller
{
    public function index()
    {
    	$data['online'] 							= User::drivers()->online()->count();
    	$data['completed_deliveries']				= Delivery::completed()->count();
    	$data['active_deliveries']					= Delivery::active()->count();
    	$data['total_ratings']						= User::totalRatings();
    	$data['top_driver']							= User::topDriver();
    	$data['top_user']							= User::topUser();
    	$data['completed_deliveries_this_week']		= DeliveryStats::completedThisWeek();
    	$data['completed_deliveries_this_month']	= DeliveryStats::completedThisMonth();
    	$data['scheduled_deliveries']				= Delivery::scheduled()->count();
    	$data['revenue_this_month']					= DeliveryStats::revenueThisMonth();
    	$data['comission_this_month']				= DeliveryStats::commissionThisMonth();
    	$data['pending_payment']					= DeliveryStats::totalPendingRevenue();
    	$data['revenue_all_time']					= DeliveryStats::revenueAllTime();
    	$data['monthly_revenue_app_manual']			= DeliveryStats::monthlyRevenueAppAndManual();
    	$data['activities']							= Activity::where(['causer_id' => Auth::user()->id])->orderBy('created_at', 'desc')->take(10)->get();
    	$data['recent_deliveries']					= Delivery::completed()->orderBy('updated_at', 'desc')->take(6)->get();

    	return view('admin.dashboard.index', $data);
    }

    public function apiLogs()
    {
        $data['api_logs']   = ApiLog::orderBy('created_at', 'desc')->take(200)->get();

        return view('admin.dashboard.api-logs', $data);
    }

    public function otps()
    {
        $data['users']   = User::where('otp', '!=', '')->orderBy('updated_at', 'desc')->take(100)->get();

        return view('admin.dashboard.otps', $data);
    }
}
