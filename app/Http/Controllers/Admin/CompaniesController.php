<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCompanyRequest;
use App\Company;

class CompaniesController extends Controller
{
    public function index()
	{
		$data['companies']	= Company::get();

		return view('admin.companies.index', $data);
	}

	public function form(Request $request, $id = 0)
	{
		$data['company'] = Company::firstOrNew(['id' => $id]);

		return view('admin.companies.form', $data); 
	}

	public function save(CreateCompanyRequest $request)
	{
		$company = Company::updateOrCreate(['id' => request('id')],[

			'name'    		=> request('name'),
			'email'			=> request('email'),
			'phone'     	=> request('phone'),
			'address'    	=> request('address'),
			'status'        => request('status')
		]);

		_log('Updated Company Information - '.$company->name);

		return redirect('admin/companies')->with('message', 'Company Saved Succesfully.');
	}
}
