<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TrackStatus;

class TrackStatusesController extends Controller
{
    public function index()
	{
		$data['statuses']	= TrackStatus::get();

		return view('admin.track-statuses.index', $data);
	}

	public function form(Request $request, $id = 0)
	{
		$data['status'] = TrackStatus::firstOrNew(['id' => $id]);

		return view('admin.track-statuses.form', $data); 
	}

	public function save(Request $request)
	{
		$status = TrackStatus::updateOrCreate(['id' => request('id')],[

			'order'    		=> request('order'),
			'description'   => request('description'),
			'color'   		=> request('color')
		]);

		 _log('Updated Track Status '.$status->description);

		return redirect('admin/track-statuses')->with('message', 'Track Status Saved Succesfully.');
	}
}
