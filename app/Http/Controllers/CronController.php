<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Delivery;
use App\User;
use Carbon\Carbon;

class CronController extends Controller
{
    public function clearAssignedDriversWhoDidNotStartDelivery()
    {
        $assignedDrivers = User::where(['role' => 'driver'])->assigned()->get();

        foreach($assignedDrivers as $driver){

            $deliveries = Delivery::where(['driver_id' => $driver->id])->driverAssigned()->get();

            foreach($deliveries as $delivery){

                if($delivery->updated_at->diffInMinutes(Carbon::now()) > 5){

                    $delivery->update(['status' => 'pending']);
                    $driver->update(['driver_status' => 'offline']);
                }
            }

            if($deliveries->count() < 1){

                if($driver->updated_at->diffInMinutes(Carbon::now()) > 5)
                    $driver->update(['driver_status' => 'offline']);
            }
            
        }        
    }
}
