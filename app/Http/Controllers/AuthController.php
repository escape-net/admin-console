<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;

class AuthController extends Controller
{
    public function login(Request $request)
    {           
        if(Auth::check())
            return redirect('admin');

        if(request()->isMethod('post')){

            if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){

                _log('New Sign In');

                return redirect('admin');

            }else{

                session()->flash('error', 'Invalid Username or password');
            }
        }

        return view('admin.auth.login');
    }

    public function logout()
    {
        Auth::logout();
        _log('Logout Successful.');
        
        return redirect('login');
    }

    public function forgotPassword(Request $request)
    {
        if(request()->isMethod('post')){

            $user = User::where(['email' => request('email'), 'role' => 'admin'])->first();

            if(!$user) 
                return redirect('forgot-password')->with('error', 'Opzz!... We do not recognize this Email Address.');

            if($user->status == 0) 
                return redirect('forgot-password')->with('error', 'Your account is currently inactive. Please contact your Administrator.');

            $token = str_random(6);

            $user->update(['remember_token' => $token]);

            $subject    = 'Here is the Password Reset you Ordered For';
            $email_data = [

                'user'      => $user,
                'token'     => $token,
                'subject'   => $subject
            ];

            $body = view('admin.emails.auth.reset-password', $email_data)->render();

            _log('Forgot Password Email sent');
            _email($user->email, $subject, $body);

            return redirect('forgot-password')->with('message', 'A password reset email has been sent to '.$user->email);

        }

        return view('admin.auth.forgot-password');
    }

    public function resetPassword($token = '')
    {
        $user = User::where(['remember_token' => $token])->first();

        if(!$user) return redirect('login');

        $data['user']   = $user;

        return view('admin.auth.reset-password', $data);
    }

    public function changePassword(Request $request)
    {
        $this->validate($request, [

            'password'  => 'required|min:6|confirmed'
        ]);

        $user = User::find(request('id'));

        if($user && $user->role == 'admin' && $user->remember_token == request('token')){

            $user->update(['password' => bcrypt(request('password'))]);
             _log('Password changed', $user);
             
            return redirect('login')->with('message', 'Your Password has been reset Successfully. You can now login.');
        }

        return redirect('login')->with('error', 'Password Reset failed');
    }
}
