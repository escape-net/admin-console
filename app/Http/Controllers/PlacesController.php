<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \GuzzleHttp\Client;
use App\Place;

class PlacesController extends Controller
{
    public function search()
    {
    	try{

    		$address  	= request('term');

            if(!$address) return response()->json(false);

    		$key 		= env('GOOGLE_API_KEY');
    		//$endpoint 	= "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=$address&inputtype=textquery&fields=formatted_address,name,rating,opening_hours,geometry,place_id&key=$key";
            $endpoint   = "https://maps.googleapis.com/maps/api/geocode/json?address=$address&key=$key";
    		
    		$response = (new Client())->request('GET', $endpoint)->getBody();
    		$response = json_decode($response);

    		if(count($response->results) < 1){

    			return response()->json(['results' => ['id' => 1, 'text' => 'No places match your search query']]);
    		}

            $data     = $response->results[0];

            $country                        = ''; 
            $administrative_area_level_1    = ''; 
            $administrative_area_level_2    = '';

            foreach($data->address_components as $row){

                if($row->types[0] == 'country')
                    $country = $row->short_name;

                if($row->types[0] == 'administrative_area_level_1')
                    $administrative_area_level_1 = $row->short_name;

                if($row->types[0] == 'administrative_area_level_2')
                    $administrative_area_level_2 = $row->short_name;
            }

            //return response()->json($data);

    		$place = Place::updateOrCreate(['google_place_id' => $data->place_id], [

    			'name'				           => $data->formatted_address,
    			'address'			           => $data->formatted_address,
    			'google_place_id'	           => $data->place_id,
    			'latitude'			           => $data->geometry->location->lat,
    			'longitude'			           => $data->geometry->location->lng,
                'country'                      => $country,
                'administrative_area_level_1'  => $administrative_area_level_1,
                'administrative_area_level_2'  => $administrative_area_level_2
    		]);

    		$result = [

    			'id' 	=> $place->id,
    			'text'	=> $place->address
    		];

    		return response()->json(['results' => [$result]]);

    	}catch(Exception $e){

            return response()->json(['error' => $e->geMessage()]);
    	}
    }

    public function places()
    {
        $places = Place::get();

        $data = [];

        foreach($places as $row){

            $data[] = ['id' => $row->id, 'name' => $row->address];
        }

        return response()->json($data);
    }
}
