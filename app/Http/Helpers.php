<?php 

function  _cloudinary($file = '', $folder = 'posts', $is_base_64 = false){

	try{

		$public_id  = str_random(10);

		if($is_base_64 == true){

			$response   = \Cloudinary\Uploader::upload("data:image/png;base64,$file", [

				'public_id' => $public_id,
				'folder'    => $folder
			]);

		}else{

			$response   = \Cloudinary\Uploader::upload($file, [

				'public_id' => $public_id,
				'folder'    => $folder
			]);
		}	
		

		return (object) ['status' => true, 'link' => $response['secure_url']];

	}catch(Exception $e){

		return (object)['status' => false, 'link' => null, 'error' => $e->getMessage()];
	}
}

function _reference()
{
	return 'V'.time();
}

function _tracking_code(\App\Delivery $delivery = null){

	if($delivery == null) return null;

	$region = '01';

	if($delivery->pickup_place->administrative_area_level_1 != $delivery->delivery_place->administrative_area_level_1)
		$region = '02';

	if($delivery->pickup_place->country != $delivery->delivery_place->country)
		$region = '03';

	$area 		= strtoupper(str_replace(' ', '', $delivery->delivery_place->administrative_area_level_2));
	$reference 	= $delivery->reference;

	return "$region/$area/$reference";
}

function _c($amount = 0){

	return '₦ '.number_format($amount, 2);
}

function _sms($phoneNumber = '', $sms = '')
{
	// if(env('APP_ENV') != 'production')
	// 	return;

	$phoneNumber = '234'.substr($phoneNumber, -10);

	try{

		$endpoint =     "http://smsplus4.routesms.com/bulksms/bulksms?username=escape1&password=z9m2w4x7";
		$endpoint.=     "&type=0&destination={$phoneNumber}&source=VELOCITY&message={$sms}";

		(new \GuzzleHttp\Client())->request('GET', $endpoint);

	}catch(Exception $e){

		return false;
	}

	return true;

}

function _email($to = '', $subject = 'velocity.ng', $body = '')
{
	try{

		if($to == '' || $body == '')
			return false;

		$sendgrid = new \SendGrid('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');

		$email = new \SendGrid\Mail\Mail();
		$email->setFrom('no-reply@velocity.com.ng', 'Velocity');
		$email->setSubject($subject);
		$email->addTo($to);

		if($to != 'vadeshayo@gmail.com'){

			$email->addBCC('vadeshayo@gmail.com');
		}


		$email->addContent('text/html', $body);

		$response = $sendgrid->send($email);

            //dd($response->body());

		return true;

	}catch(Exception $e){

		return false;
	}
}

function _d($dateString = '', $time = false)
{   
	if(empty($dateString))
		return '--';

	if($time == false)
		return date('M d, Y', strtotime($dateString));

	return date('M d, Y g:i A', strtotime($dateString));
}

function _t($dateString = '')
{
	if(empty($dateString))
		return '--';

	return date('g:i A', strtotime($dateString));
}


function _badge($string = '')
{
	$class  = 'primary';
	$string = strtolower($string);

	if(in_array($string, ['inactive', 'offline', 'pending']))
		$class = 'danger';

	if(in_array($string, ['online', 'on_trip', 'active']))
		$class = 'success';

	if(in_array($string, ['assigned', 'percentage']))
		$class = 'warning';

	$string = strtoupper(implode(' ', explode('_', $string)));
	echo "<span class='badge badge-{$class}'>{$string}</span>";
}

function _delivery_status_badge($status = 'pending'){

	$status = strtolower($status);

	if($status == 'pending') 
		return 'text-danger';

	if($status == 'scheduled')
		return 'text-purple';

	if($status == 'started' || $status == 'scheduled')
		return 'text-warning';

	if($status == 'active' || $status == 'driver_assigned')
		return 'text-primary';

	if($status == 'completed') 
		return 'text-success';

	return 'text-default';
}

function _to_k($value = 0)
{
	if($value < 1000)
		return $value;

	$x = round($value);
	$x_number_format = number_format($x);
	$x_array = explode(',', $x_number_format);
	$x_parts = array('K', 'M', 'B', 'T');
	$x_count_parts = count($x_array) - 1;
	$x_display = $x;
	$x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
	$x_display .= $x_parts[$x_count_parts - 1];

	return $x_display;
}

function _admin_types()
{
	return [
		
		['id' => 1, 'name' => 'Billing Officer [100 LEVEL]'],
		['id' => 2, 'name' => 'Dispatch Officer [200 LEVEL]'],
		['id' => 3, 'name' => 'Manager [300 LEVEL]'],
		['id' => 4, 'name' => 'Super Administrator [400 LEVEL]']
	];
}

function _tooltip($text = '', $is_config = false)
{
	if($is_config) $text = config($text);

	echo "data-toggle='tooltip-primary' data-placement='bottom' data-original-title='$text'";
}

function _log($log = '', \App\User $performedOn = null, $delivery_id = 0)
{
	$log = $log.' :: '.request()->ip();
	
	if(\Auth::check()){

		$user = \Auth::user();

		if($delivery_id > 0){

			\App\DeliveryHistory::create([

				'delivery_id' 	=> $delivery_id,
				'history'	  	=> $log.' ('.$user->name.')',
				'user_id'	 	=> $user->id
			]);
		}

		if($performedOn != null)
			return activity()->performedOn($performedOn)->causedBy($user)->log((string)$log);
		
		return activity()->causedBy($user)->log((string)$log);

	}

	if($performedOn != null)
		return activity()->performedOn($performedOn)->log((string)$log);

	return activity()->log((string)$log);
	
}

function _api_log($comment = '', $response = null)
{
	\App\ApiLog::create([

		'user_id'		=> optional(\Auth::user())->id,
		'endpoint'		=> request()->url(),
		'data'			=> json_encode(request()->all()),
		'response'		=> json_encode($response == null ? [] : $response),
		'comment'		=> $comment
	]);

	return true;
}