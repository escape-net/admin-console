<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\VehicleType;
use App\ParcelSize;
use \Carbon\Carbon;
use App\Promotion;
use App\DeliveryHistory;
use App\Events\DeliveryUpdated;
use App\User;

class Delivery extends Model
{
	use SoftDeletes;
	
	protected $hidden 	= ['updated_at', 'deleted_at'];
	protected $guarded 	= ['updated_at'];

	protected $dispatchesEvents = [

		'updated' 	=> DeliveryUpdated::class,
	];

	public function user()
	{
		return $this->belongsTo('App\User')->withDefault(function(){

			return new \App\User();
		});
	}

	public function vehicle_type()
	{
		return $this->belongsTo('App\VehicleType')->withDefault(function(){

			return new \App\VehicleType();
		});
	}

	public function pickup_place()
	{
		return $this->belongsTo('App\Place', 'pickup_place_id')->withDefault(function(){

			return new \App\Place();
		});
	}

	public function delivery_place()
	{
		return $this->belongsTo('App\Place', 'delivery_place_id')->withDefault(function(){

			return new \App\Place();
		});
	}

	public function driver()
	{
		return $this->belongsTo('App\User', 'driver_id')->withDefault(function(){

			return new \App\User();
		});
	}

	public function transaction()
	{
		return $this->belongsTo('App\Transaction')->withDefault(function(){

			return new \App\Transaction();
		});
	}

	public function promotion()
	{
		return $this->belongsTo('App\Promotion')->withDefault(function(){

			return new \App\Promotion();
		});
	}

	public function histories()
	{
		return $this->hasMany('App\DeliveryHistory');
	}

	public function setScheduleAtAttribute($value)
	{
		return date('Y-m-d', strtotime($value));
	}

	public function scopePending($query)
	{
		return $query->where(['status' => 'pending']);
	}

	public function scopeScheduled($query)
	{
		return $query->where(['status' => 'scheduled']);
	}

	public function scopeDriverAssigned($query)
	{
		return $query->where(['status' => 'driver_assigned']);
	}

	public function scopeActive($query)
	{
		return $query->where(['status' => 'active']);
	}

	public function scopeCompleted($query)
	{
		return $query->where(['status' => 'completed']);
	}

	public function scopeBy($query, $driver_id = 0)
	{
		return $query->where(['driver_id' => $driver_id]);
	}
	
	public static function saveEstimate(Delivery $delivery)
	{
		$vehicle_type  = VehicleType::firstOrNew(['id' => $delivery->vehicle_type_id]);
		
		$baseFare 				= $vehicle_type->base_fare;
		$perMinute				= $vehicle_type->per_minute;
		$perKM 					= $vehicle_type->per_km;
		$cancellationFee 		= $vehicle_type->cancellation_fee;
		$distance 				= self::getDistanceDifferenceInKm($delivery->pickup_place, $delivery->delivery_place);
		$surcharge				= 0;
		$discount 				= 0;
		$minimumEstimate 		= $baseFare + ($perMinute * $distance) + $surcharge - $discount;
		$maximumEstimate 		= $minimumEstimate + floor($minimumEstimate * .4);

		$delivery->update([

			'base_fare'				=> (float)$baseFare,
			'per_minute'			=> (int)$perMinute,
			'per_km'				=> (int)$perKM,
			'approx_km'				=> (int)$distance,
			'cancellation_fee'		=> (float)$cancellationFee,
			'surcharge'				=> (float)$surcharge,
			'discount'				=> (float)$discount,
			'minimum' 				=> ceil($minimumEstimate / 100) * 100, 
			'maximum' 				=> ceil($maximumEstimate / 100) * 100

		]);

		return true;
	}

	public static function getDistanceDifferenceInKm(Place $pickup_place, Place $delivery_place)
	{
		$pickup_lat 	= $pickup_place->latitude;
		$pickup_long 	= $pickup_place->longitude;

		$delivery_lat 	= $delivery_place->latitude;
		$delivery_long	= $delivery_place->longitude;


		$theta 		= $pickup_long - $delivery_long;
		$distance 	= sin(deg2rad($pickup_lat)) * sin(deg2rad($delivery_lat)) + cos(deg2rad($pickup_lat)) * cos(deg2rad($delivery_lat)) * cos(deg2rad($theta));
		$distance 	= acos($distance);
		$distance 	= rad2deg($distance);
		$miles 		= $distance * 60 * 1.1515;

		if($miles < 1)
			return 1;
		
		return round($miles * 1.609344);
	}

	public static function getAll()
	{
		$deliveries = self::orderBy('id', 'desc')->get();

		$data = [];

		foreach($deliveries as $row){

			$data[] = [

				'id'			=> $row->id,
				'user'			=> $row->user->name,
				'item'			=> ucwords($row->item),
				'date'			=> _d($row->created_at),
				'pickup'		=> $row->pickup_place->address,
				'status'		=> $row->status,
				'star'			=> $row->status == 'completed'  ? 'active' : '',
				'image'			=> $row->user->photo ?? asset('img/deliveries.png')
			];
		}

		$pending 	= collect($deliveries)->where('status', 'pending')->count();
		$active 	= collect($deliveries)->where('status', 'active')->count();
		$completed 	= collect($deliveries)->where('status', 'completed')->count();

		return [

			'deliveries' 	=> $data,
			'pending'		=> _to_k($pending),
			'active'		=> _to_k($active),
			'completed'		=> _to_k($completed)
		];
	}

	public static function getSingle($id = 0)
	{
		$delivery = self::with(['user', 'vehicle_type', 'pickup_place', 'delivery_place', 'driver', 'transaction', 'promotion', 'histories'])->find($id);

		if(!$delivery) return false;

		$delivery->user->name 				= $delivery->user->name;
		$delivery->driver->name 			= $delivery->driver->name;
		$delivery->date 					= _d($delivery->created_at, true);
		$delivery->badge 					= _delivery_status_badge($delivery->status);
		$delivery->base_fare 				= _c($delivery->base_fare);
		$delivery->per_minute 				= _c($delivery->per_minute);
		$delivery->per_km 					= _c($delivery->per_km);
		$delivery->minimum 					= _c($delivery->minimum);
		$delivery->maximum 					= _c($delivery->maximum);
		$delivery->scheduled 				= $delivery->scheduled_at == null ? false : true;
		$delivery->schedule_difference 		= $delivery->scheduled_at != null ? Carbon::now()->diffInDays(Carbon::parse($delivery->scheduled_at)) : '';
		$delivery->scheduled_at	 			= _d($delivery->scheduled_at, true);
		$delivery->start_at 				= _d($delivery->start_at, true);
		$delivery->end_at 					= _d($delivery->end_at, true);
		$delivery->discount 				= _c($delivery->discount);
		$delivery->subtotal 				= _c($delivery->subtotal);
		$delivery->total 					= _c($delivery->total);
		$delivery->commission 				= _c($delivery->commission);
		$delivery->total_minutes 			= number_format($delivery->total_minutes).'Mins';
		$delivery->total_km 				= number_format($delivery->total_km).'KM';
		$delivery->status 					= strtoupper(implode(' ', explode('_', $delivery->status)));
		$delivery->transaction->amount 		= _c($delivery->transaction->amount);
		$delivery->user->photo 				= $delivery->user->photo ?? asset('img/deliveries.png');

		return $delivery;
	}

	public static function getTotalAndDiscount($id = 0, $total_km = 0, $total_minutes = 0)
	{
		$delivery = self::find($id);
		if(!$delivery) return 0;

		$discount 	= 0;
		$total 		= $delivery->base_fare + ($delivery->per_minute * $total_minutes) + ($delivery->per_km * $total_km);

		if($delivery->promotion_id > 0){

			$discount = Promotion::getPromotionTotal($delivery->promotion_id, $total);
		}

		return ['total' => $total, 'discount' => $discount];
	}

	public static function search($query = '')
	{
		if((int)$query > 0){

			return self::where('total', 'like', "%$query%")
			->orWhere('subtotal', 'like', "%$query%")
			->orWhere('recipient_phone', 'like', "$query%")
			->orderBy('created_at', 'desc')
			->take(50)
			->get();
		}

		$user = User::where('phone', 'like', "%$query%")->first();

		if($user){

			return self::where(['user_id' => $user->id])->orderBy('created_at', 'desc')->get();
		}

		return self::where('status', 'like', "$query")
		->orWhere('reference', 'like', "%$query%")
		->orWhere('item', 'like', "%$query%")
		->orderBy('created_at', 'desc')
		->take(50)
		->get();
	}

	public static function getApiDelivery($id = 0)
	{
		$delivery = Delivery::with([

			'user' => function($query) { 

				$query->select('id', 'first_name', 'last_name', 'phone', 'email'); 
			},
			'pickup_place' => function($query){

				$query->select('id', 'address', 'google_place_id', 'latitude', 'longitude');
			}, 
			'delivery_place' => function($query){

				$query->select('id', 'address', 'google_place_id', 'latitude', 'longitude');
			}

		])->where(['id' => $id])->first();

		$delivery->driver = User::getApiUser($delivery->driver);

		return $delivery;
	}

	public static function getApiDeliveries($type = 'user_id', $id = 0)
	{
		$deliveries = Delivery::with([

			'user' => function($query) { 

				$query->select('id', 'first_name', 'last_name'); 
			}, 

			'pickup_place' => function($query){

				$query->select('id', 'address');
			}, 
			'delivery_place' => function($query){

				$query->select('id', 'address');
			}
		])
		->where([$type => $id])
		->whereIn('status', ['driver_assigned', 'active', 'completed'])
		->orderBy('id', 'desc')
		->get();

		$deliveries = $deliveries->map(function($row){

			return collect($row)->only(['id', 'reference', 'created_at', 'item', 'recipient_name', 'recipient_phone', 'status', 'user', 'pickup_place', 'delivery_place', 'total', 'instructions', 'commission', 'start_at', 'end_at'])->toArray();

		});

		return $deliveries;
	}
}
