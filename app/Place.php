<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\User;

class Place extends Model
{
	protected $hidden   = ['updated_at'];
	protected $guarded  = ['updated_at'];

	public static function closestUserToLocation($latitude = 0, $longitude = 0, $exclude_driver_id = 0)
	{	
		//Distance is in Miles.
		$query = "
		id, 
		latitude, 
		longitude,  
		3956 * 2 * ASIN(SQRT(POWER(SIN(($latitude -abs(latitude)) * 
		pi()/180 / 2),2) + COS($latitude * pi()/180 ) * 
		COS(abs(latitude) *  pi()/180) * 
		POWER(SIN(($longitude - abs(longitude)) *  pi()/180 / 2), 2))
		) as distance
		";
		
		return DB::table('users')
		->selectRaw($query)
		->havingRaw('distance < ?', [20])
		->whereRaw('role = ? and driver_status = ? and latitude != 0 and longitude != 0', ['driver', 'online'])
		->where('id', '!=', $exclude_driver_id)
		->orderByRaw('distance')
		->first();

	}
}
