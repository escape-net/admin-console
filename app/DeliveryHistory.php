<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryHistory extends Model
{
    protected $hidden 	= ['updated_at', 'deleted_at'];
	protected $guarded 	= ['updated_at'];

	public function delivery()
	{
		return $this->belongsTo('App\Delivery')->withDefault(function(){

			return new \App\Delivery();
		});
	}

	public function user()
	{
		return $this->belongsTo('App\User')->withDefault(function(){

			return new \App\User();
		});
	}
}
