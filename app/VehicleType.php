<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleType extends Model
{
    protected $hidden 	= ['updated_at', 'deleted_at'];
	protected $guarded 	= ['updated_at'];

	public function vehicles()
	{
		return $this->hasMany('App\Vehicle');
	}
}
