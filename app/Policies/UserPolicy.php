<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if($user->role == 'admin' && $user->access_level == 4) return true;
    }

    //Only Managers
    public function writeDriver(User $user, User $driver = null)
    {
        return $user->role == 'admin' && !is_null($user->access_level) && $user->access_level  == 3;
    }

    //Only Managers and Super Admins
    public function viewAdminMenu(User $user, User $_user = null)
    {
        return $user->role == 'admin' && !is_null($user->access_level) && $user->access_level  == 3;
    }
}
