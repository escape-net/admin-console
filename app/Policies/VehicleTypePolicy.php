<?php

namespace App\Policies;

use App\User;
use App\VehicleType;
use Illuminate\Auth\Access\HandlesAuthorization;

class VehicleTypePolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if($user->role == 'admin' && $user->access_level == 4) return true;
    }

    //Only Super Admins
    public function write(User $user, VehicleType $vehicleType = null)
    {
        return $user->role == 'admin' && !is_null($user->access_level) && $user->access_level  == 4;
    }

    //Only Super Admins
    public function view(User $user, VehicleType $vehicleType = null)
    {
        return $user->role == 'admin' && !is_null($user->access_level) && $user->access_level  == 3;
    }
}
