<?php

namespace App\Policies;

use App\User;
use App\Delivery;
use Illuminate\Auth\Access\HandlesAuthorization;

class DeliveryPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if($user->role == 'admin' && $user->access_level == 4) return true;
    }

    //Only Managers
    public function write(User $user, Delivery $delivery = null)
    {
        return $user->role == 'admin' && !is_null($user->access_level) && $user->access_level  == 3;
    }

    //Dispatch and Managers
    public function viewGodsEye(User $user, Delivery $delivery = null)
    {
        return $user->role == 'admin' && !is_null($user->access_level) && $user->access_level  >= 2;
    }
}
