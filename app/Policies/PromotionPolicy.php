<?php

namespace App\Policies;

use App\User;
use App\Promotion;
use Illuminate\Auth\Access\HandlesAuthorization;

class PromotionPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if($user->role == 'admin' && $user->access_level == 4) return true;
    }

    //Only Managers
    public function view(User $user, Promotion $promotion = null)
    {
        return $user->role == 'admin' && !is_null($user->access_level) && $user->access_level  == 3;
    }

    //Only Admins
    public function write(User $user, Promotion $promotion = null)
    {
        return $user->role == 'admin' && !is_null($user->access_level) && $user->access_level  == 4;
    }
}
