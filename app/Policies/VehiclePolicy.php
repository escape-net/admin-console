<?php

namespace App\Policies;

use App\User;
use App\Vehicle;
use Illuminate\Auth\Access\HandlesAuthorization;

class VehiclePolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if($user->role == 'admin' && $user->access_level == 4) return true;
    }

    //Only Managers
    public function write(User $user, Vehicle $vehicle = null)
    {
        return $user->role == 'admin' && !is_null($user->access_level) && $user->access_level  == 3;
    }
}
