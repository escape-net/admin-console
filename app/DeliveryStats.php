<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use \Carbon\Carbon;

class DeliveryStats extends Model
{
    public static function completedThisWeek()
    {
    	$start  = Carbon::now()->startOfWeek()->format('Y-m-d H:i:s');
        $end    = Carbon::now()->endOfWeek()->format('Y-m-d H:i:s');

    	return DB::table('deliveries')
    	->where(['status' => 'completed'])
    	->whereBetween('updated_at', [$start, $end])
    	->count();
    }

    public static function completedThisMonth()
    {
    	$start  = Carbon::now()->startOfMonth()->format('Y-m-d H:i:s');
        $end    = Carbon::now()->startOfMonth()->format('Y-m-d H:i:s');

    	return DB::table('deliveries')
    	->where(['status' => 'completed'])
    	->whereBetween('updated_at', [$start, $end])
    	->count();
    }

    public static function revenueThisMonth()
    {
    	$start  = Carbon::now()->startOfWeek()->format('Y-m-d H:i:s');
        $end    = Carbon::now()->endOfWeek()->format('Y-m-d H:i:s');

    	return DB::table('deliveries')
    	->where(['status' => 'completed'])
    	->whereBetween('updated_at', [$start, $end])
    	->sum('total');
    }

    public static function commissionThisMonth()
    {
        $start  = Carbon::now()->startOfMonth()->format('Y-m-d H:i:s');
        $end    = Carbon::now()->endOfMonth()->format('Y-m-d H:i:s');

    	return DB::table('deliveries')
        ->where('status', 'completed')
        ->whereBetween('updated_at', [$start, $end])
        ->sum('commission');
    }

    public static function totalPendingRevenue()
    {
    	return DB::table('deliveries')
    	->where('status', '!=', 'completed')
    	->sum('minimum');
    }

    public static function revenueAllTime()
    {
    	return DB::table('deliveries')
    	->where(['status' => 'completed'])
    	->sum('total');
    }

    public static function monthlyRevenueAppAndManual()
    {
    	return [];
    }
}
