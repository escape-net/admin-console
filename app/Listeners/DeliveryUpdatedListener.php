<?php

namespace App\Listeners;

use App\Events\DeliveryUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Broadcast;
use App\Delivery;

class DeliveryUpdatedListener
{
    public $delivery;

    public function __construct()
    {
        
    }

    public function handle(DeliveryUpdated $event)
    {
        $this->delivery = $event->delivery;

        if($this->delivery->isDirty('driver_id'))
            $this->driverAssigned();

        if($this->delivery->isDirty('status'))
            $this->statusChanged();
    }

    private function driverAssigned()
    {
        $delivery = $this->delivery;

        $new_driver_id = $delivery->driver_id;
        $old_driver_id = $delivery->getOriginal('driver_id');

        if($old_driver_id > 0) return;

        $sms = "You have been assigned to Delivery #{$delivery->reference}";

        Broadcast::create([

            'user_id'   => $delivery->user_id,
            'type'      => 'sms',
            'body'      => $sms,
            'phone'     => $delivery->driver->phone
        ]);

        $subject    = $delivery->driver->name.' Has been assigned to your Delivery #'.$delivery->reference;
        $body       = view('admin.emails.delivery.driver-assigned', ['delivery' => $delivery, 'subject' => $subject])->render();

        Broadcast::create([

            'user_id'   => $delivery->user_id,
            'type'      => 'email',
            'body'      => $body,
            'subject'   => $subject,
            'email'     => $delivery->user->email
        ]);

    }

    private function statusChanged()
    {
        $delivery = $this->delivery;

        $new_status = $delivery->status;

        if($new_status != 'completed') return;
        
        $subject    = "Thank you for Delivery {$delivery->user->name}";
        $body       = view('admin.emails.delivery.complete', ['delivery' => $delivery, 'subject' => $subject])->render();
        
        Broadcast::create([

            'user_id'   => $delivery->user_id,
            'type'      => 'email',
            'body'      => $body,
            'subject'   => $subject,
            'email'     => $delivery->user->email
        ]);

    }
}
