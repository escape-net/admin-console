<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Delivery;
use Auth;

class Transaction extends Model
{
    protected $hidden 	= ['updated_at'];
	protected $guarded 	= ['updated_at'];

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function delivery()
	{
		return $this->belongsTo('App\Delivery')->withDefault(function(){

			return new \App\Delivery();
		});
	}

	public static function addCashPayment(Delivery $delivery)
	{
		$transaction = self::create([

			'user_id'					=> $delivery->user_id,
			'delivery_id'				=> $delivery->id,
			'source'					=> 'CASH',
			'amount'					=> $delivery->total,
			'status'					=> 'PAID',
			'transaction_reference'		=> str_random(8),
			'transaction_date'			=> date('Y-m-d H:i:s'),
			'notes'						=> 'Payment created by '.Auth::user()->name
		]);

		return $transaction;
	}
}
