<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Pusher\Pusher;
use \Pusher\PushNotifications\PushNotifications;

class PusherChannel extends Model
{
    public static function trigger($channel = '', $event = '', $payload = [])
    {
    	try{

    		$pusher = new Pusher(env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), ['cluster' => env('PUSHER_APP_CLUSTER'), 'useTLS' => true]);
    		
    		$pusher->trigger($channel, $event, $payload);

    		return true;

    	}catch(Exception $e){
            
    		_log('Could not trigger '.$channel.' Error : '.$e->getMessage());

    		return false;
    	}
    }

    public static function pushNotification($interests = [], $title = 'Silence', $body = 'Silence is Golden')
    {
        try{

            return true;
            
            $beamsClient = new PushNotifications(['instanceId' => env('PUSHER_INSTANCE_ID'), 'secretKey' => env('PUSHER_INSTANCE_SECRET_KEY')]);
            
            $beamsClient->publishToInterests($interests, [

                'fcm'   => [

                    'notification' => ['title' => $title, 'body' => $body]
                ]
            ]);

            return true;

        }catch(Exception $e){

            _log('Could not dispatch push notification '.$e->getMessage());

            return false;
        }
    }
}
