<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{

    protected $policies = [

        'App\Company' => 'App\Policies\CompanyPolicy',
        'App\Delivery' => 'App\Policies\DeliveryPolicy',
        'App\Promotion' => 'App\Policies\PromotionPolicy',
        'App\Transaction' => 'App\Policies\TransactionPolicy',
        'App\Vehicle' => 'App\Policies\VehiclePolicy',
        'App\VehicleType' => 'App\Policies\VehicleTypePolicy',
        'App\User' => 'App\Policies\UserPolicy'
        
    ];

    public function boot()
    {
        $this->registerPolicies();

    }
}
