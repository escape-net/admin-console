<?php

namespace App;

use Kreait\Firebase;
use Firebase\Auth\Token\Exception\InvalidToken;
use Kreait\Firebase\Exception\Messaging\InvalidMessage;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
use Kreait\Firebase\Messaging\AndroidConfig;
use App\Delivery;
use App\User;

class FirebaseMessaging
{
    public static function notify($device_token = null, $title = 'Velocity', $body = 'Velocity Notification', $data = [])
    {

        try{

            if($device_token == null) return false;

            $serviceAccount = ServiceAccount::fromJsonFile(config_path('massive-1090-firebase-adminsdk-omqjk-d652e6e67a.json'));

            $firebase   = (new Factory)->withServiceAccount($serviceAccount)->create();

            $messaging  = $firebase->getMessaging();

            $config = AndroidConfig::fromArray([

                'ttl'               => '3600s',
                'priority'          => 'high',
                'notification'      => [

                    'title'     => $title,
                    'body'      => $body,
                    'color'     => '#52009f',
                ],
            ]);

            $message = CloudMessage::withTarget('token', $device_token)
            ->withAndroidConfig($config)
            ->withData($data);

            $firebase->getMessaging()->validate($message);

            $messaging->send($message);

            return true;

        }catch(InvalidMessage $e){

            _log('Notification sending failed. '. $e->getMessage());
            return false;

        }catch(Exception $e){

            _log('Could not send notification '. $e->getMessage());
            return false;
        
        }finally{

            return false;
        }
    }

    public static function notifyDriverOfDeliveryAssignment(User $driver, Delivery $delivery)
    {
        $data = [

            'type'      => 'assigned',
            'to'        => 'driver',
            'payload'   => json_encode(['delivery_id' => $delivery->id])
        ];

        $title  = 'Velocity';
        $body   = "You just got assigned to delivery {$delivery->reference}";

        FirebaseMessaging::notify($driver->device_token, $title, $body, $data);

        return true;
    }

    
    public static function notifyUserThatDeliveryWasDeclined(User $user, Delivery $delivery){

        $data = [

            'type'      => 'declined',
            'to'        => 'user',
            'payload'   => json_encode(['delivery_id' => $delivery->id])
        ];

        $title  = 'The Driver Declined';
        $body   = "The driver declined this delivery. A new driver would be assigned.";

        FirebaseMessaging::notify($user->device_token, $title, $body, $data);

        return true;
    }

    public static function notifyUserThatDeliveryHasStarted(User $user, Delivery $delivery){

        $data = [

            'type'      => 'started',
            'to'        => 'user',
            'payload'   => json_encode(['delivery_id' => $delivery->id])
        ];

        $title  =  "Your Package {$delivery->item} is on its way.";
        $body   =  "{$delivery->driver->name} has started the journey to deliver your Item(s)";

        FirebaseMessaging::notify($user->device_token, $title, $body, $data);

        return true;
    }

    public static function notifyUserThatDeliveryIsComplete(User $user, Delivery $delivery)
    {
        $data = [

            'type'      => 'completed',
            'to'        => 'user',
            'payload'   => json_encode(['delivery_id' => $delivery->id])
        ];

        $title  = 'Your Delivery has been completed.';
        $body   = "Your Package ({$delivery->item}) has been successfully Delivered.";

        FirebaseMessaging::notify($user->device_token, $title, $body, $data);

        return true;
    }
}
