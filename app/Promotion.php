<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $hidden = ['updated_at'];
	protected $guarded 	= ['updated_at'];

    public function setStartAtAttribute($value)
    {
        return date('Y-m-d', strtotime($value));
    }

    public function setEndAtAttribute($value)
    {
        return date('Y-m-d', strtotime($value));
    }

    public static function getPromotionTotal($id = 0, $total = 0)
    {
        $promotion = self::find($id);
        if(!$promotion) return 0;

        if($promotion->type == 'flat')
            return $promotion->amount;

        return floor(($promotion->amount / 100) * $total);
    }
}
