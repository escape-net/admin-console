<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $hidden 	= ['updated_at'];
	protected $guarded 	= ['updated_at'];
	
	public function company()
	{
		return $this->belongsTo('App\Company');
	}

	public function vehicle_type()
	{
		return $this->belongsTo('App\VehicleType')->withDefault(function(){

            return new \App\VehicleType();
        });
    }

    public function driver(){

        return $this->belongsTo('App\User', 'user_id')->withDefault(function(){

        	return new \App\User();
        });
    }

    public function getNameAttribute()
    {
        return $this->plate_number.'/'.$this->make.'/'.$this->year.'['.$this->vehicle_type->name.']';
    }
}
