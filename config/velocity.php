<?php 

return [

	'tooltips' => [

		'promotion' => [

			'user' => 'If you would like to allow only a few users to use this code, Select them from the drop down. you can choose the All Users option to make this promocode open to all active Users.',
			'type' => 'When you choose Percentage, promo Amount will be amount% of Total Delvery.',
			'limit' => 'Promocode can be used Once by a user. You can set a Usage Limit to set the number of allowed users for this promo code. Leave Blank for Unlimited Use.',
			'start_at' => '',
			'end_at' => ''
		]
	]
];